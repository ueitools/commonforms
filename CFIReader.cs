using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace CommonForms
{
    public class CFIReader
    {
        bool  m_ToggleBit = false;
        int[] prefixes;

        public CFIReader()
        {
            m_ToggleBit = false;
            prefixes = new int[20];
        }

        public string ReadCFIFile(string file)
        {
            return GetCFIDataString(file);
        }

        public void ScanXprefix(string ID, string Path,
            out int PrefixCount)
        {
            string filename;

            if (Path == null)
                filename = GetCFG("TNDBPATH") + "\\xprefix\\"
                    + ID + ".exp";
            else filename = Path + "\\xprefix\\" + ID + ".exp";

            if (!File.Exists(filename))
            {
                throw new Exception(
                    "Cannot find external prefix file");
            }

            FileInfo file = new FileInfo(filename);
            StreamReader stream = file.OpenText();

            string buf;
            int index = 0;

            while ((buf = stream.ReadLine()) != null)
            {
                if (buf.ToUpper().Contains(" IF "))
                    if (buf.Contains(" HC05RC16"))
                        break;
            }

            while ((buf = stream.ReadLine()) != null)
            {
                if (buf.ToUpper().Contains("ENDIF"))
                    break;

                string[] str = buf.Split(new char[] { ';' },
                    StringSplitOptions.RemoveEmptyEntries);

                str = str[0].Split(new char[] { ',' },
                    StringSplitOptions.RemoveEmptyEntries);

                if (str == null || str.Length < 1)
                    continue;

                string[] str1 = str[0].Split(new char[] { ' ' },
                    StringSplitOptions.RemoveEmptyEntries);

                if (str1 != null)
                {
                    try
                    {
                        int n = int.Parse(str1[str1.Length - 1]);

                        if (n > 0)
                            prefixes[index++] = n;
                    }
                    catch { }
                }

                if (str.Length < 2)
                    continue;

                string[] str2 = str[1].Split(new char[] { ' ' },
                    StringSplitOptions.RemoveEmptyEntries);

                if (str2 != null)
                {
                    try
                    {
                        int n = int.Parse(str2[0]);

                        if (n > 0)
                            prefixes[index++] = n;
                    }
                    catch { }
                }
            }

            while ((buf = stream.ReadLine()) != null)
            {
                if (buf.Contains("DB"))
                {
                    string[] str = buf.Split(new char[] { ' ' },
                        StringSplitOptions.RemoveEmptyEntries);

                    if (str.Length > 1)
                    {
                        string num = str[1].Trim();
                        prefixes[index] = 0;

                        for (int i = 0; i < str.Length; i++)
                        {
                            prefixes[index] |= (num[str.Length - i - 1]
                                == '1' ? 1 : 0) << i;
                        }
                        index++;
                    }
                }
            }

            PrefixCount = index;
            stream.Close();
        }

        public void GetPrefix(int pfx, out int prefix)
        {
            prefix = prefixes[pfx];
        }

        public string GetCfiLabel(string cfiFilePath) {
            string buf;
            FileInfo file = new FileInfo(cfiFilePath);
            string keyLabel = string.Empty;

            int lineNo = 0;
            
             using (StreamReader stream = file.OpenText())
             {
                while ((buf = stream.ReadLine()) != null && keyLabel == string.Empty)
                {
                    lineNo++;
                    if (buf.StartsWith("Key Label"))
                    {
                        string s = buf.Replace("Key Label", "");
                        s = s.Replace("=", "");

                        keyLabel = s.Trim();
                    }
                }
                stream.Close();
                file = null;
            }

            return keyLabel;
        }

        public string GetCFIDataString(string cfiFilePath)
        {
            CFIInfo cfiInfo;
            return GetCFIDataString(cfiFilePath, out cfiInfo);
        }

        public string GetCFIDataString(string cfiFilePath,
            out CFIInfo cfiInfo)
        {
            return GetCFIDataString(cfiFilePath, out cfiInfo, 
                true, 0);
        }

        public string GetCFIDataString(string cfiFilePath,
            out CFIInfo cfiInfo, bool maskToggle, int iter)
        {
            string buf;
            string mCFIFrameBuf = "", mToggleBuf = "",
                mCFIDataBuffer = "", lastFrameData = "";
            bool codeGapLineRead = false;
            bool addedLastFrame = false;
            FileInfo file = new FileInfo(cfiFilePath);
            int numOfPulseDelay = 0, pulseDelay = 0;

            cfiInfo = new CFIInfo();
            int lineNo = 0;

            using (StreamReader stream = file.OpenText())
            {
                while ((buf = stream.ReadLine()) != null)
                {
                    lineNo++;

                    if (buf.StartsWith("Data"))
                    {
                        ReadDataLine(buf, ref mCFIFrameBuf);
                        codeGapLineRead = false;

                        CFILineInfo lineInfo = new CFILineInfo();
                        lineInfo.Name = "Data";
                        lineInfo.lineNo = lineNo;
                        lineInfo.Value = -1;

                        cfiInfo.LineList.Add(lineInfo);
                    }
                    else if (buf.StartsWith("Code Gap Duration"))
                    {
                        lastFrameData = mCFIFrameBuf;
                        addedLastFrame = TryAddingFrameData(ref mCFIFrameBuf,
                                        ref mCFIDataBuffer, mToggleBuf, maskToggle,
                                        iter);
                        codeGapLineRead = true;

                        numOfPulseDelay = 0;
                        pulseDelay = 0;

                        CFILineInfo lineInfo = new CFILineInfo();
                        lineInfo.Name = "CodeGap";
                        lineInfo.lineNo = lineNo;

                        string s = buf.Replace("Code Gap Duration", "");
                        s = s.Replace("=", "");
                        s = s.Replace("usec", "");

                        int gapValue;
                        lineInfo.Value = int.MaxValue;
                        if (int.TryParse(s.Trim(), out gapValue))
                        {
                            lineInfo.Value = gapValue;
                        }

                        cfiInfo.LineList.Add(lineInfo);
                    }
                    else if (buf.StartsWith("Toggle Bit"))
                    {
                        ReadToggleLine(buf, out mToggleBuf);
                    }
                    else if (buf.StartsWith("Period"))
                    {
                        string s = buf.Replace("Period", "");
                        s = s.Replace("=", "");
                        s = s.Replace("usec", "");

                        cfiInfo.Frequency = double.Parse(s.Trim());
                    }
                    else if (buf.StartsWith("Dutycycle"))
                    {
                        string s = buf.Replace("Dutycycle", "");
                        s = s.Replace("=", "");
                        s = s.Replace("%", "");

                        cfiInfo.DutyCycle = int.Parse(s.Trim());
                    }

                    if (buf.StartsWith("Pulse Duration"))
                    {
                        if (pulseDelay == 1)
                        {
                            numOfPulseDelay++;
                            pulseDelay = 0;
                        }
                        else
                        {
                            pulseDelay = 1;
                        }

                        if (numOfPulseDelay >= 5)
                        {
                            continue;
                        }

                        CFILineInfo lineInfo = new CFILineInfo();
                        lineInfo.Name = "Pulse";
                        lineInfo.lineNo = lineNo;

                        string s = buf.Replace("Pulse Duration", "");
                        s = s.Replace("=", "");
                        s = s.Replace("usec", "");

                        lineInfo.Value = int.Parse(s.Trim());

                        cfiInfo.LineList.Add(lineInfo);
                    }
                    else if (buf.StartsWith("Delay Duration"))
                    {
                        if (pulseDelay == 1)
                        {
                            numOfPulseDelay++;
                            pulseDelay = 0;
                        }
                        else
                        {
                            pulseDelay = 1;
                        }

                        if (numOfPulseDelay >= 5)
                        {
                            continue;
                        }

                        CFILineInfo lineInfo = new CFILineInfo();
                        lineInfo.Name = "Delay";
                        lineInfo.lineNo = lineNo;

                        string s = buf.Replace("Delay Duration", "");
                        s = s.Replace("=", "");
                        s = s.Replace("usec", "");

                        lineInfo.Value = int.Parse(s.Trim());

                        cfiInfo.LineList.Add(lineInfo);
                    }
                    else
                    {
                        numOfPulseDelay = 0;
                        pulseDelay = 0;
                    }
                }

                stream.Close();
                file = null;
            }

            // If CFI file ended and no codegap line, make 
            // a call to record the data read so far.
            if (codeGapLineRead == false)
                TryAddingFrameData(ref mCFIFrameBuf, ref mCFIDataBuffer,
                    mToggleBuf, maskToggle, iter, true);
            else
            {
                if (addedLastFrame == false)
                    TryAddingFrameData(ref lastFrameData, ref mCFIDataBuffer,
                                       mToggleBuf, maskToggle, iter, true);

            }

            cfiInfo.Format();
            return mCFIDataBuffer.Trim(); ;
        }

        private string GetCFG(string varName)
        {
            string toolscfgPath
                = Assembly.GetExecutingAssembly().GetName().CodeBase;

            int i = 0;
            toolscfgPath = toolscfgPath.Trim();

            if (toolscfgPath.StartsWith("file:"))
                i = 5;

            while (toolscfgPath[i] == '/')
                i++;

            toolscfgPath = toolscfgPath.Substring(i);

            i = toolscfgPath.Length - 1;

            while (toolscfgPath[i] != '/')
                i--;

            toolscfgPath = toolscfgPath.Substring(0, i + 1);
            toolscfgPath += "/TOOLS.CFG";

            toolscfgPath = toolscfgPath.Replace('/', '\\');

            if (!File.Exists(toolscfgPath))
            {
                throw new Exception(
                    "TOOLS.CFG is missing, process abort.");
            }

            string buf;

            FileInfo file = new FileInfo(toolscfgPath);
            StreamReader stream = file.OpenText();

            while ((buf = stream.ReadLine()) != null)
            {
                buf = buf.Trim();

                if (buf.StartsWith(varName))
                {
                    string[] str = buf.Split(new char[] { '=' },
                        StringSplitOptions.RemoveEmptyEntries);

                    if (str == null || str.Length < 2)
                    {
                        throw new Exception("File TOOLS.CFG is corrupt"
                            + ", process abort.");
                    }

                    return str[1].Trim();
                }
            }

            throw new Exception("Variable " + varName
                + " is not in file TOOLS.CFG, process abort.");

            return null;
        }

        private void ReadDataLine(string buf, ref string mCFIFrameBuf)
        {
            string[] str = buf.Split(new char[] { '=' },
                StringSplitOptions.RemoveEmptyEntries);

            if (str == null || str.Length < 2)
            {
                return;
            }

            str = str[1].Split(new char[] { ' ' },
                StringSplitOptions.RemoveEmptyEntries);

            mCFIFrameBuf += str[0] + " ";
        }

        private void ReadToggleLine(string buf, out string mToggleBuf)
        {
            string[] str = buf.Split(new char[] { '=' },
                StringSplitOptions.RemoveEmptyEntries);

            if (str == null || str.Length < 2)
            {
                mToggleBuf = "";
                return;
            }

            str = str[1].Split(new char[] { ' ' },
                StringSplitOptions.RemoveEmptyEntries);

            mToggleBuf = str[0];

            m_ToggleBit = true;
        }

        /// <summary>
        /// Code Gap line in CFI file indicates the end of a Frame.
        /// At this point take the data we've just read for the past frame and add it to 
        /// the existing data of past frames only if the data for the frame is not a duplicate.
        /// </summary>
        /// <param name="mCFIFrameBuf"></param>
        /// <param name="mCFIDataBuffer"></param>
        /// <param name="mToggleBuf"></param>
        /// <param name="maskToggle"></param>
        /// <param name="iter"></param>
        private bool TryAddingFrameData(ref string mCFIFrameBuf,
            ref string mCFIDataBuffer, string mToggleBuf, 
            bool maskToggle, int iter, bool forceAdd)
        {
            if (mCFIFrameBuf == "")
                return true;

            SetToggleBits(ref mCFIFrameBuf, mToggleBuf, maskToggle, iter);

            bool duplicated = false;

            string[] str = mCFIDataBuffer.Split(new string[] { "FD" },
                StringSplitOptions.RemoveEmptyEntries);

            mCFIFrameBuf = mCFIFrameBuf.Trim();
            if (forceAdd == false)
            {
            for (int i = 0; i < str.Length; i++)
            {
                str[i] = str[i].Trim();
                if (str[i] == mCFIFrameBuf)
                {
                    duplicated = true;
                    break;
                }
            }
            }

            if (!duplicated)
                mCFIDataBuffer += "FD " + mCFIFrameBuf;

            mCFIFrameBuf = "";

            return !duplicated;
        }

        private bool TryAddingFrameData(ref string mCFIFrameBuf,
            ref string mCFIDataBuffer, string mToggleBuf, 
            bool maskToggle, int iter)
        {
            return TryAddingFrameData(ref mCFIFrameBuf, ref mCFIDataBuffer, mToggleBuf, maskToggle, iter, false);
        }

        private void SetToggleBits(ref string mCFIFrameBuf,
            string mToggleBuf, bool maskToggle, int iter)
        {
            if (!m_ToggleBit)
                return;

            mCFIFrameBuf = mCFIFrameBuf.Trim();

            for (int i = 0, j = 0; i < mCFIFrameBuf.Length
                && j < mToggleBuf.Length; i++)
            {
                if (mCFIFrameBuf[i] != 'B' && mCFIFrameBuf[i] != ' ')
                {
                    if (mToggleBuf[j] == 'T' && maskToggle)
                    {
                        if (i == 0)
                        {
                            mCFIFrameBuf = "" + 'T'
                                + mCFIFrameBuf.Substring(1);
                        }
                        else if (i == (mCFIFrameBuf.Length - 1))
                        {
                            mCFIFrameBuf
                                = mCFIFrameBuf.Substring(0, i) + 'T';
                        }
                        else
                        {
                            mCFIFrameBuf
                                = mCFIFrameBuf.Substring(0, i)
                                    + 'T'
                                     + mCFIFrameBuf.Substring(i + 1);
                        }
                        j++;
                    }
                    else if (mToggleBuf[j++] == 'T' && iter >= 0)
                    {
                        bool doubleBits = false;

                        if (j < mToggleBuf.Length && mToggleBuf[j] == 'T')
                        {
                            doubleBits = true;
                            j++;
                        }

                        if (!doubleBits)
                        {
                            string toggle;

                            if ((iter % 2) == 0)
                                toggle = "0";
                            else toggle = "1";

                            if (i == 0)
                            {
                                mCFIFrameBuf = toggle
                                    + mCFIFrameBuf.Substring(1);
                            }
                            else if (i == (mCFIFrameBuf.Length - 1))
                            {
                                mCFIFrameBuf
                                    = mCFIFrameBuf.Substring(0, i) + toggle;
                            }
                            else
                            {
                                mCFIFrameBuf
                                    = mCFIFrameBuf.Substring(0, i)
                                        + toggle
                                         + mCFIFrameBuf.Substring(i + 1);
                            }
                        }
                        else
                        {
                            string toggle;

                            if ((iter % 4) == 0)
                                toggle = "00";
                            else if ((iter % 4) == 1) 
                                toggle = "01";
                            else if ((iter % 4) == 2)
                                toggle = "10";
                            else toggle = "11";

                            if (i == 0)
                            {
                                mCFIFrameBuf = toggle
                                    + mCFIFrameBuf.Substring(2);
                            }
                            else if (i >= (mCFIFrameBuf.Length - 2))
                            {
                                mCFIFrameBuf
                                    = mCFIFrameBuf.Substring(0, i) + toggle;
                            }
                            else
                            {
                                mCFIFrameBuf
                                    = mCFIFrameBuf.Substring(0, i)
                                        + toggle
                                         + mCFIFrameBuf.Substring(i + 2);
                            }
                        }
                    }
                }
            }
        }
    }
}
