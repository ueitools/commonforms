using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

namespace CommonForms
{
    public static class UserSettings
    {
        const string _USERCONFIGFILE = "UserSettings.config";
        const string _ROOTNODE = "//UserConfig/";
        const string _KEY = "key";
        const string _VALUE = "value";
        public enum CurrentApp
        {
            READBACK_VERIFY,
            TIMING_INSPECTOR
        }

        public enum ConfigKeys
        {
            PICK_SOURCE,
            DATABASE,
            PICK_DIR,
            READBACK_DIR,
            DBSEL,
            FREQ_DEV,
            PULSE_DEV,
            DELAY_DEV,
            CODEGAP_DEV,
            DELAYPULSE_DEV,
            DUTYCYCLE_DEV,
            KEEPFI,
            KEEPCFI,
            CMPDUTYCHKBOX,
            CMPFREQCHKBOX,
            CMPPULSECHKBOX,
            CMPDELAYCHKBOX,
            CMPCODEGAP,
            CMPDELAYPUSLE,
            TICMPFRAMES,
            MASK_TOGGLE_BITS,
            CMPFRAMENUM,
            RPT_SORT_ORDER,
            RPT_SORT_COLOUMN,
            MERGE_REPORT,
            MERGE_REPORT_APPEND,
            MERGE_REPORT_SHOWALL
        }
        static string _strConfigFile = String.Empty;
        static UserSettings()
        {
            _strConfigFile = Configuration.GetWorkingDirectory() + Path.DirectorySeparatorChar + _USERCONFIGFILE;

            if (!File.Exists(_strConfigFile))
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
                XmlElement rootNode = xmlDoc.CreateElement("UserConfig");
                xmlDoc.InsertBefore(xmlDeclaration, xmlDoc.DocumentElement);
                xmlDoc.AppendChild(rootNode);
                XmlElement rbNode = xmlDoc.CreateElement(CurrentApp.READBACK_VERIFY.ToString());
                rootNode.AppendChild(rbNode);
                XmlElement tiNode = xmlDoc.CreateElement(CurrentApp.TIMING_INSPECTOR.ToString());
                rootNode.AppendChild(tiNode);
                xmlDoc.Save(_strConfigFile);
            }
        }

        public static void SetUserConfig(CurrentApp curApp, ConfigKeys key, string value)
        {
            ////
            _strConfigFile = Configuration.GetWorkingDirectory() + Path.DirectorySeparatorChar + _USERCONFIGFILE;
            if (!File.Exists(_strConfigFile))
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
                XmlElement rootNode = xmlDoc.CreateElement("UserConfig");
                xmlDoc.InsertBefore(xmlDeclaration, xmlDoc.DocumentElement);
                xmlDoc.AppendChild(rootNode);
                XmlElement rbNode = xmlDoc.CreateElement(CurrentApp.READBACK_VERIFY.ToString());
                rootNode.AppendChild(rbNode);
                XmlElement tiNode = xmlDoc.CreateElement(CurrentApp.TIMING_INSPECTOR.ToString());
                rootNode.AppendChild(tiNode);
                xmlDoc.Save(_strConfigFile);
            }
            ////

            bool bNewEntry = true;
            string strQuery = _ROOTNODE + curApp.ToString() + "/set";
            XmlNodeList xmlNodeList;
            XmlDocument doc = new XmlDocument();
            doc.Load(_strConfigFile);
            xmlNodeList = doc.SelectNodes(strQuery);
            foreach (XmlNode node in xmlNodeList)
            {
                if (node.Attributes[_KEY].Value == key.ToString())
                {
                    bNewEntry = false;
                    node.Attributes[_VALUE].Value = value;
                    break;
                }
            }
            if (bNewEntry)
            {
                XmlNode appNode = doc.SelectSingleNode(_ROOTNODE + curApp.ToString());
                XmlElement element = doc.CreateElement("set");
                element.SetAttribute(_KEY, key.ToString());
                element.SetAttribute(_VALUE, value);
                appNode.AppendChild(element);
            }
            doc.Save(_strConfigFile);
        }

        public static string GetUserConfig(CurrentApp curApp, ConfigKeys key)
        {
            string value = string.Empty;
            string strQuery = _ROOTNODE + curApp.ToString() + "/set";
            XmlNodeList xmlNodeList;
            XmlDocument doc = new XmlDocument();
            doc.Load(_strConfigFile);
            xmlNodeList = doc.SelectNodes(strQuery);
            foreach (XmlNode node in xmlNodeList)
            {
                if (node.Attributes[_KEY].Value == key.ToString())
                {
                    value = node.Attributes[_VALUE].Value;
                    break;
                }
            }
            return value;
        }

        public static int GetUserConfig(CurrentApp curApp, ConfigKeys key, int iDefaultVal)
        {
            bool bFound = false;
            string value = string.Empty;
            string strQuery = _ROOTNODE + curApp.ToString() + "/set";
            XmlNodeList xmlNodeList;
            XmlDocument doc = new XmlDocument();
            doc.Load(_strConfigFile);
            xmlNodeList = doc.SelectNodes(strQuery);
            foreach (XmlNode node in xmlNodeList)
            {
                if (node.Attributes[_KEY].Value == key.ToString())
                {
                    value = node.Attributes[_VALUE].Value;
                    bFound = true;
                    break;
                }
            }
            if (!bFound)
                return iDefaultVal;

            return Convert.ToInt16(value);
        }
    }
}
