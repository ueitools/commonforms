using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using BusinessObject;
using ExecLib;
using Microsoft.Win32;

namespace CommonForms
{
    public class FunctionList
    {
        #region Private members
        private static string PROJ_ROOT =
            "HKEY_CURRENT_USER\\Software\\UEITools\\IRDBSearch";

        private static string DICPath = "F:\\EREBUS\\SOURCE\\ID\\DEVEL\\";
        private static Hashtable DicIntronList = null;

        private static int MAX_EXEC = ExecsInC.NUM_EXECS;
        private static int INVALID_EXEC = ExecsInC.INVALID_EXEC;

        private static PrinterSettings printerSettings = new PrinterSettings();
        private static PageSettings pageSettings = new PageSettings();
        private static StringReader streamToPrint;
        private static Font printFont;

        //Start Add 20120621 Warning for Exec Not Implemented
        private static int _iEXEC_STATUS = 1;
        //End Add 20120621

        private static IExecsInC ExFuncs  = new ExecsInC();
        #endregion

        #region Print functions

        public static void PrinterPageSetup()
        {
            PageSetupDialog psd = new PageSetupDialog();

            psd.PageSettings = pageSettings;
            psd.PrinterSettings = printerSettings;
            psd.ShowDialog();
            Registry.SetValue(PROJ_ROOT, "PageSettings", pageSettings);
        }

        public static void PrintReport(string ReportText)
        {
            streamToPrint = new System.IO.StringReader(ReportText);
            printFont = new System.Drawing.Font("Courier New", 10,
                                            System.Drawing.FontStyle.Regular);

            PrintDocument doc = new PrintDocument();
            doc.PrintPage += new PrintPageEventHandler(document_PrintPage);
            doc.PrinterSettings = printerSettings;
            doc.DefaultPageSettings = pageSettings;

            PrintDialog dlg = new PrintDialog();
            dlg.Document = doc;
            dlg.PrinterSettings = printerSettings;

            DialogResult result = dlg.ShowDialog();

            if (result != DialogResult.Cancel)
            {
                doc.Print();
            }
        }
        #endregion

        #region Save functions

        public static void Save(string ReportText, string fileName)
        {
            string file = fileName.Substring(
                fileName.LastIndexOf('\\') + 1);

            string surfix = file.Substring(file.LastIndexOf('.'));
            surfix = surfix.ToLower();
            surfix = "*" + surfix;
            file = file.Substring(0, file.LastIndexOf('.'));
            string fileType = "Files";

            if (surfix == "*.log")
                fileType = "Log Files";
            else if (surfix == "*.uei")
                fileType = "UEI Files";
            else if (surfix == "*.qa")
                fileType = "QA Files";
            else if (surfix == "*.rpt")
                fileType = "RPT Files";

            string filename = SaveFileAs(fileName,
                        String.Format(
                        "{0} ({1})|{1}|All Files (*.*)|*.*",
                        fileType, surfix));

            if (!string.IsNullOrEmpty(filename))
                File.WriteAllText(filename, ReportText);
        }

        // The PrintDialog will print the document
        // by handling the document's PrintPage event.
        private static void document_PrintPage(object sender, PrintPageEventArgs ev)
        {
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            //float leftMargin = ev.PageBounds.Left;
            //float topMargin = ev.PageBounds.Top;
            string line = null;

            // Calculate the number of lines per page.
            linesPerPage = ev.MarginBounds.Height /
               printFont.GetHeight(ev.Graphics);

            // Print each line of the file.
            while (count < linesPerPage &&
               ((line = streamToPrint.ReadLine()) != null))
            {
                yPos = topMargin + (count *
                   printFont.GetHeight(ev.Graphics));
                ev.Graphics.DrawString(line, printFont, Brushes.Black,
                   leftMargin, yPos, new StringFormat());
                count++;
            }

            // If more lines exist, print another page.
            if (line != null)
                ev.HasMorePages = true;
            else
                ev.HasMorePages = false;
        }

        private static string SaveFileAs(string FileName,
            string filter)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.RestoreDirectory = true;
            sfd.FileName = FileName;
            sfd.Filter = filter;
            if (sfd.ShowDialog() == DialogResult.OK)
                return sfd.FileName;
            return null;
        }
        #endregion

        #region Data query functions
        /// <summary>
        /// retrieve the data for the given ID and Query
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Query"></param>
        /// <returns></returns>
        public static HashtableCollection GetDBdata(string ID, string Query)
        {
            Hashtable paramList = new Hashtable();

            paramList.Add("ID", ID);
            HashtableCollection list = DAOFactory.AdHoc().Select(Query, paramList);
            return list;
        }

        /// <summary>
        /// retrieve the data for the given ID list and Query
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Query"></param>
        /// <returns></returns>
        public static HashtableCollection GetGroupDBdata(StringCollection IDList,
            string Query)
        {
            Hashtable paramList = new Hashtable();
            //paramList.Add("IDList", IDList);
            //
            string idList = string.Empty;
            foreach (string item in IDList)
            {
                idList += item;
            }

            paramList.Add("IDList", idList);
            //
            HashtableCollection list = DAOFactory.AdHoc().Select(Query,
                paramList);
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="label"></param>
        /// <returns></returns>
        public static string GetIntronData(string mode , string label)
        {
            //
            if (string.IsNullOrEmpty(label))
                return null;

            Hashtable paramList = new Hashtable();

            paramList.Add("Label", label);
            paramList.Add("Modes", mode);

            Hashtable table = DAOFactory.AdHoc().SelectIntron(paramList);

            if (table == null)
            {
                paramList["Modes"] = "G";
                table = DAOFactory.AdHoc().SelectIntron(paramList);

                if (table == null)
                    return null;
            }

            return (string)table["Intron"];
        }


        #endregion 

        #region TX->CFI, RX->CFI files, TX->FI & RX->FI files

        public static string GetFileName(string filePath)
        {
            string fileName = filePath;
            int i = fileName.LastIndexOf('\\');
            return fileName.Substring(i + 1);
        }

        /// <summary>
        /// Convert the TN Capture file into a .CFI file using the execs() Rx() function 
        /// </summary>
        /// <param name="OutFile1">Output file Path/Name</param>
        /// <param name="RxFile">Input file Path/Name</param>
        /// <param name="Exec">Exec number to set into the exec object</param>
        public static void ConvertCaptureToCfiFile(string OutFile1, string RxFile, int Exec)
        {
            ConvertCaptureToCfiFile(OutFile1, RxFile, Exec, "test key");
        }

        public static void ConvertCaptureToCfiFile(string OutFile1, 
            string RxFile, int Exec, string label)
        {
            ExFuncs.cInit(OutFile1, Exec, 0);
            ExFuncs.cSetKeyLabel(label);

            ExFuncs.cSetReportType(3);
            _iEXEC_STATUS = ExFuncs.cRx(RxFile);

            if (ExFuncs.cSetExec(Exec, null) != 0)
                _iEXEC_STATUS = 1;

            ExFuncs.cShutdown();

           
            //CC
            try
            {
                int iRet = 0;
                RBKSPECWRAPPERLib.IRbkSpecWrapper irb = new RBKSPECWRAPPERLib.RbkSpecWrapper();
                irb.DoRbkTranslate(OutFile1, Exec, out iRet);
                if (iRet == 1)
                {
                    string strCFTfile = Path.GetDirectoryName(OutFile1) + "\\" +
                                        Path.GetFileNameWithoutExtension(OutFile1) + ".CFT";

                    File.Copy(strCFTfile, OutFile1, true);
                    File.Delete(strCFTfile);
                }
            }
            catch (Exception exx)
            { }
            //
        }

        public static void ConvertCaptureToCfiFile(string OutFile1, 
            string RxFile, int Exec, string GenericCfgPath, string label)
        {
            ExFuncs.cInit(OutFile1, Exec, 0);
            ExFuncs.cSetExec(Exec, GenericCfgPath);
            ExFuncs.cSetKeyLabel(label);

            ExFuncs.cSetReportType(3);
            ExFuncs.cRx(RxFile);
            ExFuncs.cShutdown();
            //CC
            try
            {
                int iRet = 0;
                RBKSPECWRAPPERLib.IRbkSpecWrapper irb = new RBKSPECWRAPPERLib.RbkSpecWrapper();
                irb.DoRbkTranslate(OutFile1, Exec, out iRet);
                if (iRet == 1)
                {
                    string strCFTfile = Path.GetDirectoryName(OutFile1) + "\\" +
                                        Path.GetFileNameWithoutExtension(OutFile1) + ".CFT";

                    File.Copy(strCFTfile, OutFile1, true);
                    File.Delete(strCFTfile);
                }
            }
            catch (Exception exx)
            { }
            //
        }

        public static void ConvertCaptureToCfiFile(string OutFile1, string RxFile,
            int Exec, string GenericCfgPath, Hashtable labelList) {
            string label = string.Empty;

            if (labelList != null) {
                string fileName = GetFileName(OutFile1);
                fileName = fileName.Replace(".cfi", "");
                label = (string)labelList[fileName];
            }

            if (String.IsNullOrEmpty(label)) {
                label = "test key";
            }

            ConvertCaptureToCfiFile(OutFile1, RxFile, Exec, GenericCfgPath, label);
        }

        /// <summary>
        /// Convert the TN Capture file into a .CFI file using the execs() Rx() function 
        /// </summary>
        /// <param name="OutFile1">Output file Path/Name</param>
        /// <param name="RxFile">Input file Path/Name</param>
        /// <param name="Exec">Exec number to set into the exec object</param>
        public static void ConvertCaptureToCfiFile(string OutFile1, string RxFile, int Exec,
            Hashtable labelList)
        {
            string label = string.Empty;

            if (labelList != null) {
                string fileName = GetFileName(OutFile1);
                fileName = fileName.Replace(".cfi", "");
                label = (string)labelList[fileName];
            }

            if (String.IsNullOrEmpty(label))
                label = "test key";

            ConvertCaptureToCfiFile(OutFile1, RxFile, Exec, label);
        }

        /// <summary>
        /// Retrieve all prefixes, they may come from the database or an
        /// external prefix file return them in a List array
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="IDheader"></param>
        /// <returns></returns>
        public static List<int> CollectPrefixes(string ID, HashtableCollection IDheader)
        {
            List<int> Prefixes = new List<int>();

            if (IDheader.Count == 0)
                return Prefixes;

            CFIReader CfiReader = new CFIReader();
            bool ExternalPrefix = ((string)IDheader[0]["IsExternalPrefix"] == "Y");
            //int PrefixCount = 0;
            //int Prefix = 0;
            if (ExternalPrefix)
            {
                throw new Exception("error: ID is no longer valid");
                /*
                string ID3 = ID[0] + ID.Substring(2);
                CfiReader.ScanXprefix(ID3, null, out PrefixCount);
                for (int pfx = 0; pfx < PrefixCount; pfx++)
                {
                    CfiReader.GetPrefix(pfx, out Prefix);
                    Prefixes.Add(Prefix);
                }
                 */ 
            }
            else
            {
                foreach (Hashtable row in IDheader)
                {
                    Prefixes.Add(Convert.ToInt32((string)row["Data"], 2));
                }
            }
            return Prefixes;
        }

        /// <summary>
        /// Retrieve all prefixes, they may come from the database or an
        /// external prefix file return them in a List array
        /// </summary>
        /// <param name="idHeader"></param>
        /// <returns></returns>
        public static List<int> CollectPrefixes(IDHeader idHeader)
        {
            List<int> Prefixes = new List<int>();

            CFIReader CfiReader = new CFIReader();
            bool ExternalPrefix = (idHeader.IsExternalPrefix == "Y");
            //int PrefixCount = 0;
            //int Prefix = 0;
            if (ExternalPrefix)
            {
                throw new Exception("error: ID is no longer valid");
                /*
                string ID3 = idHeader.ID[0] + idHeader.ID.Substring(2);
                CfiReader.ScanXprefix(ID3, null, out PrefixCount);
                for (int pfx = 0; pfx < PrefixCount; pfx++)
                {
                    CfiReader.GetPrefix(pfx, out Prefix);
                    Prefixes.Add(Prefix);
                }*/
            }
            else
            {
                if (idHeader.PrefixList == null 
                    || idHeader.PrefixList.Count == 0)
                    return Prefixes;

                foreach (Prefix prefix in idHeader.PrefixList)
                {
                    if (String.IsNullOrEmpty(prefix.Data))
                    {
                        Prefixes.Add(0);
                        continue;
                    }

                    
                    Prefixes.Add(Convert.ToInt32(prefix.Data, 2));
                }
            }
            return Prefixes;
        }

        /// <summary>
        /// Use the execs() Tx() function to create a .CFI file using supplied information
        /// </summary>
        /// <param name="IDheader"></param>
        /// <param name="DataRow"></param>
        /// <param name="OutFile2"></param>
        public static void CreateTxCfiFile(IDHeader IDheader, List<int> Prefixes,
                                        Hashtable DataRow, string OutFile2)
        {
            CreateTxCfiFile(IDheader, Prefixes, DataRow, OutFile2, false);
        }

        public static int CreateTxCfiFile(IDHeader IDheader, List<int> Prefixes,
                                        Hashtable DataRow, string OutFile2, bool bSeparateToggleOutput)
        {
            int iFilesCreated = 0;
            int iToggleFileCount = 0;
            bool InvertData = IDheader.IsInversedData.Equals("Y");
            bool InvertPrefix = false; //IDheader.IsInversedPrefix.Equals("Y");// always false for now
            int Exec = IDheader.Executor_Code;

            ExFuncs.cInit(OutFile2, Exec, 0);
            ExFuncs.cSetReportType(3);
            ExFuncs.cSetKeyLabel((string)DataRow["Label"]);
            string outron = (string)DataRow["Intron"];
            
            if (!String.IsNullOrEmpty(outron))
                ExFuncs.cSetOutron(outron);
            LoadPrefixes(Prefixes, InvertPrefix);
            LoadData((string)DataRow["Data"], InvertData);

            ExFuncs.cTx();

            iFilesCreated++;            

            if (bSeparateToggleOutput) {
                string[] filename = OutFile2.Split(new char[] { '.' });
                iToggleFileCount = ExFuncs.cTg() * 2; // should be power of 2 later

                // send toggle data
                if (iToggleFileCount != 0) {
                    for (int i = 1; i < iToggleFileCount; i++) {
                        ExFuncs.cResetOutputStream(filename[0] + "_" + i + ".CFI");
                        ExFuncs.cTx();
                        iFilesCreated++;
                    }
                }
            }

            ExFuncs.cShutdown();

            //CC
            try
            {
                int iRet = 0;
                RBKSPECWRAPPERLib.IRbkSpecWrapper irb = new RBKSPECWRAPPERLib.RbkSpecWrapper();
                irb.DoRbkTranslate(OutFile2, Exec, out iRet);
                if (iRet == 1)
                {
                    string strCFTfile = Path.GetDirectoryName(OutFile2) + "\\" +
                                        Path.GetFileNameWithoutExtension(OutFile2) + ".CFT";

                    File.Copy(strCFTfile, OutFile2, true);
                    File.Delete(strCFTfile);
                }
            }
            catch (Exception exx)
            { }
            //

            return iFilesCreated;
        }

        /// <summary>
        /// Use the execs() Tx() function to create a .CFI file using supplied information
        /// </summary>
        /// <param name="IDheader"></param>
        /// <param name="DataRow"></param>
        /// <param name="OutFile2"></param>
        public static void CreateTxCfiFile(HashtableCollection IDheader, List<int> Prefixes,
                                        Hashtable DataRow, string OutFile2)
        {
            CreateTxCfiFile(IDheader, Prefixes, DataRow, OutFile2, false);
        }

        public static int CreateTxCfiFile(HashtableCollection IDheader, List<int> Prefixes,
                                        Hashtable DataRow, string OutFile2, bool bSeparateToggleOutput)
        {
            int iFilesCreated = 0;
            int iToggleFileCount = 0;
            bool InvertData = ((string)IDheader[0]["IsInversedData"]).Equals("Y");
            bool InvertPrefix = false;  //((string)IDheader[0]["IsInversedPrefix"]).Equals("Y");// always false for now
            int Exec = (int)IDheader[0]["Executor_Code"];

            ExFuncs.cInit(OutFile2, Exec, 0);
            ExFuncs.cSetReportType(3);
            ExFuncs.cSetKeyLabel((string)DataRow["Label"]);
            string outron = (string)DataRow["Intron"];
            if (!String.IsNullOrEmpty(outron))
                ExFuncs.cSetOutron(outron);
            LoadPrefixes(Prefixes, InvertPrefix);
            LoadData((string)DataRow["Data"], InvertData);

            ExFuncs.cTx();

            iFilesCreated++;

           

            if (bSeparateToggleOutput) {
                string[] filename = OutFile2.Split(new char[] { '.' });
                iToggleFileCount = ExFuncs.cTg() * 2; // should be power of 2 later

                // send toggle data
                if (iToggleFileCount != 0) {
                    for (int i = 1; i < iToggleFileCount; i++) {
                        ExFuncs.cResetOutputStream(filename[0] + "_" + i + ".CFI");
                        ExFuncs.cTx();
                        iFilesCreated++;
                      
                    }
                }
            }
            
            ExFuncs.cShutdown();

            //CC
            try
            {
                int iRet = 0;
                RBKSPECWRAPPERLib.IRbkSpecWrapper irb = new RBKSPECWRAPPERLib.RbkSpecWrapper();
                irb.DoRbkTranslate(OutFile2, Exec, out iRet);
                if (iRet == 1)
                {
                    string strCFTfile = Path.GetDirectoryName(OutFile2) + "\\" +
                                        Path.GetFileNameWithoutExtension(OutFile2) + ".CFT";

                    File.Copy(strCFTfile, OutFile2, true);
                    File.Delete(strCFTfile);
                }
            }
            catch (Exception exx)
            { }
            //
        
            return iFilesCreated;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IDheader"></param>
        /// <param name="Prefixes"></param>
        /// <param name="DataRow"></param>
        /// <param name="OutFile2"></param>
        public static void CreateTxFiFile(IDHeader IDheader, List<int> Prefixes,
                                        Hashtable DataRow, string OutFile2)
        {
            CreateTxFiFile(IDheader, Prefixes, DataRow, OutFile2, false);
        }

        public static int CreateTxFiFile(IDHeader IDheader, List<int> Prefixes,
                                        Hashtable DataRow, string OutFile2,
                                        bool bSeparateToggleOutput)
        {
            int iFilesCreated = 0;
            int iToggleFileCount = 0;
            bool InvertData, InvertPrefix;

            InvertData = IDheader.IsInversedData.Equals("Y");
            InvertPrefix = false;
            int Exec = IDheader.Executor_Code;

            ExFuncs.cInit(OutFile2, Exec, 0);
            ExFuncs.cSetReportType(2);  // 1:U1, 2:FI, 3: CFI
            ExFuncs.cSetKeyLabel((string)DataRow["Label"]);
            ExFuncs.cSetOutron((string)DataRow["Intron"]);
            LoadPrefixes(Prefixes, InvertPrefix);
            LoadData((string)DataRow["Data"], InvertData);

            ExFuncs.cTx();

            iFilesCreated++;

            if (bSeparateToggleOutput)
            {
                string[] filename = OutFile2.Split(new char[] { '.' });
                iToggleFileCount = ExFuncs.cTg() * 2; // should be power of 2 later

                // send toggle data
                if (iToggleFileCount != 0)
                {
                    for (int i = 1; i < iToggleFileCount; i++)
                    {
                        ExFuncs.cResetOutputStream(filename[0] + "_" +
                            i.ToString() + ".FI");
                        ExFuncs.cTx();
                        iFilesCreated++;
                    }
                }
            }

            ExFuncs.cShutdown();

            return iFilesCreated;
        }

        /// <summary>
        /// Use the execs() Tx() function to create a .FI file using 
        /// supplied information
        /// </summary>
        /// <param name="IDHeader"></param>
        /// <param name="FunctionData"></param>
        /// <param name="iPrefixList"></param>
        /// <param name="OutFile"></param>
        /// <param name="bSeparateToggleOutput"></param>
        /// <returns></returns>
        public static int CreateTxFiFile(IDictionary<string, string> IDHeader,
            IDictionary<string, string> FunctionData, List<int> iPrefixList,
            string OutFile, bool bSeparateToggleOutput)
        {
            int iFilesCreated = 0;
            int iToggleFileCount = 0;
            bool InvertData, InvertPrefix;

            if (IDHeader["IsInversedData"] == "Y")
                InvertData = true;
            else
                InvertData = false;

            //if (IDHeader["IsInversedPrefix"] == "Y")
            //    InvertPrefix = true;
            //else
            InvertPrefix = false;

            int Exec = Int32.Parse(IDHeader["Executor_Code"]);

            ExFuncs.cInit(OutFile + ".FI", Exec, 0);
            ExFuncs.cSetReportType(2);  // 1:U1, 2:FI, 3: CFI
            ExFuncs.cSetKeyLabel((string)FunctionData["Label"]);
            ExFuncs.cSetOutron((string)FunctionData["Outron"]);

            LoadPrefixes(iPrefixList, InvertPrefix);
            LoadData((string)FunctionData["DATA"], InvertData);

            ExFuncs.cTx();
            iFilesCreated++;

            iToggleFileCount = ExFuncs.cTg() * 2; // should be power of 2 later

            if (bSeparateToggleOutput)
            {
                // send toggle data
                if (iToggleFileCount != 0)
                {
                    for (int i = 1; i < iToggleFileCount; i++)
                    {
                        ExFuncs.cResetOutputStream(OutFile + "_" +
                            i.ToString() + ".FI");
                        ExFuncs.cTx();
                        iFilesCreated++;
                    }
                }
            }
            ExFuncs.cShutdown();
            return iFilesCreated;
        }

        public static int CreateTxFiFile(int exec, string label, string data, List<int> Prefixes, bool invertdata, string outputfile, bool bSeparateToggleOutput)
        {
            int iFilesCreated = 0;
            int iToggleFileCount = 0;
            bool InvertData, InvertPrefix;

            InvertData = invertdata;
            InvertPrefix = false;
            int Exec = exec;

            ExFuncs.cInit(outputfile, Exec, 0);
            ExFuncs.cSetReportType(2);  // 1:U1, 2:FI, 3: CFI
            ExFuncs.cSetKeyLabel(label);
            ExFuncs.cSetOutron(label);
            LoadPrefixes(Prefixes, InvertPrefix);
            LoadData(data, InvertData);

            ExFuncs.cTx();

            iFilesCreated++;

            if (bSeparateToggleOutput)
            {
                string[] filename = outputfile.Split(new char[] { '.' });
                iToggleFileCount = ExFuncs.cTg() * 2; // should be power of 2 later

                // send toggle data
                if (iToggleFileCount != 0)
                {
                    for (int i = 1; i < iToggleFileCount; i++)
                    {
                        ExFuncs.cResetOutputStream(filename[0] + "_" +
                            i.ToString() + ".FI");
                        ExFuncs.cTx();
                        iFilesCreated++;
                    }
                }
            }

            ExFuncs.cShutdown();

            return iFilesCreated;
        }

        /// <summary>
        /// Set prefixes in the Exec object 
        /// </summary>
        /// <param name="Prefixes"></param>
        /// <param name="InvertPrefix"></param>
        private static void LoadPrefixes(List<int> Prefixes, bool InvertPrefix)
        {
            int byt = 0;
            foreach (int Prefix in Prefixes)
            {
                if (InvertPrefix)
                    ExFuncs.cSetPrefix(~Prefix, byt++);
                else
                    ExFuncs.cSetPrefix(Prefix, byt++);
            }
        }

        /// <summary>
        /// Set data for transmission into the Exec object
        /// </summary>
        /// <param name="Data"></param>
        /// <param name="invert"></param>
        private static void LoadData(string Data, bool invert)
        {
            List<uint> value = new List<uint>();
            int byt = 0;

            ExtractData(Data, invert, value);
            foreach (int dataValue in value)
                ExFuncs.cSetData(dataValue, byt++);
        }

        /// <summary>
        /// assuming that data is in 8 bit length increments
        /// break the data into one or more 8 bit byte chunks,
        /// invert if necessary and convert to integer
        /// </summary>
        /// <param name="Data">the string data, usually in ascii binary format</param>
        /// <param name="invert">a flag used to require that the result be inverted</param>
        /// <param name="value">the result is store in this integer list</param>
        private static void ExtractData(string Data, bool invert, List<uint> value)
        {
            string DataB;
            uint data;

            if (Data.Contains("."))                         // values with a '.' are assumed to be
            {                                               // single byte floating point values
                value.Add((uint)(float.Parse(Data) * 100));  // so are converted with 2 dp precision
                return;                                     // and returned.
            }
            if (Data.Length > 32)
            {
                for (int byt = 0; (byt * 32) < Data.Length; byt++)// for each '8 bit' group...
                {
                    //DataB = Data.Substring((byt * 32), 32);     // extract the 8 bits to a seperate string
                    DataB = Data.Substring((byt * 32), (Data.Length - 32));
                    data = Convert.ToUInt32(DataB, 2);           // convert the ascii 'binary' to an integer
                    if (invert)                                 // if the data needs to be inverted,
                        data = (~data & 0xffff);                // invert and mask
                    value.Add(data);                            // and store it in the value array
                }
            }
            else
            {
                data = Convert.ToUInt32(Data, 2);            // convert the ascii 'binary' to an integer
                if (invert)                                 // if the data needs to be inverted,
                    data = (~data & 0xffff);                // invert and mask
                value.Add(data);                            // and store it in the value array
            }
        }
        #endregion

        #region Get exec list

        public static List<int> GetExecutorList(string path, bool SigngleU1,
            ref string logFileContents, Hashtable labelList,
            ProcessingForm processingForm)
        {
            if (!SigngleU1)
                return GetExecutorListFromZipFile(path,
                    ref logFileContents, labelList, processingForm);
            return GetExecutorListFromU1File(path,
                ref logFileContents, labelList, processingForm);
        }

        public static List<int> GetExecutorListFromU1File(string U1file)
        {
            List<int> executorList = new List<int>();
            string outFile = Path.GetTempFileName();

            ExFuncs.cInit(outFile, INVALID_EXEC,
                (int)ReportType.CmprsdFI);

            int[] execs = new int[MAX_EXEC];
            int count = ExFuncs.cId(U1file, execs, INVALID_EXEC,
                null, null);

            for (int j = 0; j < count; j++)
            {
                if (!executorList.Contains(execs[j]))
                    executorList.Add(execs[j]);
            }

            ExFuncs.cShutdown();

            try
            {
                File.Delete(outFile);
            }
            catch { }

            return executorList;
        }

        private static List<int> GetExecutorListFromZipFile(string filePath,
            ref string logFileContents, Hashtable labelList,
            ProcessingForm processingForm)
        {
            List<int> executorList = new List<int>();

            string[] files = Directory.GetFiles(filePath);

            if (files == null || files.Length == 0)
                return executorList;

            string outFile = Path.GetTempFileName();

            ExFuncs.cInit(outFile, INVALID_EXEC,
                (int)ReportType.CmprsdFI);

            int[] execs = new int[MAX_EXEC];

            int count = 0;

            for (int i = 0; i < files.Length; i++)
            {
                if (processingForm != null)
                    processingForm.DoEvents();
                if (!files[i].EndsWith(".u1") && !files[i].EndsWith(".U1") &&
                    !files[i].EndsWith(".u2") && !files[i].EndsWith(".U2"))
                    continue;

                logFileContents += String.Format(
                    "      Identifying executors for {0}:"
                    + "       {1}", files[i],
                    (string)labelList[GetFileName(files[i])]);
                count = ExFuncs.cId(files[i], execs, INVALID_EXEC,
                    null, null);

                logFileContents += "\r\n      Possible executors:";
                for (int j = 0; j < count; j++)
                {
                    if (!executorList.Contains(execs[j]))
                        executorList.Add(execs[j]);
                    if (j > 0 && j % 10 == 0)
                    {
                        logFileContents += "\r\n                         ";
                    }
                    logFileContents += String.Format(" {0,-3}", execs[j]);
                }

                logFileContents += "\r\n\r\n";
            }

            ExFuncs.cShutdown();

            executorList.Sort();
            return executorList;
        }

        private static List<int> GetExecutorListFromU1File(string filePath,
            ref string logFileContents, Hashtable labelList,
            ProcessingForm processingForm)
        {
            if (processingForm != null)
                processingForm.DoEvents();

            List<int> executorList = new List<int>();

            if (filePath == null)
                return executorList;

            string file = filePath.Trim();

            if (file == "")
                return executorList;

            string outFile = Path.GetTempFileName();

            logFileContents += String.Format(
                "      Identifying executors for {0}:"
                + "       {1}", file,
                (string)labelList[GetFileName(file)]);

            ExFuncs.cInit(outFile, INVALID_EXEC,
                (int)ReportType.CmprsdFI);

            int[] execs = new int[MAX_EXEC];
            int count = ExFuncs.cId(file, execs, INVALID_EXEC,
                null, null);

            logFileContents += "\r\n      Possible executors:";

            for (int j = 0; j < count; j++)
            {
                if (!executorList.Contains(execs[j]))
                    executorList.Add(execs[j]);
                if (j > 0 && j % 10 == 0)
                    logFileContents += "\r\n                         ";
                logFileContents += String.Format(" {0,-3}", execs[j]);
            }

            ExFuncs.cShutdown();

            logFileContents += "\r\n\r\n";
            executorList.Sort();

            return executorList;
        }

        public static List<int> GetAllExecutorList()
        {
            List<int> executorList = new List<int>();
            string outFile = Path.GetTempFileName();

            ExFuncs.cInit(outFile, INVALID_EXEC, (int)ReportType.CmprsdFI);

            for (int i = 0; i <= MAX_EXEC; i++)
            {
                if (ExFuncs.cSetExec(i, null) != 0)
                    executorList.Add(i);
            }

            ExFuncs.cShutdown();

            return executorList;
        }
        #endregion

        #region Time function

        /// <summary>
        /// get the time difference
        /// </summary>
        /// <param name="now"></param>
        /// <param name="createdTime"></param>
        /// <returns></returns>
        public static string GetTimeDifference(DateTime startTime, DateTime endTime)
        {
            TimeSpan diff = endTime - startTime;
            
            return String.Format("{0} Hr  {1}  Min.  {2}  Sec.",
                                diff.Hours, diff.Minutes, diff.Seconds);
        }
        #endregion

        #region Get IDList Functions

        public static StringCollection GetIDList(List<int> execList,
            string modes, StringCollection cfiFiles, 
            out StringCollection ExcludedIDList)
        {
            List<int> execNumber90 = null;
            StringCollection IDListExec90 = new StringCollection();
            StringCollection IDList = new StringCollection();
            ExcludedIDList = new StringCollection();

            if (execList.Contains(90))
            {
                execList.Remove(90);
                execNumber90 = new List<int>();
                execNumber90.Add(90);
            }

            if (execList.Count > 0)
            {
                IDList = GetIDList(execList, modes);
            }

            if (execNumber90 != null )
            {
                IDListExec90 = GetIDList(execNumber90, modes);
                IDListExec90 = ComparePrefix(IDListExec90, cfiFiles,
                    out ExcludedIDList);
                IDList.Append(IDListExec90);

                execList.Add(90);
                execList.Sort();
            }

            return IDList;
        }

        public static StringCollection GetIDList(List<int> execList,
            string modes, StringCollection brandeList,
            StringCollection cfiFiles,
            out StringCollection ExcludedIDList)
        {
            return GetIDList(execList, modes, brandeList, cfiFiles,
                out ExcludedIDList, null);
        }

        public static StringCollection GetIDList(List<int> execList,
            string modes, StringCollection brandeList,
            StringCollection cfiFiles, 
            out StringCollection ExcludedIDList, 
            StringCollection CheckedIDList)
        {
            List<int> execNumber90 = null;
            StringCollection IDListExec90 = new StringCollection();
            StringCollection IDList = new StringCollection();
            ExcludedIDList = new StringCollection();

            if (execList.Contains(90))
            {
                execList.Remove(90);
                execNumber90 = new List<int>();
                execNumber90.Add(90);
            }

            if (execList.Count > 0)
            {
                IDList = GetIDList(execList, modes, brandeList);

                if ( CheckedIDList != null)
                {
                    foreach (string id in CheckedIDList)
                    {
                        if (IDList.Contains(id))
                            IDList.Remove(id);
                    }
                }
            }

            if (execNumber90 != null)
            {
                IDListExec90 = GetIDList(execNumber90, modes, brandeList);

                if (CheckedIDList != null)
                {
                    foreach (string id in CheckedIDList)
                    {
                        if (IDListExec90.Contains(id))
                            IDListExec90.Remove(id);
                    }
                }

                IDListExec90 = ComparePrefix(IDListExec90, cfiFiles,
                    out ExcludedIDList);
                IDList.Append(IDListExec90);

                execList.Add(90);
                execList.Sort();
            }

            return IDList;
        }

        public static StringCollection GetIDList(List<int> execList,
            string modes, StringCollection brandeList,
            StringCollection regionList, StringCollection cfiFiles,
            out StringCollection ExcludedIDList)
        {
            List<int> execNumber90 = null;
            StringCollection IDListExec90 = new StringCollection();
            StringCollection IDList = new StringCollection();
            ExcludedIDList = new StringCollection();

            if (execList.Contains(90))
            {
                execList.Remove(90);
                execNumber90 = new List<int>();
                execNumber90.Add(90);
            }

            if (execList.Count > 0)
            {
                IDList = GetIDList(execList, modes, brandeList, 
                    regionList);
            }

            if (execNumber90 != null)
            {
                IDListExec90 = GetIDList(execNumber90, modes, 
                    brandeList, regionList);
                IDListExec90 = ComparePrefix(IDListExec90, cfiFiles,
                    out ExcludedIDList);
                IDList.Append(IDListExec90);

                execList.Add(90);
                execList.Sort();
            }

            return IDList;
        }

        public static StringCollection GetIDList(List<int> execList,
            string modes, StringCollection brandeList, 
            StringCollection regionList)
        {
            StringCollection modeList = null;

            if (modes != null && modes.Length > 0)
            {
                modeList = new StringCollection();
                for (int i = 0; i < modes.Length; i++)
                {
                    modeList.Add(modes.Substring(i, 1));
                }
            }

            StringCollection IDList = GetIDList(execList,
                modes, brandeList);

            if (regionList.Count == 0)
            {
                return IDList;
            }

            StringCollection QualifiedIDList = new StringCollection();
            StringCollection regionIDList = null;

            try
            {
                DAOFactory.BeginTransaction();
                if (modeList == null|| modes.Contains("ALL"))
                {
                    regionIDList =
                        DBFunctions.GetIDListByRegionList(regionList);
                }
                else
                {
                    regionIDList
                        = DBFunctions.GetIDListByRegionList(
                                regionList, modeList);
                }
                DAOFactory.CommitTransaction();
            }
            catch (Exception ex)
            {
                DAOFactory.RollBackTransaction();
                throw;
            }

            foreach (string id in IDList)
            {
                if (regionIDList.Contains(id))
                    QualifiedIDList.Add(id);
            }

            return QualifiedIDList;
        }

        public static StringCollection GetIDList(List<int> execList,
            string modes, StringCollection brandeList)
        {
            StringCollection modeList = null;

            if (modes != null && modes.Length > 0)
            {
                modeList = new StringCollection();

                if (modes.Contains("ALL"))
                    modeList.Add("ALL");
                else
                {
                    for (int i = 0; i < modes.Length; i++)
                    {
                        modeList.Add(modes.Substring(i, 1));
                    }
                }
            }

            StringCollection IDList = GetIDList(execList, modeList);

            if (brandeList.Count == 0)
            {
                return IDList;
            }

            StringCollection QualifiedIDList = new StringCollection();
            StringCollection brandIDList = null;

            try
            {
                DAOFactory.BeginTransaction();
                if (modeList == null|| modes.Contains("ALL"))
                {
                    brandIDList =
                        DBFunctions.GetIDListByBrandList(brandeList);
                }
                else
                {
                    brandIDList
                        = DBFunctions.GetIDListByBrandList(
                                brandeList, modeList);
                }
                DAOFactory.CommitTransaction();
            }
            catch (Exception ex)
            {
                DAOFactory.RollBackTransaction();
                throw;
            }

            foreach (string id in IDList)
            {
                if (brandIDList.Contains(id))
                    QualifiedIDList.Add(id);
            }

            return QualifiedIDList;
        }

        public static StringCollection GetIDList(List<int> execList,
            string modes)
        {
            StringCollection modeList = null;

            if (modes != null && modes.Length > 0)
            {
                modeList = new StringCollection();
                for (int i = 0; i < modes.Length; i++)
                {
                    modeList.Add(modes.Substring(i, 1));
                }
            }

            return GetIDList(execList, modeList);
        }

        private static StringCollection GetIDList(List<int> execList,
            StringCollection modeList)
        {
            StringCollection IDList = null;

            try
            {
                DAOFactory.BeginTransaction();

                if (modeList == null || modeList.Contains("ALL"))
                {
                    IDList =
                        DBFunctions.GetIDListByExecutorList(execList);
                }
                else
                {
                    IDList
                        = DBFunctions.GetIDListByExecutorList(
                                modeList, execList);
                }

                DAOFactory.CommitTransaction();
            }
            catch (Exception ex)
            {
                DAOFactory.RollBackTransaction();
                throw;
            }
            return IDList;
        }

        public static StringCollection ComparePrefix(StringCollection IDList,
            StringCollection cfiFiles, out StringCollection ExculdedIDList)
        {
            StringCollection newIDList = new StringCollection();
            int iter = 0; ;
            CFIReader cfiReader = new CFIReader();
            string RXData = "";
            Hashtable RXDataTable = new Hashtable();
            ExculdedIDList = new StringCollection();

            foreach (string id in IDList)
            {
                HashtableCollection IDheader = FunctionList.GetDBdata(id, "AdHoc.GetIDHeader");
                List<int> Prefixes = FunctionList.CollectPrefixes(id, IDheader);
                int prefixData = GetPrefix(Prefixes);

                if (prefixData == -1)
                {
                    newIDList.Add(id);
                    continue;
                }

                if (prefixData == -2)
                {
                    MessageBox.Show("Prefixes of " + id + " does not meet"
                        + " the requirement of E90");
                    continue;
                }

                foreach (string OutFile1 in cfiFiles)
                {
                    RXData = (string)RXDataTable[OutFile1];

                    if (RXData == null)
                    {
                        RXData = cfiReader.ReadCFIFile(OutFile1);
                        RXDataTable[OutFile1] = RXData;
                    }

                    int first16bits;
                    if (RXData.Length <= 22)
                    {
                        first16bits = -1;
                    }
                    else
                    {
                        string s = RXData.Substring(0, 22);
                        if (RXDataTable.ContainsKey(s))
                            first16bits = (int)RXDataTable[s];
                        else
                        {
                            first16bits = GetFirst16Bits(s);
                            RXDataTable[s] = first16bits;
                        }
                    }

                    if (first16bits != prefixData)
                    {
                        if (first16bits != -1 && iter > 0
                            && RXDataTable.Count
                            == (cfiFiles.Count + 1))
                        {
                            break;
                        }
                    }
                    else
                    {
                        newIDList.Add(id);
                        break;
                    }
                }

                if (!newIDList.Contains(id))
                    ExculdedIDList.Add(id);
                iter++;
            }
            return newIDList;
        }

        private static int GetFirst16Bits(string RXData)
        {
            string s = RXData.Replace("FD", "");
            s = s.Replace("B", "");
            s = s.Replace(" ", "");

            if (s.Length < 16)
                return -1;

            s = s.Substring(0, 16);
            int p = 1, value = 0;

            for (int i = 15; i >= 0; i--)
            {
                if (s[i] == '1')
                    value += p;
                p *= 2;
            }
            return value;
        }

        private static int GetPrefix(List<int> Prefixes)
        {
            int prefix1 = 0, prefix2 = 0;

            if (Prefixes.Count < 2)
                return -1;

            prefix1 = Prefixes[1];

            if ((Prefixes[0] & 0x20) != 0)
            {
                if (Prefixes.Count < 3)
                    return -2;

                prefix2 = Prefixes[2];
                if ((Prefixes[0] & 0x04) != 0)
                {
                    prefix1 = prefix1 ^ 0x80;
                    prefix2 = prefix2 ^ 0x80;
                }
            }
            else
            {
                prefix2 = ~prefix1 & 0xff;
                if ((Prefixes[0] & 0x04) != 0)
                {
                    if (Prefixes.Count < 3)
                        return -2;

                    prefix1 = Prefixes[2];
                    prefix2 = ~prefix1 & 0xff;
                }
            }

            return (prefix1 & 0xff) << 8
                | (prefix2 & 0xff);
        }

        #endregion

        #region Get Intron List from Dictionary

        public static void InitIntronTable()
        {
            if ( DicIntronList == null )
                DicIntronList = new Hashtable();
            else DicIntronList.Clear();
        }

        public static Hashtable GetIntronList(Hashtable labelList, string id)
        {
            string mode = id.Substring(0, 1);
            string path = DICPath + id.Substring(0, 1) + ".dic";

            Hashtable tabel = (Hashtable)DicIntronList[mode];

            if (tabel != null)
                return tabel;

            Hashtable intronList = new Hashtable();

            if (File.Exists(path))
                ScanDICFile(labelList, ref intronList, path);

            if (intronList.Count < labelList.Count)
            {
                path = DICPath + "g.dic";
                if (File.Exists(path))
                    ScanDICFile(labelList, ref intronList, path);
            }

            DicIntronList[mode] = intronList;
            return intronList;
        }

        public static string GetIntron(string label, string id)
        {
            string path = DICPath + id.Substring(0, 1) + ".dic";

            string intron = ScanDICFile(label, path);

            if (intron == null)
            {
                path = DICPath + "g.dic";
                intron = ScanDICFile(label, path);
            }

            return intron;
        }

        private static void ScanDICFile(Hashtable labelList,
            ref Hashtable intronList, string path)
        {
            StreamReader stream = new StreamReader(path,
                Encoding.ASCII);

            string buf;

            while ((buf = stream.ReadLine()) != null)
            {
                string[] strs = buf.Split(new string[] { "  " },
                    StringSplitOptions.RemoveEmptyEntries);

                if (strs == null || strs.Length < 2)
                    continue;

                string label = strs[0].Trim();

                if (!labelList.ContainsValue(label))
                    continue;

                if (intronList.ContainsKey(label))
                    continue;

                string intron = strs[1].Trim();

                if (Char.IsLetter(intron[0])
                    && Char.IsDigit(intron[1]))
                {
                    if (strs.Length < 3)
                        continue;
                    intron = strs[2].Trim();
                }

                int i = 0;

                while (i < intron.Length && !Char.IsLetter(intron[i]))
                    i++;

                intron = intron.Substring(i);

                intronList[label] = intron;

                if (intronList.Count == labelList.Count)
                    break;
            }

            stream.Close();
        }

        private static string ScanDICFile(string label, string path)
        {
            FileInfo fileInfo = new FileInfo(path);
            StreamReader stream = fileInfo.OpenText();

            string buf;

            while ((buf = stream.ReadLine()) != null)
            {
                string[] strs = buf.Split(new string[] { "  " },
                    StringSplitOptions.RemoveEmptyEntries);

                if (strs == null || strs.Length < 3)
                    continue;

                if (label != strs[0].Trim())
                    continue;

                string intron = strs[1].Trim();

                if (Char.IsLetter(intron, 0))
                    intron = strs[2].Trim();

                int i = 0;

                while (i < intron.Length && Char.IsNumber(intron[i]))
                    i++;

                if (intron[i] == '.')
                    i++;

                intron = intron.Substring(i);

                stream.Close();
                return intron;
            }

            stream.Close();
            return null;
        }

        #endregion

        #region CFI files

        public static string ReadCFIFile(string file)
        {
            CFIReader cfiReader = new CFIReader();
            return cfiReader.ReadCFIFile(file);
        }

        public static StringCollection GetCfiFiles(string[] files, int exec,
            string outputPath, string id, Hashtable LabelList,
            Hashtable OutronList)
        {
            StringCollection cfiFiles = new StringCollection();

            if (files == null || files.Length == 0)
                return cfiFiles;

            int j = 0;
            string path = files[0].Substring(
                0, files[0].LastIndexOf("\\") + 1)
                + String.Format("E{0:000}", exec) + "\\";

            if (Directory.Exists(path))
            {
                string[] filesInPath = Directory.GetFiles(path);

                if (filesInPath != null && filesInPath.Length != 0)
                {
                    for (int i = 0; i < filesInPath.Length; i++)
                    {
                        if (filesInPath[i].ToUpper().EndsWith(".CFI"))
                            cfiFiles.Add(filesInPath[i]);
                    }
                }
                return cfiFiles;
            }

            Directory.CreateDirectory(path);

            if ( !outputPath.EndsWith("\\"))
                outputPath += "\\";

            for (int i = 0; i < files.Length; i++)
            {
                if (!files[i].Contains(".u1") && !files[i].Contains(".U1") &&
                    !files[i].Contains(".u2") && !files[i].Contains(".U2"))
                {
                    continue;
                }

                if (OutronList[Path.GetFileName(files[i])] == null)
                    continue;

                string cfiFile = outputPath + id + "_"
                    + (string)OutronList[Path.GetFileName(files[i])] 
                    + "#.CFI";

                cfiFile = ValidateFileName(cfiFile);

                string label = (string)LabelList[Path.GetFileName(cfiFile)];

                if (label == null)
                    label = "test key";

                try
                {
                    ConvertCaptureToCfiFile(cfiFile, files[i],
                        exec, label);
                }
                catch (Exception ex)
                {
                    continue;
                }
                cfiFiles.Add(cfiFile);
                j++;
            }

            return cfiFiles;
        }

        //Start Add 20120621 Warning for 'Exec Not Implemented'
        //GetCfiFiles Function Over Loaded
        public static StringCollection GetCfiFiles(string[] files, int exec,
           string outputPath, string id, Hashtable LabelList,
           Hashtable OutronList, out int iExecStatus)
        {
            iExecStatus = 1;
            StringCollection cfiFiles = new StringCollection();

            if (files == null || files.Length == 0)
                return cfiFiles;

            int j = 0;
            string path = files[0].Substring(
                0, files[0].LastIndexOf("\\") + 1)
                + String.Format("E{0:000}", exec) + "\\";

            if (Directory.Exists(path))
            {
                string[] filesInPath = Directory.GetFiles(path);

                if (filesInPath != null && filesInPath.Length != 0)
                {
                    for (int i = 0; i < filesInPath.Length; i++)
                    {
                        if (filesInPath[i].ToUpper().EndsWith(".CFI"))
                            cfiFiles.Add(filesInPath[i]);
                    }
                }
                return cfiFiles;
            }

            Directory.CreateDirectory(path);

            if (!outputPath.EndsWith("\\"))
                outputPath += "\\";

            for (int i = 0; i < files.Length; i++)
            {
                if (!files[i].Contains(".u1") && !files[i].Contains(".U1") &&
                    !files[i].Contains(".u2") && !files[i].Contains(".U2"))
                {
                    continue;
                }

                if (OutronList[Path.GetFileName(files[i])] == null)
                    continue;

                string cfiFile = outputPath + id + "_"
                    + (string)OutronList[Path.GetFileName(files[i])]
                    + "#.CFI";

                cfiFile = ValidateFileName(cfiFile);

                string label = (string)LabelList[Path.GetFileName(cfiFile)];

                if (label == null)
                    label = "test key";

                try
                {
                    ConvertCaptureToCfiFile(cfiFile, files[i],
                        exec, label);
                }
                catch (Exception ex)
                {
                    continue;
                }
                cfiFiles.Add(cfiFile);
                j++;
            }

            iExecStatus = _iEXEC_STATUS;
            return cfiFiles;
        }
        //End Add 20120621

        public static StringCollection GetCfiFiles(string[] files, int exec)
        {
            string logFileContents = "";
            List<string> skipedFiles;
            return GetCfiFiles(files, exec, null, ref logFileContents, 
                out skipedFiles);
        }

        public static StringCollection GetCfiFiles(string[] files, int exec, string genericCfgPath,
            Hashtable labelList, ref string logFileContents, out List<string> skipedFiles) {
            StringCollection cfiFiles = new StringCollection();
            skipedFiles = new List<string>();

            if (files == null || files.Length == 0)
                return cfiFiles;

            int j = 0;
            string path = files[0].Substring(
                0, files[0].LastIndexOf("\\") + 1)
                + String.Format("E{0:000}", exec) + "\\";

            if (Directory.Exists(path)) {
                string[] filesInPath = Directory.GetFiles(path);

                if (filesInPath != null && filesInPath.Length != 0) {
                    for (int i = 0; i < filesInPath.Length; i++) {
                        if (filesInPath[i].ToUpper().EndsWith(".CFI"))
                            cfiFiles.Add(filesInPath[i]);
                    }
                }
                return cfiFiles;
            }

            Directory.CreateDirectory(path);

            for (int i = 0; i < files.Length; i++) {
                if (!files[i].Contains(".u1") && !files[i].Contains(".U1") &&
                    !files[i].Contains(".u2") && !files[i].Contains(".U2")) {
                    continue;
                }
                string cfiFile = path + GetFileName(files[i])
                    + ".cfi";

                try {
                    const int GENERIC_EXEC = 998;
                    if (exec == GENERIC_EXEC && !string.IsNullOrEmpty(genericCfgPath)) {
                        ConvertCaptureToCfiFile(cfiFile, files[i], exec, genericCfgPath, labelList);
                    } else {
                        ConvertCaptureToCfiFile(cfiFile, files[i], exec, labelList);
                    }
                } catch (Exception ex) {
                    if (labelList != null) {
                        if (skipedFiles.Count == 0)
                            logFileContents += "\r\n";
                        logFileContents += String.Format("\r\n{0} with E{1:000} is skipped due to RX() error!",
                                                         files[i], exec);
                        skipedFiles.Add(String.Format("{0} is skipped due to RX() error!", files[i]));
                    }
                    continue;
                }
                cfiFiles.Add(cfiFile);
                j++;
            }

            return cfiFiles;
        }

        public static StringCollection GetCfiFiles(string[] files, int exec,
            Hashtable labelList, ref string logFileContents, out List<string> skippedFiles)
        {
            return GetCfiFiles(files, exec, "", labelList, ref logFileContents, out skippedFiles);
        }

        public static List<string> CreateTxCfiFiles(PickID pickID, 
            string path)
        {
            if ( !path.EndsWith("\\") )
            {
                path += "\\";
            }

            List<int> PrefixList = CollectPrefixes(pickID.OID.Header);

            List<string> CFIFiles = new List<string>();

            if (pickID.DataMapList != null)
            {
                foreach (DataMap dataMap in pickID.DataMapList)
                {
                    if (String.IsNullOrEmpty(dataMap.Data))
                    {
                        continue;
                    }

                    Hashtable DataRow = new Hashtable();
                    DataRow["Data"] = dataMap.Data;
                    DataRow["Label"] = dataMap.KeyLabel;
                    DataRow["Intron"] = dataMap.Outron;

                    //replace illegal charators for file names
                    string key = dataMap.Outron;
                    key = ValidateFileName(key);

                    string outputFile = path + "\\"
                        + pickID.OID.Header.ID + "_" + key + ".CFI";

                    CFIFiles.Add(pickID.OID.Header.ID + "_" + key + ".CFI");
                    CreateTxCfiFile(pickID.OID.Header, PrefixList, 
                        DataRow, outputFile);
                }
            }

            return CFIFiles;
        }

        public static List<string> CreateTxCfiFiles(PickID pickID,
            string path, out Hashtable intronTable, out Hashtable labelTable,
            out Hashtable intronPriorityTable, out Hashtable dataTable)
        {
            return CreateTxCfiFiles(pickID, path, false, out intronTable, out labelTable, out intronPriorityTable, out dataTable);
        }

        public static List<string> CreateTxCfiFiles(PickID pickID,
            string path, bool generateToggleFiles, out Hashtable intronTable, out Hashtable labelTable,
            out Hashtable intronPriorityTable, out Hashtable dataTable)
        {
            if (!path.EndsWith("\\"))
            {
                path += "\\";
            }

            List<int> PrefixList = CollectPrefixes(pickID.OID.Header);

            List<string> CFIFiles = new List<string>();

            intronTable = new Hashtable();
            labelTable = new Hashtable();
            intronPriorityTable = new Hashtable();
            dataTable = new Hashtable();

            if (pickID.DataMapList != null)
            {
                foreach (DataMap dataMap in pickID.DataMapList)
                {
                    if (String.IsNullOrEmpty(dataMap.Data))
                    {
                        continue;
                    }

                    string labelString = "";

                    if (dataMap.LabelList != null
                        && dataMap.LabelList.Count > 0)
                        labelString = dataMap.LabelList[0];

                    Hashtable DataRow = new Hashtable();
                    DataRow["Data"] = dataMap.Data;
                    DataRow["Label"] = labelString;
                    DataRow["Intron"] = dataMap.Outron;

                    //replace illegal charators for file names
                    string key = dataMap.Outron;
                    key = ValidateFileName(key);

                    string outputFile = path + "\\"
                        + pickID.OID.Header.ID + "_" + key + ".CFI";

                    CFIFiles.Add(pickID.OID.Header.ID + "_" + key + ".CFI");
                    CreateTxCfiFile(pickID.OID.Header, PrefixList,
                        DataRow, outputFile, generateToggleFiles);

                    dataTable[pickID.OID.Header.ID + "_"  + key + ".CFI"]
                        = dataMap.Data;

                    if (!intronTable.Contains(dataMap.Data))
                    {
                        StringCollection intronList = new StringCollection();
                        StringCollection labelList = new StringCollection();
                        StringCollection intronPriorityList
                            = new StringCollection();

                        intronList.Add((string)dataMap.Intron);
                        labelList.Append(dataMap.LabelList);
                        intronPriorityList.Add(
                            (string)dataMap.IntronPriority);

                        intronTable[dataMap.Data] = intronList;
                        labelTable[dataMap.Data] = labelList;
                        intronPriorityTable[dataMap.Data] = intronPriorityList;
                    }
                    else
                    {
                        ((StringCollection)intronTable[dataMap.Data]).Add(
                            (string)dataMap.Intron);
                        foreach (string label in dataMap.LabelList)
                        {
                            if ( !((StringCollection)labelTable[dataMap.Data])
                                .Contains(label))
                                ((StringCollection)labelTable[dataMap.Data])
                                    .Add(label);
                        }
                        ((StringCollection)intronPriorityTable[dataMap.Data]).
                            Add((string)dataMap.IntronPriority);
                    }
                }
            }

            return CFIFiles;
        }

        public static string MakeCFIFilename(string id, string outron, string path)
        {
            if (String.IsNullOrEmpty(path))
                path = "";
            else if (path.EndsWith("\\") == false)
                path += "\\";

            //replace illegal charators for file names
            string key = outron.ToUpper();
            key = ValidateFileName(key);

            return path + id.ToUpper() + "_" + key + ".CFI";
        }

        #endregion

        #region UnZip Files

        public static string UnzipTNZipFile(string filePath, string TNZipFile)
        {
            if (!filePath.EndsWith("\\"))
            {
                filePath += "\\";
            }

            int i = TNZipFile.LastIndexOf('\\'), 
                j = TNZipFile.LastIndexOf('.');

            filePath += TNZipFile.Substring(i + 1, j - i - 1) + "\\";

            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);
            else
            {
                Directory.Delete(filePath, true);
                Directory.CreateDirectory(filePath);
            }

            ZipHelper.ExtractAll(TNZipFile, filePath);
            return filePath;
        }

        #endregion

        #region Intron and Label functions

        public static Hashtable GetLabels(string PrjFile)
        {
            Hashtable labelList = new Hashtable();

            if (!File.Exists(PrjFile))
            {
                return labelList;
            }

            StreamReader stream = new StreamReader(PrjFile);

            string buf;

            while ((buf = stream.ReadLine()) != null)
            {
                if (!buf.Contains(".u1") && !buf.Contains(".U1") &&
                    !buf.Contains(".u2") && !buf.Contains(".U2"))
                    continue;

                string U1File = "";
                string label = "";
                if (!ExtractLabel(buf, out U1File, out label))
                    continue;

                labelList[U1File] = label;
                ChedkForMultipleLabels(labelList, U1File, label);
                //if (label.Contains("|"))
                //{
                //    string[] labels = label.Split(new string[] { "|" },
                //        StringSplitOptions.RemoveEmptyEntries);
                //    if (labels == null || labels.Length < 1)
                //        continue;

                //    for (int i = 0; i < labels.Length; i++)
                //        labelList[U1File + i] = labels[i];
                //}
            }

            stream.Close();
            return labelList;
        }

        public static Hashtable GetLabels(string PrjFile, string id,
            out Hashtable OutronList)
        {
            Hashtable labelList = new Hashtable();
            OutronList = new Hashtable();

            if (!File.Exists(PrjFile))
            {
                return labelList;
            }

            StreamReader stream = new StreamReader(PrjFile);

            string buf;

            while ((buf = stream.ReadLine()) != null)
            {
                if (!buf.Contains(".u1") && !buf.Contains(".U1") &&
                    !buf.Contains(".u2") && !buf.Contains(".U2"))
                    continue;

                string U1File = "";
                string label = "";
                if (!ExtractLabel(buf, out U1File, out label))
                    continue;

                int start = buf.LastIndexOf("(");
                int end = buf.LastIndexOf(")");
 
                OutronList[U1File] = buf.Substring(start + 1, end - start - 1);

                labelList[id + "_" + (string)OutronList[U1File] + "#.CFI"] = label;

                ChedkForMultipleLabels(labelList, U1File, label);
            }

            stream.Close();
            return labelList;
        }

        public static Hashtable GetLabels(string PrjFile, string id,
            out Hashtable OutronList, out string[] fileList)
        {
            Hashtable labelList = new Hashtable();
            List<string> cfiFileNames = new List<string>();
            OutronList = new Hashtable();
            fileList = null;

            if (!File.Exists(PrjFile))
            {
                return labelList;
            }

            StreamReader stream = new StreamReader(PrjFile);

            string buf;

            while ((buf = stream.ReadLine()) != null)
            {
                if (!buf.Contains(".u1") && !buf.Contains(".U1") &&
                    !buf.Contains(".u2") && !buf.Contains(".U2"))
                    continue;

                string U1File = "";
                string label = "";
                if (!ExtractLabel(buf, out U1File, out label))
                    continue;

                int start = buf.LastIndexOf("(");
                int end = buf.LastIndexOf(")");


                //Begin: PAL Temp Fix 
                //Old code
                //OutronList[U1File] = buf.Substring(start + 1, end - start - 1);
                //New code
                string tempStrOutron = buf.Substring(start + 1, end - start - 1);
                string strOutronMapFile = "FWL-2-Outron.txt";
                if (File.Exists(strOutronMapFile))
                {
                    StreamReader streamReader = new StreamReader(strOutronMapFile);
                    String line;
                    try
                    {
                        while ((line = streamReader.ReadLine()) != null)
                        {
                            line = line.Trim();
                            if (line.EndsWith(tempStrOutron))
                                break;
                        }
                        if (line != null)
                            tempStrOutron = line.Replace(tempStrOutron, "");
                      tempStrOutron =  tempStrOutron.Replace("\t", "");
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        streamReader.Close();
                    }
                }
                OutronList[U1File] = tempStrOutron;
                //End: PAL temp fix
               
                string cfiFileName = id + "_" + (string)OutronList[U1File] + "#.CFI";
                cfiFileName = ValidateFileName(cfiFileName);

                labelList[cfiFileName] = label;
                cfiFileNames.Add(cfiFileName);

                ChedkForMultipleLabels(labelList, U1File, label);
            }

            stream.Close();
            fileList = cfiFileNames.ToArray();
            return labelList;
        }

        private static string ValidateFileName(string cfiFileName) {
            cfiFileName = cfiFileName.Replace("+", "@");
            cfiFileName = cfiFileName.Replace('<', '&');
            cfiFileName = cfiFileName.Replace('>', '!');
            return cfiFileName;
        }

        private static void ChedkForMultipleLabels(Hashtable labelList, string U1File, string label)
        {
            if (label.Contains("|"))
            {
                string[] strs = label.Split(new string[] { "|" },
                    StringSplitOptions.RemoveEmptyEntries);
                if (strs != null && strs.Length >= 1)
                    for (int i = 0; i < strs.Length; i++)
                        labelList[U1File + i] = strs[i];
            }
        }

        private static bool ExtractLabel(string buf, out string U1File, out string label)
        {
            U1File = "";
            label = "";
            if (!buf.Contains(":"))
                return false;

            U1File = buf.Substring(0, buf.IndexOf(':')).ToUpper().Trim();
            label = buf.Substring(buf.IndexOf(':') + 1).Trim();
             
            if (label.Contains("(") && label.Length > label.LastIndexOf('(') + 4)
            {
                int paren = label.LastIndexOf('(');
                if (paren > 1 && label[paren + 4].Equals(')'))
                    label = label.Substring(0, label.LastIndexOf('(') - 1).Trim();
            }
            return true;
        }

        public static string IntronMatch(string rbCfiFile, string data,
            Hashtable rbIntronList, Hashtable rbLabelList,
            Hashtable pickIntronList)
        {
            if (rbCfiFile == null || data == null)
                return null;

            int i = rbCfiFile.LastIndexOf('\\');

            string fileName = rbCfiFile;

            if (i > 0)
                fileName = fileName.Substring(i + 1);
            fileName = fileName.Replace(".cfi", "");

            string label = (string)rbLabelList[fileName];

            if (label == null)
                return null;

            string intron = (string)rbIntronList[label];

            StringCollection introns
                = (StringCollection)pickIntronList[data];

            if (introns == null)
                return null;

            if (introns.Contains(intron))
                return intron;
            
            if (label.Contains("|"))
            {
                string[] strs = label.Split(new string[] { "|" }, 
                    StringSplitOptions.RemoveEmptyEntries);
                if (strs == null || strs.Length == 0)
                    return null;
                for (i = 0; i < strs.Length; i++)
                {
                    intron
                        = (string)rbIntronList[strs[i].Trim()];
                    if (introns.Contains(intron))
                        return intron;
                }
            }

            return null;
        }


        public static void ConvertCaptureToFI(string OutFile1,
           string RxFile, int Exec)
        {
            ExFuncs.cInit(OutFile1, Exec, 0);
            //ExFuncs.cSetKeyLabel(label);
            ExFuncs.cSetReportType(2);
            ExFuncs.cRx(RxFile);
            ExFuncs.cShutdown();
        }
        #endregion
    }
}
