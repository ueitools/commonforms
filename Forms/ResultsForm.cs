using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;
using CommonForms.Forms;

namespace CommonForms
{
    #region Data Type Definitions
    public enum ReportStyle : int
    {
        TOP5,
        TOP10,
        ALL
    }

    public struct CompareResults
    {
        public string captureFile;
        public string ID;
        public string exec;
        public int dataMatch;
        public int intronMatch;
        public string outputFilePath;
        //Added by Venki on Oct 07 2010 for Intron Enhancement Requirement --Start
        public int ConflictCount;
        //Added by Venki on Oct 07 2010 for Intron Enhancement Requirement --End

    }

    #endregion

    public partial class ResultsForm : Form
    {
        const int ERROR_NO_ASSOCIATION = 1155;

        List<CompareResults> results = null;
        ReportStyle reportStyle;
        string WORKDIR = "";
        private string contents = "";
        bool loadFromFile = true;
        //For Sorting- Start
        bool multipleTNS = false;
        //For Sorting - End

        public ResultsForm()
        {
            InitializeComponent();
            WORKDIR = Configuration.GetWorkingDirectory();
            toolStripNewFuncNewSigns.Enabled = false;
        }

        public ResultsForm(ReportStyle reportStyle,
            List<CompareResults> results)
            : this()
        {
            results = rearrangeResult(results);
            this.results = results;
            this.reportStyle = reportStyle;
            loadFromFile = false;
            toolStripNewFuncNewSigns.Enabled = true;

        }

        private List<CompareResults> rearrangeResult(List<CompareResults> results)
        {
            List<CompareResults> re_results = new List<CompareResults>();
            for (int i = 0; i < results.Count; i++)
            {
                for (int j = i + 1; j < results.Count; j++)
                {
                    if (results[i].captureFile != results[j].captureFile)
                    {
                        multipleTNS = true;
                    }
                    else
                    {
                        if (results[i].intronMatch < results[j].intronMatch)
                        {
                            CompareResults temp = new CompareResults();
                            temp = results[j];
                            results[j] = results[i];
                            results[i] = temp;

                        }
                    }

                }
            }
            //For Sorting - Start
            if (multipleTNS)
            {
                reportDataGrid.Columns["Freames"].SortMode = DataGridViewColumnSortMode.NotSortable;
                reportDataGrid.Columns["Intron"].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            else
            {
                reportDataGrid.Columns["Freames"].SortMode = DataGridViewColumnSortMode.Automatic;
                reportDataGrid.Columns["Intron"].SortMode = DataGridViewColumnSortMode.Automatic;

            }
            //For Sorting - End

            return results;
        }

        private int minCount(List<CompareResults> results)
        {
            int min = results[0].ConflictCount;
            for (int i = 0; i < results.Count; i++)
            {
                if (results[i].ConflictCount < min)
                {
                    min = results[i].ConflictCount;
                }
            }

            return min;
        }
        private void OK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ResultsForm_Shown(object sender, EventArgs e)
        {
            this.Update();
            string capturefile = null;
            bool writeToFile = true;

            int num = 0, MAX = 0;

            if (loadFromFile)
            {
                MAX = -1;
                writeToFile = false;

                if (results == null)
                {
                    this.Text = WORKDIR + "result.rpt";
                    // Modified for Sorting - Venki - Start
                    //results = GetResult();
                    results = rearrangeResult(GetResult());
                    // Modified for Sorting - Venki - End
                }
            }
            else
            {
                if (reportStyle == ReportStyle.ALL)
                    MAX = -1;
                else if (reportStyle == ReportStyle.TOP5)
                    MAX = 5;
                else if (reportStyle == ReportStyle.TOP10)
                    MAX = 10;
            }

            //Added by Venki for Intron Enhancement Requirement --Start
            this.Text = WORKDIR + "result.rpt";
            //if (results.Count > 0)
            //{
            //    this.Text = WORKDIR + " result.rpt for ";// +results[0].captureFile;
            //}
            //Added by Venki for Intron Enhancement Requirement --End

            contents += "TN comparison result:\r\n\r\n";
            contents += String.Format("{0,-18}   {1,-15}{2,-18}{3,-18}",
                "Capture File", "ID(Exec)", "Data Match",
                "Intron Match\r\n");
            contents += "-------------------------------------------------------------------\r\n";

            foreach (CompareResults res in results)
            {
                if (res.captureFile != capturefile)
                {
                    num = 1;
                    capturefile = res.captureFile;

                    if (res.dataMatch > 0)
                    {
                        contents += String.Format("{0,-18}\r\n", capturefile + ":");
                        contents += String.Format("{0,-18}{1,-18}    {2,-14}    {3,-14}\r\n", "",
                            res.ID + "(" + res.exec + ")",
                            "" + res.dataMatch + "%", "" + res.intronMatch + "%");
                        //Venki - Start
                        //reportDataGrid.Rows.Add(capturefile + ":", "", "", "");

                        //reportDataGrid.Rows.Add("", res.ID + "(" + res.exec + ")",
                        //"" + res.dataMatch + "%", "" + res.intronMatch + "%");

                        reportDataGrid.Rows.Add(res.captureFile, res.ID + "(" + res.exec + ")",
                        res.dataMatch, res.intronMatch, res.outputFilePath, "View Details");
                        //Venki - End
                    }
                    else
                    {
                        contents += String.Format("{0,-18}\r\n", capturefile + ":");
                        contents += String.Format("{0,-18}{1,-18}    {2,-14}    {3,-14}\r\n", "",
                            "No match found", "", "");
                        //Changed - Venki
                        //reportDataGrid.Rows.Add(capturefile + ":", "", "", "");
                        //reportDataGrid.Rows.Add("", "No match found", "", "");
                        reportDataGrid.Rows.Add(capturefile + ":", "No match found", "", "", "", "View Details");
                        //reportDataGrid.Rows.Add("", "No match found", "", "");

                        //Changed - End
                    }
                }
                else
                {
                    if (MAX > 0 && num >= MAX)
                        continue;
                    num++;
                    contents += String.Format("{0,-18}{1,-18}    {2,-14}    {3,-14}\r\n", "",
                        res.ID + "(" + res.exec + ")",
                        "" + res.dataMatch + "%", "" + res.intronMatch + "%");
                    //Venki - Start
                    //reportDataGrid.Rows.Add("", res.ID + "(" + res.exec + ")",
                    //"" + res.dataMatch + "%", "" + res.intronMatch + "%");

                    reportDataGrid.Rows.Add(res.captureFile, res.ID + "(" + res.exec + ")",
                        res.dataMatch, res.intronMatch, res.outputFilePath, "View Details");
                    //Venki - End
                }
            }

            if (writeToFile)
                File.WriteAllText(WORKDIR + "result.rpt", contents);
        }

        private void report_CellDoubleClick(object sender,
            DataGridViewCellEventArgs e)
        {
            Process process = new Process();
            try
            {
                process.StartInfo.Verb = "";
                process.StartInfo.CreateNoWindow = true;
                if (e.RowIndex < 0
    || e.RowIndex >= reportDataGrid.RowCount)
                    return;

                IrSearchResultViewer irresultview;
                if (e.RowIndex > -1)
                {
                    if (e.ColumnIndex == 0)
                    {
                        //string fileName = results[0].captureFile;
                        string fileName = reportDataGrid[0, e.RowIndex].Value.ToString();
                        fileName = fileName.Replace(":", "");
                        if (fileName.Contains(".ZIP"))
                            fileName = fileName.Replace(".ZIP", ".LOG");
                        else
                        {
                            fileName = fileName.Replace(".U1", ".LOG");
                            fileName = fileName.Replace(".U2", ".LOG");
                        }
                        // open the file
                        process.StartInfo.FileName = WORKDIR + fileName;
                        process.Start();
                        //fileName = WORKDIR + fileName;
                        //irresultview = new IrSearchResultViewer(fileName, string.Empty);
                        //irresultview.Text += " [" + Path.GetFileName(fileName) + "]";
                        //irresultview.Show();

                        return;
                    }
                    else
                    {
                        if ((string)reportDataGrid[1, e.RowIndex].Value
                            == "No match found")
                        {
                            return;
                        }
                        string fileName = reportDataGrid[4, e.RowIndex].Value.ToString();
                        //process.Start();
                        if (!string.IsNullOrEmpty(fileName))
                        {
                            //irresultview = new IrSearchResultViewer(fileName, string.Empty);
                            //irresultview.Text += " [" + Path.GetFileName(fileName) + "]";
                            //irresultview.Show();
                            if (Path.GetExtension(fileName) == ".QA" ||
               Path.GetExtension(fileName) == ".UEI")
                            {
                                ProcessStartInfo procInfo = new ProcessStartInfo("notepad.exe", fileName);
                                procInfo.WindowStyle = ProcessWindowStyle.Normal;
                                Process.Start(procInfo);
                            }
                        }

                    }
                }

            }
            catch (Win32Exception ex)
            {
                if (ex.NativeErrorCode == ERROR_NO_ASSOCIATION)
                {
                    string notepad_path = "notepad.exe";
                    //Process.Start(notepad_path,
                    //    process.StartInfo.FileName);
                }
                else MessageBox.Show(ex.Message);
            }
        }



        private void report_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex > -1) && (e.ColumnIndex > -1))
            {
                if (reportDataGrid.Columns[e.ColumnIndex].GetType().Equals(typeof(DataGridViewButtonColumn)))
                {
                    DataGridViewButtonCell bc = (DataGridViewButtonCell)reportDataGrid.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    string fileName = reportDataGrid[4, e.RowIndex].Value.ToString();
                    if (!string.IsNullOrEmpty(fileName))
                    {
                        string id = reportDataGrid[1, e.RowIndex].Value.ToString();
                        IrSearchResultViewer irresultview = new IrSearchResultViewer(fileName + ".xml", getDBType(id));
                        string resultData = irresultview.ResultData;
                        irresultview.Dispose();
                        showResult(resultData);
                        //irresultview.Text += " [" + Path.GetFileName(fileName) + "]";
                        //irresultview.Show();                    }
                    }
                }
            }
        }

        //
        private void showResult(string resultData)
        {
            //System.IO.StreamWriter sr = new StreamWriter(WORKDIR + "resultData.txt", false);
            //sr.Write(resultData);
            //sr.Close();

            //ProcessStartInfo procInfo = new ProcessStartInfo("notepad.exe", WORKDIR + "resultData.txt");
            ProcessStartInfo procInfo = new ProcessStartInfo("notepad.exe", resultData);
            procInfo.WindowStyle = ProcessWindowStyle.Normal;
            Process.Start(procInfo);
        }
        private string getDBType(string id)
        {
            string dbType = string.Empty;
            if (id.Contains("(Q)"))
            {
                dbType = "Q";
            }
            if (id.Contains("(U)"))
            {
                dbType = "U";
            }
            return dbType;
        }


        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            FunctionList.Save(contents, this.Text);
        }

        private void printRptTSButton_Click(object sender, EventArgs e)
        {
            FunctionList.PrintReport(contents);
        }

        private void setupButton_Click(object sender, EventArgs e)
        {
            FunctionList.PrinterPageSetup();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private List<CompareResults> GetResult()
        {
            List<CompareResults> list = new List<CompareResults>();

            if (!File.Exists(this.Text))
                return list;

            FileInfo fileInfo = new FileInfo(this.Text);
            StreamReader stream = fileInfo.OpenText();

            string buf = "";
            string capturefile = "";

            while ((buf = stream.ReadLine()) != null)
            {
                if (buf.ToUpper().Contains(".ZIP"))
                {
                    capturefile = buf.Replace(":", "").Trim();
                    continue;
                }

                if (!buf.Contains("(Exec")
                    && !buf.Contains("%"))
                    continue;

                string[] strs = buf.Split(new char[] { ' ' },
                    StringSplitOptions.RemoveEmptyEntries);

                if (strs == null || strs.Length != 3)
                    continue;

                string[] s = strs[0].Split(new string[] { "(" },
                    StringSplitOptions.RemoveEmptyEntries);
                if (s == null || s.Length != 2)
                    continue;

                CompareResults res = new CompareResults();

                res.captureFile = capturefile;

                try
                {
                    res.dataMatch
                        = int.Parse(strs[1].Trim().Replace("%", ""));
                    res.intronMatch
                        = int.Parse(strs[2].Trim().Replace("%", ""));
                }
                catch
                {
                    continue;
                }
                res.ID = "(" + s[0].Trim();
                res.exec = s[1].Trim().Replace(")", "");

                string outputFile = capturefile.Replace(".ZIP", ""); ;
                outputFile += "_";
                s = res.ID.Split(new char[] { ')' },
                    StringSplitOptions.RemoveEmptyEntries);
                if (s == null || s.Length != 2)
                    continue;
                outputFile += s[1].Trim();

                if (res.ID.Contains("(Q)"))
                    outputFile += ".QA";
                else if (res.ID.Contains("(U)"))
                    outputFile += ".UEI";
                else if (res.ID.Contains("(I)"))
                    outputFile += ".IQA";

                res.outputFilePath = WORKDIR + outputFile;

                list.Add(res);
            }

            stream.Close();
            return list;
        }
        private void toolStripNewFuncNewSigns_Click(object sender, EventArgs e)
        {
            FunctionsSignalsViewer m_FunctionsSignals = new FunctionsSignalsViewer();
            m_FunctionsSignals.ShowDialog(this);
        }
    }
}