namespace CommonForms.Forms
{
    partial class DatabaseSelectorComboBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DBSelectorComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // DBSelectorComboBox
            // 
            this.DBSelectorComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DBSelectorComboBox.FormattingEnabled = true;
            this.DBSelectorComboBox.Location = new System.Drawing.Point(0, 0);
            this.DBSelectorComboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DBSelectorComboBox.Name = "DBSelectorComboBox";
            this.DBSelectorComboBox.Size = new System.Drawing.Size(365, 24);
            this.DBSelectorComboBox.TabIndex = 0;
            this.DBSelectorComboBox.SelectedIndexChanged += new System.EventHandler(this.dbSelectorComboBox_SelectedIndexChanged);
            // 
            // DatabaseSelectorComboBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.DBSelectorComboBox);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "DatabaseSelectorComboBox";
            this.Size = new System.Drawing.Size(365, 34);
            this.Load += new System.EventHandler(this.DatabaseSelectorComboBox_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox DBSelectorComboBox;
    }
}
