using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using UEI.Workflow2010.Report.Service;

namespace CommonForms
{
    public enum SelType { RANGE, MODE, FILE };

    public struct SelectType
    {
        public SelType SelectionType;
        public string Result;
    };

    public partial class IDSelectionForm : Form
    {
        public const string SELTYPERESULT = "IdFilterRes";
        public const string SELTYPE = "IdFilterSel";
        private IList<string> _SelectedIDList;
        private SelectType _CurrentSelection = new SelectType();

        public IDSelectionForm()
        {
            InitializeComponent();
            InitializeModeList();
        }


        public IList<string> SelectedIDList
        {
            get {
                this._SelectedIDList = GetSelectedIDList(this._CurrentSelection);
                return this._SelectedIDList; 
            }
        }
        public SelType SelectedType
        {
            get { return this._CurrentSelection.SelectionType; }            
        }
        /// <summary>
        /// Gets all the available modes from db and passes it to selection control.
        /// <remarks>Added on 18/10/2010.</remarks>
        /// </summary>
        private void InitializeModeList()
        {
            Reports report = new Reports();
            IList<string> modeList = report.GetAllModes();

            if (modeList != null && modeList.Count > 0)
            {
                this.idSelectionControl.AvailableModes = modeList;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (!idSelectionControl.ValidateAndGetData())
            {
                return;
            }

           // SelectType curSelectType = idSelectionControl.GetSelectType();
            this._CurrentSelection = idSelectionControl.GetSelectType();

           // Configuration.SetConfigItem("IdFilterRes", curSelectType.Result);
           // Configuration.SetConfigItem("IdFilterSel", curSelectType.SelectionType.ToString());

            Configuration.SetConfigItem(SELTYPERESULT, this._CurrentSelection.Result);
            Configuration.SetConfigItem(SELTYPE, this._CurrentSelection.SelectionType.ToString());
            //set the ids user selected.
            
            //Commented code moved to get accessor of SelectedIDList property
            //this._SelectedIDList = GetSelectedIDList(curSelectType);

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        //Added on 11/17/2010 as part of integrating dbm.

        #region Gets&SetsIDListBasedOnUserSelectionFromDB
        /// <summary>
        /// Gets the ids based on the user selection.
        /// </summary>
        /// <remarks>Added on 18/10/2010 by binil.Code is moved from DBMain.cs.</remarks>
        /// <param name="currentSelection"></param>
        /// <returns></returns>
        private IList<string> GetSelectedIDList(SelectType currentSelection)
        {
            IList<string> idList = null;

            switch (currentSelection.SelectionType)
            {
                case SelType.RANGE:
                    idList = GetIDListFromRange(currentSelection.Result);
                    break;

                case SelType.MODE:
                    idList = GetIDListFromMode(currentSelection.Result);
                    break;

                case SelType.FILE:
                    idList = GetIDListFromFile(currentSelection.Result);
                    break;


            }

            return idList;
        }

        private IList<string> GetIDListFromRange(string range)
        {
            if (!String.IsNullOrEmpty(range))
            {
                string[] listIDs = range.Split('|');
                string[] paramlist = new string[2];
                paramlist[0] = listIDs[0];
                if (listIDs.Length == 2)
                    paramlist[1] = listIDs[1];

                Reports reports = new Reports();

                return reports.GetAllIDs(paramlist);

            }

            return null;
        }

        private IList<string> GetIDListFromMode(string modes)
        {
            if (!String.IsNullOrEmpty(modes))
            {
                string[] paramList = new string[2];
                //if all modes are present.
                if (modes.Length == 26)
                {
                    paramList[0] = ".";
                }
                else
                {
                    StringBuilder modeBuilder = new StringBuilder();
                    CharEnumerator charEnum = modes.GetEnumerator();

                    while (charEnum.MoveNext())
                    {
                        modeBuilder.Append(charEnum.Current + ",");

                    }

                    //remove the last comma','
                    if (modeBuilder.Length > 0)
                        modeBuilder.Remove(modeBuilder.Length - 1, 1);

                    paramList[0] = modeBuilder.ToString();
                }

                Reports reports = new Reports();
                return reports.GetAllIDs(paramList);
            }
            return null;
        }

        private IList<string> GetIDListFromFile(string fileName)
        {
            if (!String.IsNullOrEmpty(fileName))
            {
                string[] paramList = new string[2];
                System.IO.FileInfo file = new System.IO.FileInfo(fileName);
                System.IO.StreamReader stream = file.OpenText();
                
                string id = stream.ReadLine();
                StringBuilder idBuilder = new StringBuilder();
                while (!String.IsNullOrEmpty(id))
                {
                    id = id.Trim();

                    if (id.Length == 0 || this.idSelectionControl.IsValidIdName(id) == null)
                    {
                        id = stream.ReadLine();
                        continue;
                    } 
                    idBuilder.Append(id + ",");

                    id = stream.ReadLine();
                }
                stream.Close();                
                if (idBuilder.Length > 0)
                {
                    idBuilder.Remove(idBuilder.Length - 1, 1);

                    paramList[0] = idBuilder.ToString();                    
                    Reports reports = new Reports();
                    return reports.GetAllIDs(paramList);
                }

                //else
                //{
                //    MessageBox.Show("File is empty.");
                //}


                return null;

            }
            return null;
        }

        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        public List<string> GetSelectedIDList(IList<string> allIDList)
        {
            return idSelectionControl.GetSelectedIDList(allIDList);            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetQueryConditions()
        {
            return idSelectionControl.GetQueryConditions();
        }

        //Overridden GetSelectedIDList(...) to get the previous selected ID list
        public List<string> GetSelectedIDList(IList<string> allIDList, SelectType selChoice)
        {
            return idSelectionControl.GetSelectedIDList(allIDList,selChoice);
        }
    }
}