namespace CommonForms
{
    partial class AccessRestrictForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelPassWd = new System.Windows.Forms.Label();
            this.textBoxPassWd = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelPassWd
            // 
            this.labelPassWd.AutoSize = true;
            this.labelPassWd.Location = new System.Drawing.Point(17, 27);
            this.labelPassWd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPassWd.Name = "labelPassWd";
            this.labelPassWd.Size = new System.Drawing.Size(151, 17);
            this.labelPassWd.TabIndex = 0;
            this.labelPassWd.Text = "Enter Password below:";
            // 
            // textBoxPassWd
            // 
            this.textBoxPassWd.Location = new System.Drawing.Point(21, 58);
            this.textBoxPassWd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxPassWd.Name = "textBoxPassWd";
            this.textBoxPassWd.PasswordChar = '*';
            this.textBoxPassWd.Size = new System.Drawing.Size(248, 22);
            this.textBoxPassWd.TabIndex = 1;
            this.textBoxPassWd.UseSystemPasswordChar = true;
            this.textBoxPassWd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPassWd_KeyPress);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(21, 116);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(100, 28);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(171, 116);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(100, 28);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // AccessRestrictForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(341, 164);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.textBoxPassWd);
            this.Controls.Add(this.labelPassWd);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "AccessRestrictForm";
            this.Text = "PASSWORD REQUIRED";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelPassWd;
        private System.Windows.Forms.TextBox textBoxPassWd;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
    }
}