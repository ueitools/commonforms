namespace CommonForms
{
    partial class IrSearchResultViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xmlResultsTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // xmlResultsTextBox
            // 
            this.xmlResultsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xmlResultsTextBox.Font = new System.Drawing.Font("Courier New", 10.86792F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xmlResultsTextBox.Location = new System.Drawing.Point(16, 15);
            this.xmlResultsTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.xmlResultsTextBox.Name = "xmlResultsTextBox";
            this.xmlResultsTextBox.Size = new System.Drawing.Size(1319, 539);
            this.xmlResultsTextBox.TabIndex = 0;
            this.xmlResultsTextBox.Text = "";
            // 
            // IrSearchResultViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1352, 570);
            this.Controls.Add(this.xmlResultsTextBox);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "IrSearchResultViewer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IRDBSearch Results";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox xmlResultsTextBox;
    }
}