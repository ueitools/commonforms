using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
namespace CommonForms
{
    public partial class IrSearchResultViewer : Form
    {
        private string _fileName = string.Empty;
        private ProcessingForm processingForm;
        //private BackgroundWorker bw;
        private IntronLabelMapper iLM;
        protected string _dbType = string.Empty;
        //
        private string _resultData = string.Empty;
        bool readDone;

        public string ResultData
        {
            get { return _resultData; }
        }
	

        public IrSearchResultViewer(string filename, string dbType)
        {
            InitializeComponent();

            if (Path.GetExtension(filename) == ".xml")
            {

                //
                _dbType = dbType;
                _fileName = filename;
                processingForm = new ProcessingForm();
                processingForm.Show();
                this.Hide();
                SearchResult sr = SearchResult.ReadFromFile(_fileName);
                LoadResults(sr);
                sr.ReadStatus = "True";
                if (!readDone)
                {
                    sr.WriteToFile(_fileName);
                    storeToText();
                }
                processingForm.Close();
                this.Show();
                //bw = new BackgroundWorker();
                //bw.DoWork += new DoWorkEventHandler(bw_DoWork);
                //bw.ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged);
                //bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
                //bw.WorkerReportsProgress = true;
                //bw.RunWorkerAsync();



            }
            else if (Path.GetExtension(filename) == ".QA" ||
               Path.GetExtension(filename) == ".UEI")
            {
                StreamReader strRdr = new StreamReader(filename);
                xmlResultsTextBox.Text = strRdr.ReadToEnd();
                strRdr.Close();
            }
        }

        //void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        //{
        //    processingForm.SetMessage("Completed");
        //    processingForm.SetProgress(100);
        //    processingForm.Update();
        //    processingForm.Close();
        //    this.Show();
        //}

        //void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        //{
        //    processingForm.SetProgress(e.ProgressPercentage);
        //    processingForm.Update();
        //}

        //void bw_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    SearchResult sr = SearchResult.ReadFromFile(_fileName);
        //    LoadResults(sr);
        //    sr.ReadStatus = "True";
        //    sr.WriteToFile(_fileName);

        //}

        private void LoadResults(SearchResult sr)
        {
            string strUnmatched = "\r\nUnmatched CFI files:\r\n";
            int unMatCount = 0;
            xmlResultsTextBox.Text += "Tracking Number = " + sr.TrackingNumber + "\r\n";
            xmlResultsTextBox.Text += "Executor Number = " + sr.Exec + "\r\n";
            xmlResultsTextBox.Text += "Comparing Capture File: " + sr.CaptureFile + " to ID " + sr.ID + "\r\n";
            xmlResultsTextBox.Text += "DataBase type: " + sr.Database + "\r\n";

            
            bool.TryParse(sr.ReadStatus, out readDone);
            if (readDone)
            {
                _resultData = _fileName.Replace("xml", "txt");
                return;
            }

            if (processingForm != null)
            {
                processingForm.SetProgress(1);
                processingForm.SetMessage("Generating Results.....");
            }
            int resultCnt = 2;
            iLM = new IntronLabelMapper(_dbType);
            foreach (ResultItem resultitemlist in sr.ResultItems)
            {
                //matches
                List<ResultItem> resmatchesList = resultitemlist.Matches;
                if (resmatchesList.Count > 0)
                {
                    xmlResultsTextBox.Text += "\r\n------------------------------------------------------------------------------------------\r\n";

                    foreach (ResultItem resmatches in resmatchesList)
                    {
                        xmlResultsTextBox.Text += String.Format("\r\n{0,-15}{1,-2}{2}{3}{4,-30}{5}\r\n", resultitemlist.Name, ":",
                                    resultitemlist.Data, "              ", resultitemlist.Label, resultitemlist.Intron);


                        xmlResultsTextBox.Text += String.Format("{0,-15}{1,-2}{2}{3}{4,-30}{5}\r\n", resmatches.Name, ":",
                                 resmatches.Data, "              ", resmatches.Label, resmatches.Intron);

                        if (!string.IsNullOrEmpty(resmatches.IntronPriority) && resmatches.IntronPriority != "(p0)")
                        {
                            // if (!resmatches.ExactMatch && resmatches.Intron.Contains(resmatches.IntronPriority))
                            if (resmatches.Intron.Contains(resmatches.IntronPriority))
                            {
                                string priorityDetails = iLM.PriorityDetailFromDB(sr.ID, resmatches.Intron.Replace(resmatches.IntronPriority, string.Empty));
                                xmlResultsTextBox.Text += "\r\n------------------------------------------------------------------------------------------\r\n";
                                xmlResultsTextBox.Text += priorityDetails;
                                xmlResultsTextBox.Text += "\r\n------------------------------------------------------------------------------------------\r\n";
                            }
                        }
                    }


                    List<ConflictResultItem> conflictResult = resultitemlist.ConflictMatches;
                    if (conflictResult.Count > 0)
                    {
                        xmlResultsTextBox.Text += "\r\n------------------------------------------------------------------------------------------\r\n";

                        if (readDone)
                        {
                            foreach (ConflictResultItem resmatches in conflictResult)
                            {
                                xmlResultsTextBox.Text += resmatches.ConflictDetails;
                            }
                        }
                        else
                        {
                            foreach (ConflictResultItem resmatches in conflictResult)
                            {
                                List<string> results = iLM.IntronLabelRelation(resmatches.CapIntron, resmatches.CapLabel, resmatches.DBLabel, resmatches.DBIntron, resmatches.AssignedIntron, resmatches.ID, resmatches.IsFromError, resmatches.IsFormat);
                                xmlResultsTextBox.Text += results[0];
                                resmatches.ConflictDetails = results[0];
                            }
                        }
                    }
                }

                else
                {
                    if (resmatchesList.Count <= 0)
                    {
                        strUnmatched += String.Format("{0,-15}{1,-2}{2}{3}{4,-30}{5}\r\n", resultitemlist.Name, ":",
                                        resultitemlist.Data, "              ", resultitemlist.Label, resultitemlist.Intron);
                        unMatCount++;

                        List<ConflictResultItem> conflictResult = resultitemlist.ConflictMatches;
                        if (conflictResult.Count > 0)
                        {
                            xmlResultsTextBox.Text += "\r\n------------------------------------------------------------------------------------------\r\n";

                            if (readDone)
                            {
                                foreach (ConflictResultItem resmatches in conflictResult)
                                {
                                    //xmlResultsTextBox.Text += resmatches.ConflictDetails;
                                    strUnmatched += resmatches.ConflictDetails;
                                }
                            }
                            else
                            {
                                foreach (ConflictResultItem resmatches in conflictResult)
                                {
                                    List<string> results = iLM.IntronLabelRelation(resmatches.CapIntron, resmatches.CapLabel, resmatches.DBLabel, resmatches.DBIntron, resmatches.AssignedIntron, resmatches.ID, resmatches.IsFromError, resmatches.IsFormat);
                                    strUnmatched += results[0];
                                    resmatches.ConflictDetails = results[0];
                                }
                            }
                        }

                    }

                }
                resultCnt++;
                processingForm.SetProgress(getPerValue(resultCnt, sr.ResultItems.Count));
                processingForm.Update();
            }

            xmlResultsTextBox.Text += "\r\n------------------------------------------------------------------------------------------\r\n";

            if (unMatCount > 0)
                xmlResultsTextBox.Text += strUnmatched + "\r\n\r\n";

            xmlResultsTextBox.Text += "Total " + unMatCount.ToString() + " unmatched CFI files.";
            //_resultData = xmlResultsTextBox.Text;

        }
        private int getPerValue(int complete, int total)
        {
            int perValue = (complete / total) * 100;
            if (perValue >= 100)
                return 100;
            else
                return perValue;

        }

        private void storeToText()
        {
            StreamWriter strWr = new StreamWriter(_fileName.Replace("xml", "txt"));
            strWr.Write(xmlResultsTextBox.Text);
            strWr.Close();
            _resultData = _fileName.Replace("xml", "txt");
        }

    }

    [Serializable]
    public class SearchResult
    {
        private string _trackingNumber;
        private int _exec;
        private string _captureFile;
        private string _id;
        private string _database;
        private string _readStatus;
        private ResultItem.List _resultItems;
        private List<string> _skippedFileMessages;

        public SearchResult()
        {
            _resultItems = new ResultItem.List();
            _conflictResults = new ResultItem.ConflictList();
        }

        public string TrackingNumber
        {
            get { return _trackingNumber; }
            set { _trackingNumber = value; }
        }

        public int Exec
        {
            get { return _exec; }
            set { _exec = value; }
        }

        public string CaptureFile
        {
            get { return _captureFile; }
            set { _captureFile = value; }
        }

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Database
        {
            get { return _database; }
            set { _database = value; }
        }

        public string ReadStatus
        {
            get { return _readStatus; }
            set { _readStatus = value; }
        }

        public ResultItem.List ResultItems
        {
            get { return _resultItems; }
            set { _resultItems = value; }
        }


        //Added by Venki for Intron Enhancement - Start
        private ResultItem.ConflictList _conflictResults;
        public ResultItem.ConflictList ConflictResults
        {
            get { return _conflictResults; }
            set { _conflictResults = value; }
        }
        //Added by Venki for Intron Enhancement - End
        [XmlArrayItem("Message")]
        public List<string> SkippedFileMessages
        {
            get { return _skippedFileMessages; }
            set { _skippedFileMessages = value; }
        }

        public ResultItem AddResult(string name, string data, string label, string intron, string priority, bool exactMatch)
        {
            ResultItem result = new ResultItem(name, data, label, intron, priority, exactMatch);
            ResultItems.Add(result);
            return result;
        }
        ////Added by Venki for Intron Enhancement - Start
        public void AddConflictMatch(ConflictResultItem conflictItem)
        {
            //ConflictResultItem match = new ConflictResultItem();
            ConflictResults.Add(conflictItem);
        }
        //Added by Venki for Intron Enhancement - End
        public void WriteToFile(string filename)
        {
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(GetType());
            TextWriter writer = new StreamWriter(filename);
            x.Serialize(writer, this);
            writer.Close();
        }

        public static SearchResult ReadFromFile(string filename)
        {
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(SearchResult));
            TextReader reader = new StreamReader(filename);

            SearchResult retValue = (SearchResult)x.Deserialize(reader);
            reader.Close();
            return retValue;
        }
    }

    [Serializable]
    public class ResultItem
    {
        private string _name;
        private string _data;
        private string _label;
        private string _intron;
        private string _priority;
        private bool _exactMatch;
        private List _matches;
        private ConflictList _conflictMatchs;

        public ResultItem()
        {
            _matches = new List();
            _conflictMatchs = new ConflictList();
        }

        public ResultItem(string name, string data, string label, string intron, string priority, bool exactMatch)
        {
            _name = name;
            _data = data;
            _label = label;
            _intron = intron;
            _priority = priority;
            _exactMatch = exactMatch;
            _matches = new List();
            _conflictMatchs = new ConflictList();
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }

        public string Intron
        {
            get { return _intron; }
            set { _intron = value; }
        }

        public string IntronPriority
        {
            get { return _priority; }
            set { _priority = value; }
        }

        public List Matches
        {
            get { return _matches; }
            set { _matches = value; }
        }

        public bool ExactMatch
        {
            get { return _exactMatch; }
            set { _exactMatch = value; }
        }

        public ConflictList ConflictMatches
        {
            get { return _conflictMatchs; }
            set { _conflictMatchs = value; }
        }

        public ResultItem AddMatch(string name, string data, string label, string intron, string priority, bool exactMatch)
        {
            ResultItem match = new ResultItem(name, data, label, intron, priority, exactMatch);
            Matches.Add(match);
            return match;
        }

        public void AddConflictMatch(ConflictResultItem conflictItem)
        {
            ConflictMatches.Add(conflictItem);
        }

        public string WriteFormatted()
        {
            return String.Format("{0,-15}{1,-2}{2}{3}{4,-30}{5}", _name, ":", _data, "              ", _label, _intron);
        }

        [Serializable]
        public class List : List<ResultItem> { }

        [Serializable]
        public class ConflictList : List<ConflictResultItem> { }
    }


    //Added by Venki for Intron Enhancement - Start
    [Serializable]
    public class ConflictResultItem
    {
        private string _id;
        private string _label;
        private string _intron;
        private string _dbLabel;
        private string _dbIntron;
        private bool _isFromError;
        private bool _isFormat;
        private string _assignedIntron;

        private string _conflictDetails;

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }
        public string CapLabel
        {
            get { return _label; }
            set { _label = value; }
        }

        public string CapIntron
        {
            get { return _intron; }
            set { _intron = value; }
        }

        public string DBLabel
        {
            get { return _dbLabel; }
            set { _dbLabel = value; }
        }

        public string DBIntron
        {
            get { return _dbIntron; }
            set { _dbIntron = value; }
        }

        public bool IsFromError
        {
            get { return _isFromError; }
            set { _isFromError = value; }
        }

        public bool IsFormat
        {
            get { return _isFormat; }
            set { _isFormat = value; }
        }

        public string AssignedIntron
        {
            get { return _assignedIntron; }
            set { _assignedIntron = value; }
        }

        public string ConflictDetails
        {
            get { return _conflictDetails; }
            set { _conflictDetails = value; }
        }

        //public ConflictList ConflictMatches
        //{
        //    get { return _conflictMatches; }
        //    set { _conflictMatches = value; }
        //}
    }
    //Added by Venki for Intron Enhancement - End


    #region Old
    //[Serializable]
    //public class SearchResult
    //{
    //    private string _trackingNumber;
    //    private int _exec;
    //    private string _captureFile;
    //    private string _id;
    //    private string _database;
    //    private ResultItem.List _resultItems;
    //    private List<string> _skippedFileMessages;

    //    public SearchResult()
    //    {
    //        _resultItems = new ResultItem.List();
    //    }

    //    public string TrackingNumber
    //    {
    //        get { return _trackingNumber; }
    //        set { _trackingNumber = value; }
    //    }

    //    public int Exec
    //    {
    //        get { return _exec; }
    //        set { _exec = value; }
    //    }

    //    public string CaptureFile
    //    {
    //        get { return _captureFile; }
    //        set { _captureFile = value; }
    //    }

    //    public string ID
    //    {
    //        get { return _id; }
    //        set { _id = value; }
    //    }

    //    public string Database
    //    {
    //        get { return _database; }
    //        set { _database = value; }
    //    }

    //    public ResultItem.List ResultItems
    //    {
    //        get { return _resultItems; }
    //        set { _resultItems = value; }
    //    }

    //    [XmlArrayItem("Message")]
    //    public List<string> SkippedFileMessages
    //    {
    //        get { return _skippedFileMessages; }
    //        set { _skippedFileMessages = value; }
    //    }

    //    public ResultItem AddResult(string name, string data, string label, string intron)
    //    {
    //        ResultItem result = new ResultItem(name, data, label, intron);
    //        ResultItems.Add(result);
    //        return result;
    //    }

    //    public void WriteToFile(string filename)
    //    {
    //        System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(GetType());
    //        TextWriter writer = new StreamWriter(filename);
    //        x.Serialize(writer, this);


    //    }

    //    public static SearchResult ReadFromFile(string filename)
    //    {
    //        System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(SearchResult));
    //        TextReader reader = new StreamReader(filename);
    //        return (SearchResult)x.Deserialize(reader);
    //    }
    //}

    //[Serializable]
    //public class ResultItem
    //{
    //    private string _name;
    //    private string _data;
    //    private string _label;
    //    private string _intron;
    //    private List _matches;

    //    public ResultItem()
    //    {
    //        _matches = new List();
    //    }

    //    public ResultItem(string name, string data, string label, string intron)
    //    {
    //        _name = name;
    //        _data = data;
    //        _label = label;
    //        _intron = intron;
    //        _matches = new List();
    //    }

    //    public string Name
    //    {
    //        get { return _name; }
    //        set { _name = value; }
    //    }

    //    public string Data
    //    {
    //        get { return _data; }
    //        set { _data = value; }
    //    }

    //    public string Label
    //    {
    //        get { return _label; }
    //        set { _label = value; }
    //    }

    //    public string Intron
    //    {
    //        get { return _intron; }
    //        set { _intron = value; }
    //    }

    //    public List Matches
    //    {
    //        get { return _matches; }
    //        set { _matches = value; }
    //    }

    //    public ResultItem AddMatch(string name, string data, string label, string intron)
    //    {
    //        ResultItem match = new ResultItem(name, data, label, intron);
    //        Matches.Add(match);
    //        return match;
    //    }

    //    public string WriteFormatted()
    //    {
    //        return String.Format("{0,-15}{1,-2}{2}{3}{4,-30}{5}", _name, ":", _data, "              ", _label, _intron);
    //    }

    //    [Serializable]
    //    public class List : List<ResultItem> { }
    //}
    #endregion
}