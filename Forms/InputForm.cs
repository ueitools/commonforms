using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CommonForms
{
    public partial class InputForm : Form
    {
        string _value = null;

        public string Value
        {
            get { return _value; }
            set {_value = value; }
        }

        public InputForm()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            _value = textBoxValue.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        public void SetLabel(string label)
        {
            this.labelValue.Text = label;
        }

        public void SetTitle(string title)
        {
            this.Text = title;
        }

        private void textBoxValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
                buttonOK_Click(sender, e);
        }
    }
}