using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace CommonForms.Forms
{
    public partial class IDSelectionControl : UserControl
    {
        SelectType _choice = new SelectType();
        bool validated; // try to ensure manual getdata call for now 9/3/2008
        private IList<string> _ListOFModes;
        public IDSelectionControl()
        {
            InitializeComponent();
            ModeRdoBtn.Checked = true;
            AllModes.Checked = true;
            validated = false;
        }

        #region 11/17/2010IntegrateDBM

        /// <summary>
        /// Enables only those modes available in the list.
        /// </summary>
        public IList<string> AvailableModes
        {
            set
            {
                this._ListOFModes = value;
                UpdateModeCheckboxes();
            }
        }

        private void UpdateModeCheckboxes()
        {
            if (this._ListOFModes != null && this._ListOFModes.Count > 0)
            {
                foreach (CheckBox modeChk in groupBox2.Controls)
                {
                    if (modeChk.Text.Length == 1)
                    {
                        if (this._ListOFModes.Contains(modeChk.Text))
                            modeChk.Enabled = true;
                        else
                            modeChk.Enabled = false;
                    }
                }
            }
        }
        
        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        public List<string> GetSelectedIDList(IList<string> allIDList)
        {
            if (!validated)
                throw new Exception("successful validation required");

            List<string> idList = null;
            switch (_choice.SelectionType)
            {
                case SelType.RANGE:
                    idList = GetIDListFromRange(allIDList, _choice.Result);
                    break;

                case SelType.MODE:
                    idList = GetIDListFromMode(allIDList, _choice.Result);
                    break;

                case SelType.FILE:
                    idList = GetIDListFromFile(allIDList, _choice.Result);
                    break;
            }

            return idList;
        }

        /// <summary>
        /// 9/3/2008
        /// not sure how to get this to run before control closes.
        /// for now have to manually get data with validation.
        /// may want to make it part of validation routines later.
        /// </summary>
        /// <returns></returns>
        public bool ValidateAndGetData()
        {
            bool result = false;

            switch (_choice.SelectionType)
            {
                case SelType.RANGE:
                    result = CheckIDRange();
                    if (!result)
                    {
                        MessageBox.Show("Invalid Range.", "ERROR");
                    }
                    break;
                case SelType.MODE:
                    result = CheckModeSelection();
                    if (!result)
                    {
                        MessageBox.Show("You haven't completed your selection.", "ERROR");
                    }
                    break;
                case SelType.FILE:
                    if (InputFile.Text.Length > 0)
                    {
                        _choice.Result = InputFile.Text;
                        result = true;
                    }
                    if (!result)
                    {
                        MessageBox.Show("Please select a Valid text File.", "ERROR");
                    }
                    break;
            }

            //if (!result)
            //{
            //    MessageBox.Show("You haven't completed your selection.", "ERROR");
            //}

            validated = result;

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetQueryConditions()
        {
            if (!validated)
                throw new Exception("successful validation required");

            switch (_choice.SelectionType)
            {
                case SelType.RANGE:
                    return GetQueryConditionsFromRange();

                case SelType.MODE:
                    return GetQueryConditionsFromMode();

                case SelType.FILE:
                    return GetQueryConditionsFromFile();
            }

            return "";
        }

        private void RangeRdoBtn_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Enabled = true;
            groupBox2.Enabled = false;
            groupBox3.Enabled = false;
            _choice.SelectionType = SelType.RANGE;
        }

        private void ModeRdoBtn_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Enabled = false;
            groupBox2.Enabled = true;
            groupBox3.Enabled = false;
            _choice.SelectionType = SelType.MODE;
        }

        private void FileRdoBtn_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Enabled = false;
            groupBox2.Enabled = false;
            groupBox3.Enabled = true;
            _choice.SelectionType = SelType.FILE;

            if (FileRdoBtn.Checked)
                GetIDInputFile();
        }

        /// <summary>
        /// verify that at least one ID was entered
        /// </summary>
        /// <returns></returns>
        private bool CheckIDRange()
        {
            bool result = false;
            string IDName;

            IDName = IsValidIdName(FirstID.Text);
            if (IDName != null)
            {
                _choice.Result = IDName;
                result = true;
            }

            IDName = IsValidIdName(LastID.Text);
            if (IDName != null && _choice.Result != null)
            {
                if (_choice.Result.Substring(0, 1) != IDName.Substring(0, 1))
                    return false;

                if (_choice.Result.Length > 0)
                    _choice.Result += "|";
                _choice.Result += IDName;
                result = true;
            }
            if (LastID.Text != String.Empty && IDName == null)
                result = false;

            return result;
        }

        public string IsValidIdName(string ID)
        {
            ID = ID.ToUpper();
            string result = null;
            int IDNumber;            
            //if (ID.Length > 1 && ID.Length < 6)
            if (ID.Length == 5)
            {
                if (Char.IsLetter(ID[0]) && (_ListOFModes.Contains(ID[0].ToString())) &&
                     int.TryParse(ID.Substring(1), out IDNumber))
                {
                    result = string.Format("{0}{1:D4}", Char.ToUpper(ID[0]), IDNumber);
                }
            }
            return result;
        }

        /// <summary>
        /// Check for valid mode selections
        /// </summary>
        /// <returns></returns>
        private bool CheckModeSelection()
        {
            bool result = false;
            if (AllModes.Checked == true)
            {
                _choice.Result = "";
                foreach (CheckBox Box in groupBox2.Controls)
                    if (Box.Enabled && Box.Text.Length == 1)
                        _choice.Result += Box.Text;

                result = true;
            }
            else
            {
                string modes = GetSelectedModes();
                if (modes.Length > 0)
                {
                    _choice.Result = modes;
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// display an open file dialog to select an ID List file
        /// </summary>
        private void GetIDInputFile()
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.RestoreDirectory = true;
                dlg.Filter = "Text file (*.txt)|*.txt";
                dlg.Filter += "|List File (*.lst)|*.LST";
                dlg.Filter += "|ID List FIle (*.ids)|*.ids";
                dlg.Filter += "|All File Types (*.*)|*.*";
                dlg.DefaultExt = "txt";
                DialogResult res = dlg.ShowDialog();
                if (res == DialogResult.OK)
                {
                    InputFile.Text = dlg.FileName;
                }

              
            }
            //OpenFileDialog dlg = new OpenFileDialog();
            //dlg.Filter = "Text file (*.txt)|*.txt";
            //dlg.Filter += "|List File (*.lst)|*.LST";
            //dlg.Filter += "|ID List FIle (*.ids)|*.ids";
            //dlg.Filter += "|All File Types (*.*)|*.*";
            //dlg.DefaultExt = "txt";
            //DialogResult res = dlg.ShowDialog();
            //if (res == DialogResult.OK)
            //{
            //    InputFile.Text = dlg.FileName;
            //}
        }

        /// <summary>
        /// if AllModes is checked, clear all single mode boxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AllMode_CheckedChanged(object sender, EventArgs e)
        {
            if (AllModes.Checked == true)
            {
                ResetAllModes();
                AllModes.Checked = true;
            }
        }

        /// <summary>
        /// Called if any mode checkbox state changes except AllModes.
        /// Check if all mode boxes are checked, if true reset all checks and set AllModes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModeSelection_CheckedChanged(object sender, EventArgs e)
        {
            AllModes.Checked = false;
            if (AllModesSelected())
            {
                ResetAllModes();
                AllModes.Checked = true;
            }                              
        }

        /// <summary>
        /// Return true if all mode check boxes are checked return false if any are not checked
        ///  except AllModes
        /// </summary>
        /// <returns></returns>
        private bool AllModesSelected()
        {            
            foreach (CheckBox box in groupBox2.Controls)
                if (box.Checked == false && box.Text.Length == 1 && this._ListOFModes.Contains(box.Text) == true)
                    return false;

            return true;
        }

        /// <summary>
        /// Reset all mode check boxes
        ///  except AllModes
        /// </summary>
        private void ResetAllModes()
        {
            foreach (CheckBox box in groupBox2.Controls)
                if (box.Text.Length == 1)
                    box.Checked = false;
        }

        /// <summary>
        /// Add the mode letter to the result string for each selected mode
        ///  except AllModes
        /// </summary>
        /// <returns></returns>
        private string GetSelectedModes()
        {
            string modes = "";

            foreach (CheckBox Box in groupBox2.Controls)
                if (Box.Checked && Box.Text.Length == 1)
                    modes += Box.Text;

            return modes;
        }

        /// <summary>
        /// assume 1 id in 1 line
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private List<string> GetIDListFromFile(
            IList<string> allIDList, string fileName)
        {
            List<string> idList = new List<string>();
            FileInfo file = new FileInfo(fileName);
            StreamReader stream = file.OpenText();

            string id = stream.ReadLine();
            while (id != null)
            {
                id = id.Trim();
                if (allIDList.Contains(id))
                    idList.Add(id);

                id = stream.ReadLine();
            }

            stream.Close();
            return idList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modeList"></param>
        /// <returns></returns>
        private List<string> GetIDListFromMode(
            IList<string> allIDList, string modeList)
        {
            List<string> idList = new List<string>();
            if (AllModes.Checked)
            {
                foreach (string id in allIDList)
                    idList.Add(id);
            }
            else
            {
                foreach (string id in allIDList)
                {
                    if (modeList.Length > 0 &&
                        modeList.Contains(id.Substring(0, 1)) == true)
                    {
                        idList.Add(id);
                    }
                }
            }
            return idList;
        }

        /// <summary>
        /// TODO: cross range is not covered
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        private List<string> GetIDListFromRange(
            IList<string> allIDList, string range)
        {
            List<string> idList = new List<string>();

            /// first ID only
            if (range.Length == 5)
            {
                if (allIDList.Contains(range))
                    idList.Add(range);

                return idList;
            }

            ///
            /// first and last
            ///
            string[] listIDs = range.Split('|');
            char mode = range[0];
            int start = int.Parse(listIDs[0].Substring(1));
            int end = int.Parse(listIDs[1].Substring(1));

            foreach (string id in allIDList)
            {
                if (id[0] == mode)
                {
                    if (int.Parse(id.Substring(1)) >= start &&
                        int.Parse(id.Substring(1)) <= end)
                    {
                        idList.Add(id);
                    }
                }
            }

            return idList;
        }

        /// <summary>
        /// assume 1 id in 1 line
        /// </summary>
        /// <returns></returns>
        private string GetQueryConditionsFromFile()
        {
            string conditions = "";
            FileInfo file = new FileInfo(_choice.Result);
            StreamReader stream = file.OpenText();
            string id = stream.ReadLine();

            while (id != null && id != "")
            {
                id = id.Trim();

                if (id.Length != 5)
                    continue;
                if (conditions != "")
                    conditions += ",";

                conditions += "'" + id + "'";
                id = stream.ReadLine();
            }

            stream.Close();

            if (conditions != "")
                conditions = "ID in (" + conditions + ")";

            return conditions;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetQueryConditionsFromMode()
        {
            string conditions = "";

            if (_choice.Result.Length > 0)
            {
                for (int i = 0; i < _choice.Result.Length; i++)
                {
                    if (conditions != "")
                        conditions += " OR ";

                    conditions += "ID LIKE '" + _choice.Result[i]
                        + "%'";
                }
            }

            return conditions;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetQueryConditionsFromRange()
        {
            /// first ID only
            if (_choice.Result.Length == 5)
            {
                return "ID = '" + _choice.Result + "'";
            }

            ///
            /// first and last
            ///
            string[] listIDs = _choice.Result.Split('|');
            char mode = _choice.Result[0];
            int start = int.Parse(listIDs[0].Substring(1));
            int end = int.Parse(listIDs[1].Substring(1));

            return " ID >= '" + mode + String.Format("{0:0000}", start)
                + "' AND ID <= '" + mode
                + String.Format("{0:0000}", end) + "'";
        }

        public SelectType GetSelectType()
        {
            return _choice;
        }

        //Overridden GetSelectedIDList(...) to get the previous selected ID list
        public List<string> GetSelectedIDList(IList<string> allIDList, SelectType selChoice)
        {         
            List<string> idList = null;
            switch (selChoice.SelectionType)
            {
                case SelType.RANGE:
                    idList = GetIDListFromRange(allIDList, selChoice.Result);
                    break;

                case SelType.MODE:
                    AllModes.Checked = false;                    
                    idList = GetIDListFromMode(allIDList, selChoice.Result);
                    break;

                case SelType.FILE:
                    idList = GetIDListFromFile(allIDList, selChoice.Result);
                    break;
            }
            return idList;
        }
    }
}
