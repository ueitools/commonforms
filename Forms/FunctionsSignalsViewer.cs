using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CommonForms.Forms
{
    public partial class FunctionsSignalsViewer : Form
    {
        #region Variables
        FunctionsSignals m_FunctionsSignals = FunctionsSignals.GetInstance();
        #endregion

        public FunctionsSignalsViewer()
        {
            InitializeComponent();
        }       

        #region Page Loading
        private void FunctionsSignalsViewer_Load(object sender, EventArgs e)
        {
            GetReports();
        }
        #endregion

        #region Get Reports
        private void GetReports()
        {
            if (m_FunctionsSignals.IsClicked == false)
            {
                m_FunctionsSignals.GetNewSignalInfo();
                m_FunctionsSignals.GetNewFunctionInfo();
                dgNewFuncsNewSigns.DataSource   = m_FunctionsSignals.GetReport();
                toolStripNewSignals.Text        = m_FunctionsSignals.NewSignals.ToString();
                toolStripNewFunctions.Text      = m_FunctionsSignals.NewFunctions.ToString();
            }
            else
            {
                dgNewFuncsNewSigns.DataSource   = m_FunctionsSignals.GetReport();
                toolStripNewSignals.Text        = m_FunctionsSignals.NewSignals.ToString();
                toolStripNewFunctions.Text      = m_FunctionsSignals.NewFunctions.ToString();
            }
        }
        #endregion
    }
}