using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BusinessObject;

namespace CommonForms
{
    public partial class DataBaseSelectionForm : Form
    {
        #region Properties
        private String _DBName = String.Empty;
        public String DBName
        {
            get { return _DBName; }
            set { _DBName = value; }
        }
        private String _connectionString = String.Empty;
        public String ConnectionString
        {
            get { return _connectionString; }
        }
        #endregion

        public DataBaseSelectionForm()
        {
            InitializeComponent();
            cmbDBSelector.Items.Add(new DBComboBoxItem("UEI2", DBConnectionString.UEI2));
            //string domain = SystemInformation.UserDomainName;
            cmbDBSelector.SelectedIndex = 0;
        }
        
        private void btnOk_Click(object sender, EventArgs e)
        {
            Configuration.SetConfigItem("DataBase", cmbDBSelector.SelectedItem.ToString());
            _connectionString = ((DBComboBoxItem)cmbDBSelector.SelectedItem).ConnectionString;
            _DBName = cmbDBSelector.SelectedItem.ToString();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        #region Cancel
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        #endregion

        #region DBComboBoxItem
        private class DBComboBoxItem
        {
            private String _caption;
            private String _connectionString;

            public DBComboBoxItem(String caption, String connectionString)
            {
                _caption = caption;
                _connectionString = connectionString;
            }

            public String ConnectionString
            {
                get { return _connectionString; }
            }

            public override String ToString()
            {
                return _caption;
            }
        }
        #endregion
    }    
}