namespace CommonForms
{
    partial class ResultsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResultsForm));
            this.reportDataGrid = new System.Windows.Forms.DataGridView();
            this.TN_File = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDEXEC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Freames = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Intron = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.View = new System.Windows.Forms.DataGridViewButtonColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.printRptTSButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.setupButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripNewFuncNewSigns = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.Exit = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            ((System.ComponentModel.ISupportInitialize)(this.reportDataGrid)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // reportDataGrid
            // 
            this.reportDataGrid.AllowUserToAddRows = false;
            this.reportDataGrid.AllowUserToDeleteRows = false;
            this.reportDataGrid.AllowUserToResizeColumns = false;
            this.reportDataGrid.AllowUserToResizeRows = false;
            this.reportDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reportDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.reportDataGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this.reportDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.reportDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.reportDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TN_File,
            this.IDEXEC,
            this.Freames,
            this.Intron,
            this.FileName,
            this.View});
            this.reportDataGrid.Location = new System.Drawing.Point(-3, 34);
            this.reportDataGrid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.reportDataGrid.MultiSelect = false;
            this.reportDataGrid.Name = "reportDataGrid";
            this.reportDataGrid.ReadOnly = true;
            this.reportDataGrid.RowHeadersVisible = false;
            this.reportDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.reportDataGrid.Size = new System.Drawing.Size(791, 315);
            this.reportDataGrid.TabIndex = 0;
            this.reportDataGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.report_CellClick);
            this.reportDataGrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.report_CellDoubleClick);
            // 
            // TN_File
            // 
            this.TN_File.HeaderText = "TN File";
            this.TN_File.Name = "TN_File";
            this.TN_File.ReadOnly = true;
            this.TN_File.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // IDEXEC
            // 
            this.IDEXEC.HeaderText = "ID(Exec)";
            this.IDEXEC.MinimumWidth = 120;
            this.IDEXEC.Name = "IDEXEC";
            this.IDEXEC.ReadOnly = true;
            this.IDEXEC.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.IDEXEC.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Freames
            // 
            this.Freames.HeaderText = "Data Match %";
            this.Freames.MinimumWidth = 120;
            this.Freames.Name = "Freames";
            this.Freames.ReadOnly = true;
            this.Freames.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Intron
            // 
            this.Intron.HeaderText = "Intron Match %";
            this.Intron.MinimumWidth = 120;
            this.Intron.Name = "Intron";
            this.Intron.ReadOnly = true;
            this.Intron.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // FileName
            // 
            this.FileName.HeaderText = "FileName";
            this.FileName.Name = "FileName";
            this.FileName.ReadOnly = true;
            this.FileName.Visible = false;
            // 
            // View
            // 
            this.View.HeaderText = "View";
            this.View.Name = "View";
            this.View.ReadOnly = true;
            this.View.Text = "View";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripButton,
            this.toolStripSeparator1,
            this.printRptTSButton,
            this.toolStripSeparator5,
            this.setupButton,
            this.toolStripSeparator3,
            this.toolStripNewFuncNewSigns,
            this.toolStripSeparator2,
            this.Exit});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(788, 27);
            this.toolStrip1.TabIndex = 68;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(60, 24);
            this.saveToolStripButton.Text = "&Save";
            this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Margin = new System.Windows.Forms.Padding(11, 0, 11, 0);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // printRptTSButton
            // 
            this.printRptTSButton.Image = ((System.Drawing.Image)(resources.GetObject("printRptTSButton.Image")));
            this.printRptTSButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printRptTSButton.Name = "printRptTSButton";
            this.printRptTSButton.Size = new System.Drawing.Size(59, 24);
            this.printRptTSButton.Text = "&Print";
            this.printRptTSButton.Click += new System.EventHandler(this.printRptTSButton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Margin = new System.Windows.Forms.Padding(11, 0, 11, 0);
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 27);
            // 
            // setupButton
            // 
            this.setupButton.Image = ((System.Drawing.Image)(resources.GetObject("setupButton.Image")));
            this.setupButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.setupButton.Name = "setupButton";
            this.setupButton.Size = new System.Drawing.Size(104, 24);
            this.setupButton.Text = "&Page Setup";
            this.setupButton.Click += new System.EventHandler(this.setupButton_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripNewFuncNewSigns
            // 
            this.toolStripNewFuncNewSigns.Image = ((System.Drawing.Image)(resources.GetObject("toolStripNewFuncNewSigns.Image")));
            this.toolStripNewFuncNewSigns.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripNewFuncNewSigns.Name = "toolStripNewFuncNewSigns";
            this.toolStripNewFuncNewSigns.Size = new System.Drawing.Size(239, 24);
            this.toolStripNewFuncNewSigns.Text = "&New Functions and New Signals";
            this.toolStripNewFuncNewSigns.Click += new System.EventHandler(this.toolStripNewFuncNewSigns_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Margin = new System.Windows.Forms.Padding(11, 0, 11, 0);
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // Exit
            // 
            this.Exit.Image = ((System.Drawing.Image)(resources.GetObject("Exit.Image")));
            this.Exit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(53, 24);
            this.Exit.Text = "&Exit";
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 358);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(788, 22);
            this.statusStrip1.TabIndex = 69;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ResultsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(788, 380);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.reportDataGrid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "ResultsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Report";
            this.Shown += new System.EventHandler(this.ResultsForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.reportDataGrid)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView reportDataGrid;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton printRptTSButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton setupButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton Exit;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripNewFuncNewSigns;
        private System.Windows.Forms.DataGridViewTextBoxColumn TN_File;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDEXEC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Freames;
        private System.Windows.Forms.DataGridViewTextBoxColumn Intron;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileName;
        private System.Windows.Forms.DataGridViewButtonColumn View;
    }
}