using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using BusinessObject;
using Microsoft.Win32;

namespace CommonForms.Forms
{
    /// <summary>
    /// copied from project studio for selecting product database
    /// </summary>
    public partial class DatabaseSelectorComboBox : UserControl
    {
        public delegate void SelectedValueChanged(DBSelectorComboBoxSettings.DB choice);

        private const string ScratchDB = "Using Working DataBase";
        private const string PublicDB = "Using Public DataBase";
        private const string LocalDB = "Using Local DataBase";
        DBSelectorComboBoxSettings setting;
        private string regKeyName, regValueName;
        private SelectedValueChanged callback;
        DBSelectorComboBoxSettings.DB currentChoice;

        public DatabaseSelectorComboBox()
        {
            InitializeComponent();
            currentChoice = DBSelectorComboBoxSettings.DB.Local;
            regKeyName = "CommonFormsProject";
            regValueName = "DatabaseSelectorComboBox";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="initialSetting">initial database to be selected</param>
        /// <param name="regKeyName">registry key to store setting (like project root)</param>
        /// <param name="regValueName">registry value name to store setting (ProductDB)</param>
        /// <param name="callback">called when value changes</param>
        public void Init(DBSelectorComboBoxSettings initialSetting, 
            string regKeyName, string regValueName,
            SelectedValueChanged callback)
        {
            setting = initialSetting;
            this.regKeyName = regKeyName;
            this.regValueName = regValueName;
            this.callback = callback;

            DBSelectorComboBox.Items.Clear();

            if (setting.HasChoice(DBSelectorComboBoxSettings.DB.Public))
                DBSelectorComboBox.Items.Add(PublicDB);
            if (setting.HasChoice(DBSelectorComboBoxSettings.DB.Working))
                DBSelectorComboBox.Items.Add(ScratchDB);
            if (setting.HasChoice(DBSelectorComboBoxSettings.DB.Local))
                DBSelectorComboBox.Items.Add(LocalDB);

            SetDatabase();
        }

        public void Init(DBSelectorComboBoxSettings initialSetting, 
            string regKeyName, string regValueName,
            SelectedValueChanged callback, DBSelectorComboBoxSettings.DB choice)
        {
            Init(initialSetting, regKeyName, regValueName, callback);
            SetDatabase(choice);
        }

        private void DatabaseSelectorComboBox_Load(object sender, EventArgs e)
        {
            if (setting == null)
                return;
        }

        public void SetDatabase()
        {
            SetDatabase(currentChoice);
        }

        public void SetDatabase(DBSelectorComboBoxSettings.DB choice)
        {
            int sel = 0;
            switch (choice)
            {
                case DBSelectorComboBoxSettings.DB.Working:
                    sel = DBSelectorComboBox.FindStringExact(ScratchDB);
                    DBSelectorComboBox.BackColor = Color.LightGreen;
                    callback(DBSelectorComboBoxSettings.DB.Working);
                    break;
                case DBSelectorComboBoxSettings.DB.Public:
                    sel = DBSelectorComboBox.FindStringExact(PublicDB);
                    DBSelectorComboBox.BackColor = Color.LightSteelBlue;
                    callback(DBSelectorComboBoxSettings.DB.Public);
                    break;
                case DBSelectorComboBoxSettings.DB.Local:
                    sel = DBSelectorComboBox.FindStringExact(LocalDB);
                    DBSelectorComboBox.BackColor = Color.Tan;
                    callback(DBSelectorComboBoxSettings.DB.Local);
                    break;
            }
            DBSelectorComboBox.SelectedIndex = sel;
            currentChoice = choice;
            Registry.SetValue(regKeyName, regValueName, (int)currentChoice);
        }

        private void dbSelectorComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = DBSelectorComboBox.SelectedItem.ToString();
            switch (selected)
            {
                case PublicDB:
                    currentChoice = DBSelectorComboBoxSettings.DB.Public;
                    break;
                case ScratchDB:
                    currentChoice = DBSelectorComboBoxSettings.DB.Working;
                    break;
                case LocalDB:
                    currentChoice = DBSelectorComboBoxSettings.DB.Local;
                    break;
            }
            SetDatabase();
        }
    }
    public class DBSelectorComboBoxSettings
    {
        public enum DB { Working = 1, Public = 2, Local = 3 }
        List<DB> validChoiceList = new List<DB>();

        public DBSelectorComboBoxSettings()
        {
            if (DBConnectionString.PRODUCT.Length > 0)
                validChoiceList.Add(DB.Public);
            if (DBConnectionString.WORKPRODUCT.Length > 0)
                validChoiceList.Add(DB.Working);
            if (DBConnectionString.LOCALPRODUCT.Length > 0)
                validChoiceList.Add(DB.Local);
        }

        public bool HasChoice(DB choice)
        {
            return validChoiceList.Contains(choice);
        }
    }
}
