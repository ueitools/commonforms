using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;

namespace CommonForms.Forms
{
    public partial class NewFunctionsNewSignalsViewer : Form
    {
        #region Variables
        private NewFuncNewSignal m_NewFuncNewSig = new NewFuncNewSignal();
        public NewFuncNewSignal FunctionsSignals
        {
            get { return m_NewFuncNewSig; }
            set { m_NewFuncNewSig = value; }
        }
        #endregion

        public NewFunctionsNewSignalsViewer()
        {
            InitializeComponent();
        }

        private void NewFunctionsNewSignalsViewer_Load(object sender, EventArgs e)
        {

        }
        #region Get New Functions and New Signal Report
        internal void GetReport()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("----------------------------------------------------------------------------------------");
            sb.Append(String.Format("TN File Name:{0} ", Path.GetFileName(FunctionsSignals.InputFile)));
            sb.Append(String.Format("Date and Time: {0}", DateTime.Now.ToString()));

            if (FunctionsSignals.SignalList.Count == 0)
            {
                sb.Append("There are no new signals in this TN");
            }
            else
            {
                sb.Append(String.Format("There are {0} New Signals in this TN", FunctionsSignals.SignalList.Count.ToString()));
                foreach (KeyValuePair<String, Boolean> newsignal in FunctionsSignals.SignalList)
                {
                    sb.Append(newsignal.Key.ToString());
                }
            }
            String label = String.Empty;
            if (FunctionsSignals.FunctionList.Count == 0)
            {
                sb.Append("There are no new functions in this TN");
            }
            else
            {
                sb.Append(String.Format("There are {0} new functions in this TN", FunctionsSignals.FunctionList.Count.ToString()));
                foreach (KeyValuePair<String, Boolean> newfunction in FunctionsSignals.FunctionList)
                {
                    if (newfunction.Value == true)
                    {
                        if (FunctionsSignals.LabelList.ContainsKey(newfunction.Key))
                        {
                            label = FunctionsSignals.LabelList[newfunction.Key].ToString();
                            sb.Append(String.Format("Signal {0} , Label: {1}", newfunction.Key.ToString(), label));
                        }
                    }
                }
            }
            sb.Append("----------------------------------------------------------------------------------------");
            txtResults.Text = sb.ToString();
            using (TextWriter tw = TextWriter.Synchronized(File.AppendText(CommonForms.Configuration.GetWorkingDirectory() + "NewSignalsNewFunctionsInfo.txt")))
            {
                tw.Write(sb.ToString());
                tw.Close();
            }
        }
        #endregion
    }
}