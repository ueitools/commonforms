using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace CommonForms
{
    public partial class TextReportViewerForm : Form
    {
        public TextReportViewerForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        public void Open(string fileName)
        {  
            FileInfo fileInfo = new FileInfo(fileName);
            StreamReader reader = fileInfo.OpenText( );

            string line = "";

            do
            {
                line = reader.ReadLine( );
                reportTB.Text += line;
                reportTB.Text += "\r\n";
            } while ( line != null );

            this.Show();
        }
    }
}