using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CommonForms
{
    public partial class ProcessingForm : Form
    {
        public ProcessingForm()
        {
            InitializeComponent();
        }

        public void SetProgress(int percent)
        {
            Percentage.Text = "" + percent + "%";
            progressBar.Value = percent;
        }

        public void SetMessage(string s)
        {
            textBox.Text = s;
            Application.DoEvents();
        }

        public void DoEvents()
        {
            Application.DoEvents();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ProcessingForm_Shown(object sender, EventArgs e)
        {
            progressBar.Maximum = 100;
            progressBar.Minimum = 0;

            this.Update();
        }

        private void ProcessingForm_MouseEnter(object sender, EventArgs e)
        {
            Application.DoEvents();
        }
    }
}