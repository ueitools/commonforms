namespace CommonForms.Forms
{
    partial class FunctionsSignalsViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripNewSignals = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripNewFunctions = new System.Windows.Forms.ToolStripStatusLabel();
            this.dgNewFuncsNewSigns = new System.Windows.Forms.DataGridView();
            this.statusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNewFuncsNewSigns)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripNewSignals,
            this.toolStripStatusLabel3,
            this.toolStripNewFunctions});
            this.statusStrip.Location = new System.Drawing.Point(0, 256);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip.Size = new System.Drawing.Size(627, 25);
            this.statusStrip.TabIndex = 0;
            this.statusStrip.Text = "Status";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(101, 20);
            this.toolStripStatusLabel1.Text = "New Signals : ";
            // 
            // toolStripNewSignals
            // 
            this.toolStripNewSignals.Name = "toolStripNewSignals";
            this.toolStripNewSignals.Size = new System.Drawing.Size(17, 20);
            this.toolStripNewSignals.Text = "0";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(116, 20);
            this.toolStripStatusLabel3.Text = "New Functions : ";
            // 
            // toolStripNewFunctions
            // 
            this.toolStripNewFunctions.Name = "toolStripNewFunctions";
            this.toolStripNewFunctions.Size = new System.Drawing.Size(17, 20);
            this.toolStripNewFunctions.Text = "0";
            // 
            // dgNewFuncsNewSigns
            // 
            this.dgNewFuncsNewSigns.AllowUserToAddRows = false;
            this.dgNewFuncsNewSigns.AllowUserToDeleteRows = false;
            this.dgNewFuncsNewSigns.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgNewFuncsNewSigns.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgNewFuncsNewSigns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgNewFuncsNewSigns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgNewFuncsNewSigns.GridColor = System.Drawing.SystemColors.Control;
            this.dgNewFuncsNewSigns.Location = new System.Drawing.Point(0, 0);
            this.dgNewFuncsNewSigns.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgNewFuncsNewSigns.Name = "dgNewFuncsNewSigns";
            this.dgNewFuncsNewSigns.ReadOnly = true;
            this.dgNewFuncsNewSigns.Size = new System.Drawing.Size(627, 256);
            this.dgNewFuncsNewSigns.TabIndex = 1;
            // 
            // FunctionsSignalsViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(627, 281);
            this.Controls.Add(this.dgNewFuncsNewSigns);
            this.Controls.Add(this.statusStrip);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FunctionsSignalsViewer";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "New Functions and Signals Viewer";
            this.Load += new System.EventHandler(this.FunctionsSignalsViewer_Load);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNewFuncsNewSigns)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripNewSignals;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripNewFunctions;
        private System.Windows.Forms.DataGridView dgNewFuncsNewSigns;
    }
}