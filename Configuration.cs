using System.Windows.Forms;
using System;
using System.IO;
using System.Xml;
using Microsoft.Win32;

namespace CommonForms {
    /// <summary>
    /// this is global working directory for all application
    /// each application can 
    /// </summary>
    public static class Configuration {
        private const string _UEITOOLSROOT =
            "HKEY_CURRENT_USER\\Software\\UEITools\\";

        private const string _WORKINGDIR = "WorkingDirectory";
        private const string _DEFAULT_WORKINGDIR = "C:\\Working";

        /// <summary>
        /// Set working directory for unspecified project
        /// </summary>
        public static void SetWorkingDirectory()
        {
            SetWorkingDirectory("");
        }


        /// <summary>
        /// Set Working Directory for a specific project
        /// </summary>
        /// <param name="subDir">Project Sub Directory</param>
        public static void SetWorkingDirectory(string subDir) {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.Description = "Choose your working directory";
            dlg.SelectedPath = GetWorkingDirectory(subDir);

            if (dlg.ShowDialog() == DialogResult.OK) {
                if (dlg.SelectedPath.EndsWith("\\") == false) {
                    dlg.SelectedPath += "\\";
                }

                Registry.SetValue(_UEITOOLSROOT + subDir, _WORKINGDIR,
                                  dlg.SelectedPath, RegistryValueKind.String);
            }
        }

        /// <summary>
        /// Get working directory for unspecified project
        /// </summary>
        /// <returns></returns>
        public static string GetWorkingDirectory()
        {
            return GetWorkingDirectory("");
        }

        /// <summary>
        /// Get working directory for specific project
        /// </summary>
        /// <param name="subDir">Project Sub Directory</param>
        /// <returns></returns>
        public static string GetWorkingDirectory(string subDir) {
            return (string)Registry.GetValue(
                               _UEITOOLSROOT + subDir, _WORKINGDIR, _DEFAULT_WORKINGDIR);
        }

        private static string GetRegistryPath(string subDir) {
            string registryPath = _UEITOOLSROOT;
            if (string.IsNullOrEmpty(subDir) == false) {
                registryPath = _UEITOOLSROOT + subDir + "\\";
            }
            return registryPath;
        }

        public static void SetConfigItem(string item, string value) {
            Registry.SetValue(_UEITOOLSROOT, item, value, RegistryValueKind.String);
        }

        public static string GetConfigItem(string item) {
            return Registry.GetValue(_UEITOOLSROOT, item, "") as string;
        }

        public static string GetConfigItem(string subDir, string item) {
            return Registry.GetValue(GetRegistryPath(subDir), item, "") as string;
        }

        public static void SetConfigItem(string subDir, string item, string value) {
            Registry.SetValue(GetRegistryPath(subDir), item, value, RegistryValueKind.String);
        }

        public static string GetConfigItem(string subDir, string item, string defaultValue) {
            return Registry.GetValue(GetRegistryPath(subDir), item, defaultValue) as string;
        }

        public static void SetConfigItem(string subDir, string item, int value) {
            Registry.SetValue(GetRegistryPath(subDir), item, value, RegistryValueKind.DWord);
        }

        public static int GetConfigItem(string subDir, string item, int defaultValue) {
            object value = Registry.GetValue(GetRegistryPath(subDir), item, defaultValue);
            if (value == null) {
                value = defaultValue;
            }
            return (int)value;
        }

        public static void SetConfigItem(string subDir, string item, bool value) {
            Registry.SetValue(GetRegistryPath(subDir), item, value ? 1 : 0, RegistryValueKind.DWord);
        }

        public static bool GetConfigItem(string subDir, string item, bool defaultValue) {
            object value = Registry.GetValue(GetRegistryPath(subDir), item, defaultValue ? 1 : 0);
            if (value == null) {
                value = defaultValue ? 1 : 0;
            }
            return (int)value != 0;
        }
    }

    public static class WorkflowConfig
    {
        const string _CONFIGFILE = "UEI Workflow 2010.exe.config";
        const string _QUERY = "//configuration/appSettings/add";
        const string _KEY = "key";
        const string _VALUE = "value";

        static XmlNodeList Cfg_nodeList;

        static WorkflowConfig()
        {
            XmlDocument doc = new XmlDocument();
            string configFile = AppDomain.CurrentDomain.BaseDirectory
                + Path.DirectorySeparatorChar + _CONFIGFILE;
            doc.Load(configFile);

            Cfg_nodeList = doc.SelectNodes(_QUERY);
        }

        public static string GetCfg(string key)
        {
            string tmp = "";
            foreach (XmlNode node in Cfg_nodeList)
            {
                if (node.Attributes[_KEY].Value == key)
                {
                    tmp = node.Attributes[_VALUE].Value;
                    break;
                }
            }
            return tmp;
        }
    }
}