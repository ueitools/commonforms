using System;
using System.Collections.Generic;
using System.Text;

namespace CommonForms
{
    /// <summary>
    /// 
    /// </summary>
    public class CFILineInfo
    {
        #region Static Members

        public static double PULSE_TOLERANCE = 0.06;
        public static double DELAY_TOLERANCE = 0.06;
        public static double CODEGAP_TOLERANCE = 0.06;
        public static double PULSE_DELAY_TOLERANCE = 0.06;

        public static void SET_TOLERANCE(double pulseTolerance,
            double delayTolerance, double codeGapTolerance)
        {
            SET_TOLERANCE(pulseTolerance, delayTolerance, codeGapTolerance, 
                100 * PULSE_DELAY_TOLERANCE);
        }

        public static void SET_TOLERANCE(double pulseTolerance, 
            double delayTolerance, double codeGapTolerance,
            double pulseDelayTolerance)
        {
            PULSE_TOLERANCE = pulseTolerance / 100;
            DELAY_TOLERANCE = delayTolerance / 100;
            CODEGAP_TOLERANCE = codeGapTolerance / 100;
            PULSE_DELAY_TOLERANCE = pulseDelayTolerance / 100;
        }

        #endregion

        public string Name;
        public int Value;
        public int lineNo;
        public bool LastPulse;

        public int delay = 0;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType())
                return false;

            CFILineInfo lineInfo = (CFILineInfo)obj;

            if (Name != lineInfo.Name)
                return false;

            if (ValueDiff(lineInfo) != 0)
                return false;

            return true;
        }

        public double ValueDiff(CFILineInfo lineInfo)
        {
            double tolerance;

            if (Name == "Pulse")
                tolerance = PULSE_TOLERANCE;
            else if (Name == "Delay")
                tolerance = DELAY_TOLERANCE;
            else if (Name == "CodeGap")
                tolerance = CODEGAP_TOLERANCE;
            else if (Name == "Data")
                return 0;
            else throw new Exception("Error: wrong name: " + Name);

            double diff = Math.Abs(this.Value - lineInfo.Value);
            double min = Math.Min(this.Value, lineInfo.Value);

            if (diff > tolerance * min)
            {
                if (min == 0)
                    return double.MaxValue;

                return diff / min;
            }

            return 0;
        }

        public double PulseDelayDev(CFILineInfo lineInfo)
        {
            if (Name != lineInfo.Name)
                return -1;

            if (Name != "Pulse") 
                return -1;

            double tolerance = PULSE_DELAY_TOLERANCE;

            double diff = Math.Abs((this.Value + this.delay) 
                - ( lineInfo.Value + lineInfo.delay));
            double min = Math.Min(this.Value + this.delay,
                lineInfo.Value + lineInfo.delay);

            if (diff > tolerance * min)
            {
                if (min == 0)
                    return double.MaxValue;

                return diff / min;
            }

            return 0;
        }
    }

    public class CFIFrameInfo
    {
        private int _lineIndex;
        private string _frameData;

        public int LineIndex
        {
            get { return _lineIndex; }
            set { _lineIndex = value; }
        }

        public string FrameData
        {
            get { return _frameData; }
            set { _frameData = value; }
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != GetType())
                return false;

            CFIFrameInfo cfiFrameInfo = (CFIFrameInfo)obj;

            return cfiFrameInfo._lineIndex == _lineIndex && cfiFrameInfo._frameData == _frameData;
        }

        public class List : List<CFIFrameInfo>
        {
            public override bool Equals(object obj)
            {
                if (obj.GetType() != GetType())
                    return false;

                List cfiFrameInfoList = (List)obj;

                if (cfiFrameInfoList.Count != Count)
                    return false;

                for (int frameInfoCount = 0; frameInfoCount < cfiFrameInfoList.Count; frameInfoCount++)
                {
                    if (cfiFrameInfoList[frameInfoCount].Equals(this[frameInfoCount]) == false)
                        return false;
                }

                return true;
            }
        }
    }

    public class CFIInfo
    {
        #region Static Members
        public static double FREQUENCY_TOLERANCE = 0.1;
        public static bool FRAME_COUNT = false;
        public static bool MASK_TOGGLE_BITS = true;

        public static void SET_TOLERANCE(double frequencyTolerance, double pulseTolerance, double delayTolerance, double codeGapTolerance, double pulseDelayTolerance, bool frameCount, bool maskToggleBits)
        {
            FREQUENCY_TOLERANCE = frequencyTolerance / 100;
            FRAME_COUNT = frameCount;
            MASK_TOGGLE_BITS = maskToggleBits;

            CFILineInfo.SET_TOLERANCE(pulseTolerance, delayTolerance,
                codeGapTolerance, pulseDelayTolerance);
        }

        public static void SET_TOLERANCE(double frequencyTolerance,
            double pulseTolerance, double delayTolerance,
            double codeGapTolerance, double pulseDelayTolerance)
        {
            FREQUENCY_TOLERANCE = frequencyTolerance / 100;

            CFILineInfo.SET_TOLERANCE(pulseTolerance, delayTolerance,
                codeGapTolerance, pulseDelayTolerance);
        }

        public static void SET_TOLERANCE(double frequencyTolerance,
            double pulseTolerance, double delayTolerance,
            double codeGapTolerance)
        {
            FREQUENCY_TOLERANCE = frequencyTolerance / 100;

            CFILineInfo.SET_TOLERANCE(pulseTolerance, delayTolerance,
                codeGapTolerance);
        }

        #endregion

        public double Frequency;
        public int DutyCycle;
        public int NumFrame;
        public List<string> FrameData;
        public int IndexOfLastFrames;

        public List<CFILineInfo> LineList;
        private CFIFrameInfo.List _frameList;

        public CFIInfo()
        {
            LineList = new List<CFILineInfo>();
        }

        public CFIFrameInfo.List FrameList
        {
            get { return _frameList; }
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType())
                return false;

            CFIInfo cfiInfo = (CFIInfo)obj;

            if ( FrequencyDev(cfiInfo) > 0 )
                return false;

            if (LineList == null && cfiInfo.LineList == null)
                return true;

            if (LineList != null && cfiInfo.LineList == null)
                return false;

            if (LineList == null && cfiInfo.LineList != null)
                return false;

            if (LineList.Count != cfiInfo.LineList.Count)
                return false;

            if (this.NumFrame != cfiInfo.NumFrame)
                return false;

            for (int i = 0; i < NumFrame; i++)
            {
                if (FrameData[i] != cfiInfo.FrameData[i])
                    return false;
            }

            for (int i = 0; i < LineList.Count; i++)
            {
                if (!LineList[i].Equals(cfiInfo.LineList[i]))
                    return false;
            }

            _frameList.Equals(cfiInfo.FrameList);

            return true;
        }

        public double FrequencyDev(CFIInfo cfiInfo)
        {
            double diff = Math.Abs(this.Frequency - cfiInfo.Frequency);
            double min = Math.Min(this.Frequency, cfiInfo.Frequency);

            if (diff > FREQUENCY_TOLERANCE * min)
            {
                if (min == 0)
                    return double.MaxValue;

                return diff / min;
            }

            return 0;
        }

        public void Format()
        {
            NumFrame = 0;

            if (LineList == null)
                return;


            FrameData = new List<string>();
            _frameList = new CFIFrameInfo.List();
            IndexOfLastFrames = 0;

            CFILineInfo pulseLineInfo = null;
            CFILineInfo lastPulseLineInfo = null;
            CFIFrameInfo currentFrame = null;
            string dataInfo = null;
            int indexOfCodeGap = -1, index = 0;

            foreach (CFILineInfo lineInfo in LineList)
            {
                if (lineInfo.Name == "CodeGap")
                {
                    NumFrame++;

                    if (dataInfo != null)
                        FrameData.Add(dataInfo);
                    else FrameData.Add("has no data");

                    if (pulseLineInfo != null)
                        pulseLineInfo.delay += lineInfo.Value;

                    IndexOfLastFrames = indexOfCodeGap + 1;
                    indexOfCodeGap = index;

                    if (currentFrame != null)
                    {
                        if (dataInfo != null)
                            currentFrame.FrameData = dataInfo;
                        else
                            currentFrame.FrameData = "has no data";

                        _frameList.Add(currentFrame);
                    }

                    dataInfo = null;
                    pulseLineInfo = null;
                    currentFrame = null;
                }
                else if (lineInfo.Name == "Pulse")
                {
                    pulseLineInfo = lineInfo;
                    lastPulseLineInfo = pulseLineInfo;

                    if (currentFrame == null)
                    {
                        currentFrame = new CFIFrameInfo();
                        currentFrame.LineIndex = index;
                    }
                }
                else if (lineInfo.Name == "Delay"
                    && pulseLineInfo != null)
                {
                    pulseLineInfo.delay += lineInfo.Value;

                    if (currentFrame == null)
                    {
                        currentFrame = new CFIFrameInfo();
                        currentFrame.LineIndex = index;
                    }
                }
                else if (lineInfo.Name == "Data")
                {
                    dataInfo = "Valid";
                    pulseLineInfo = null;

                    if (currentFrame == null)
                    {
                        currentFrame = new CFIFrameInfo();
                        currentFrame.LineIndex = index;
                    }
                }
                else
                {
                    pulseLineInfo = null;
                }

                index++;
            }

            if (lastPulseLineInfo != null) 
                lastPulseLineInfo.LastPulse = true;
        }
    }
}
