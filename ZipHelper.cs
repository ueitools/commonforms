using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.IO.Compression;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Checksums;


namespace CommonForms
{
    public class ZipHelper
    {
        public static void CreateZipFromDirectory(string ZipArchive, string SourcePath)
        {
            List<string> WorkFiles  = new List<string>();
            string[] FileList = Directory.GetFiles(SourcePath);
            foreach (string file in FileList)
                WorkFiles.Add(Path.GetFileName(file));

            CreateZipFile(ZipArchive, WorkFiles, SourcePath);
        }

        public static void CreateZipFile(string zipArchive, List<string> workFiles, 
                                        string workPath)
        {
            if (string.IsNullOrEmpty(zipArchive))
                return;
            if (workFiles == null || workFiles.Count < 1)
                return;

            string filePath;

            if (!string.IsNullOrEmpty(workPath) && !workPath.EndsWith("\\"))
                workPath += "\\";

            Crc32 crc = new Crc32();
            ZipOutputStream s = new ZipOutputStream(File.Create(zipArchive));

            s.SetLevel(6); // 0 - store only to 9 - means best compression

            foreach (string file in workFiles)
            {
                if (string.IsNullOrEmpty(file))
                    continue;
                filePath = workPath + file;
                if (!File.Exists(filePath))
                    continue;
                FileStream fs = File.OpenRead(filePath);
                byte[] buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                ZipEntry entry = new ZipEntry(file);
                entry.DateTime = File.GetLastWriteTime(filePath);
                // set Size and the crc
                entry.Size = fs.Length;
                fs.Close();
                crc.Reset();
                crc.Update(buffer);
                entry.Crc = crc.Value;
                s.PutNextEntry(entry);
                s.Write(buffer, 0, buffer.Length);
            }

            s.Finish();
            s.Close();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="zipArchive"></param>
        /// <param name="filePath"></param>
        public static void ExtractAll(string zipArchive, string filePath)
        {
            ZipInputStream zipInputStream = new ZipInputStream(File.OpenRead(zipArchive));

            ZipEntry entry = null;
            string curFileName = null;
            FileStream streamWriter = null;

            while ((entry = zipInputStream.GetNextEntry()) != null)
            {
                curFileName = Path.GetFileName(entry.Name);
                streamWriter = File.Create(filePath + curFileName);
                if (entry.Size > 0) {
                    StreamCopy(streamWriter, zipInputStream);
                }
                streamWriter.Close();
                File.SetCreationTime(filePath + curFileName, entry.DateTime);
                File.SetLastWriteTime(filePath + curFileName, entry.DateTime);
            }
        }

        public static Dictionary<String, String[]>
            ExtractAllToStringArray(string zipArchive, string filePath)
        {
            Dictionary<String, MemoryStream> memstreamSet =
                ExtractAllToMemoryStream(zipArchive, filePath);
            Dictionary<String, String[]> result = new Dictionary<string, string[]>();

            foreach (string filename in memstreamSet.Keys)
            {
                result[filename] = ConvertToArray(memstreamSet[filename]);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="zipArchive"></param>
        /// <param name="filename"></param>
        public static void ExtractFile(string zipArchive, string fileName,
            string filePath)
        {
            ZipInputStream zipInputStream = new ZipInputStream(File.OpenRead(zipArchive));

            ZipEntry entry = null;
            string curFileName = null;
            while ((entry = zipInputStream.GetNextEntry()) != null)
            {
                curFileName = Path.GetFileName(entry.Name);
                if (fileName == curFileName)
                {
                    FileStream streamWriter =
                        File.Create(filePath + fileName);

                    StreamCopy(streamWriter, zipInputStream);
                    streamWriter.Close();
                    File.SetCreationTime(filePath + fileName, entry.DateTime);
                    File.SetLastWriteTime(filePath + fileName, entry.DateTime);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="zipArchive"></param>
        /// <param name="filePath"></param>
        private static Dictionary<String, MemoryStream>
            ExtractAllToMemoryStream(string zipArchive, string filePath)
        {
            Dictionary<String, MemoryStream> streamDictionary =
                new Dictionary<string, MemoryStream>();
            ZipInputStream zipInputStream =
                new ZipInputStream(File.OpenRead(zipArchive));
            ZipEntry entry = null;

            while ((entry = zipInputStream.GetNextEntry()) != null)
            {
                MemoryStream ms = new MemoryStream();
                if (entry.Size > 0)
                {
                    StreamCopy(ms, zipInputStream);
                }
                ms.Close();
                streamDictionary.Add(entry.Name, ms);
            }
            return streamDictionary;
        }

        /// <summary>
        /// buffered write
        /// </summary>
        /// <param name="zipInputStream"></param>
        /// <param name="streamWriter"></param>
        private static void StreamCopy(Stream destStream,
            Stream srcStream)
        {
            int size = 2048;
            byte[] data = new byte[2048];
            while (true)
            {
                size = srcStream.Read(data, 0, data.Length);
                if (size > 0)
                {
                    destStream.Write(data, 0, size);
                }
                else
                {
                    break;
                }
            }
        }

        public static byte[] Compress(byte[] buffer)
        {
            MemoryStream ms = new MemoryStream();
            using (GZipStream zip = new GZipStream(ms, CompressionMode.Compress, true))
            {
                zip.Write(buffer, 0, buffer.Length);
            }

            ms.Position = 0;
            MemoryStream outStream = new MemoryStream();

            byte[] compressed = new byte[ms.Length];
            ms.Read(compressed, 0, compressed.Length);

            byte[] gzBuffer = new byte[compressed.Length + 4];
            System.Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
            System.Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4);
            return gzBuffer;
        }

        public static string Decompress(byte[] gzBuffer)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                int msgLength = BitConverter.ToInt32(gzBuffer, 0);
                ms.Write(gzBuffer, 4, gzBuffer.Length - 4);

                byte[] buffer = new byte[msgLength];

                ms.Position = 0;
                using (GZipStream zip = new GZipStream(ms, CompressionMode.Decompress))
                {
                    zip.Read(buffer, 0, buffer.Length);
                }
                return Encoding.UTF8.GetString(buffer);
            }
        }

        public static string Compress(string text)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            MemoryStream ms = new MemoryStream();
            using (GZipStream zip = new GZipStream(ms, CompressionMode.Compress, true))
            {
                zip.Write(buffer, 0, buffer.Length);
            }

            ms.Position = 0;
            MemoryStream outStream = new MemoryStream();

            byte[] compressed = new byte[ms.Length];
            ms.Read(compressed, 0, compressed.Length);

            byte[] gzBuffer = new byte[compressed.Length + 4];
            System.Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
            System.Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4);
            return Convert.ToBase64String(gzBuffer);
        }

        public static string Decompress(string compressedText)
        {
            byte[] gzBuffer = Convert.FromBase64String(compressedText);
            using (MemoryStream ms = new MemoryStream())
            {
                int msgLength = BitConverter.ToInt32(gzBuffer, 0);
                ms.Write(gzBuffer, 4, gzBuffer.Length - 4);

                byte[] buffer = new byte[msgLength];

                ms.Position = 0;
                using (GZipStream zip = new GZipStream(ms, CompressionMode.Decompress))
                {
                    zip.Read(buffer, 0, buffer.Length);
                }

                return Encoding.UTF8.GetString(buffer);
            }
        }

        private static string[] ConvertToArray(MemoryStream ms)
        {
            byte[] byteArray = ms.ToArray();
            string text = Encoding.Default.GetString(byteArray);
            StringReader sr = new StringReader(text);
            List<string> result = new List<string>();
            string line;

            while ((line = sr.ReadLine()) != null)
            {
                result.Add(line);
            }
            return result.ToArray();
        }
    }
}
