using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;
using BusinessObject;
using System.Data;
using System.Collections.ObjectModel;

namespace CommonForms
{
    public class FunctionsSignals : Collection<NewFunctionsNewSignals>
    {
        #region Variables
        private static FunctionsSignals m_Instance = null;
        #endregion        

        #region Properties
        private Boolean m_IsClicked = false;
        public Boolean IsClicked
        {
            get { return m_IsClicked; }
            set { m_IsClicked = value; }
        }
        private Int32 m_NewSignals = 0;
        public Int32 NewSignals
        {
            get { return m_NewSignals; }
            set { m_NewSignals = value; }
        }
        private Int32 m_NewFunctions = 0;
        public Int32 NewFunctions
        {
            get { return m_NewFunctions; }
            set { m_NewFunctions = value; }
        }
        #endregion

        #region Constrcutor
        private FunctionsSignals() { }        
        public static FunctionsSignals GetInstance()
        {
            if (m_Instance == null)
                m_Instance = new FunctionsSignals();
            return m_Instance;
        }
        #endregion

        #region Set Signal Flag
        public void SetNewSignalFlag(String m_InputFile, String m_FileName, Boolean flag)
        {
            if (this.Items != null)
            {
                foreach (NewFunctionsNewSignals item in this.Items)
                {
                    if (item.InputFile == m_InputFile)
                    {
                        item.SetNewSignalFlag(m_FileName, flag);
                    }
                }
            }            
        }
        #endregion

        #region Get New Signal Information
        public void GetNewSignalInfo()
        {
            if (this.Items != null)
            {
                foreach (NewFunctionsNewSignals item in this.Items)
                {
                    for (int m_Count = 0; m_Count < item.CaptureFiles.Length; m_Count++)
                    {
                        String m_FileName = Path.GetFileName(item.CaptureFiles[m_Count].ToUpper());
                        if (m_FileName.Contains("U1") || m_FileName.Contains("U2"))
                        {
                            if (!item.SignalInfo.ContainsKey(m_FileName))
                            {
                                if (!item.SignalList.ContainsKey(m_FileName))
                                {
                                    item.SignalList.Add(m_FileName, true);
                                }
                            }
                        }
                    }
                }
            }            
        }
        #endregion

        #region Get New Function Information
        public void GetNewFunctionInfo()
        {
            if (this.Items != null)
            {
                String m_Labels = String.Empty;
                foreach (NewFunctionsNewSignals item in this.Items)
                {
                    m_Labels = String.Empty;
                    m_Labels = GetLableList(item.LabelList, item.CaptureFiles);
                    List<String> m_Functions = new List<String>();
                    m_Functions = GetNewLabelList(item.Modes, m_Labels);
                    foreach (String m_Function in m_Functions)
                    {
                        for (Int32 m_Count = 0; m_Count < item.CaptureFiles.Length; m_Count++)
                        {
                            String m_FileName = Path.GetFileName(item.CaptureFiles[m_Count].ToUpper());
                            if (m_FileName.Contains("U1") || m_FileName.Contains("U2"))
                            {
                                if (item.LabelList[m_FileName].ToString() == m_Function)
                                {
                                    item.FunctionList.Add(m_FileName, true);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region Get Label List
        private String GetLableList(Hashtable m_LabelList, String[] m_CaptureFiles)
        {
            String m_Labels = String.Empty;
            String m_FileName = String.Empty;
            for (int m_Count = 0; m_Count < m_CaptureFiles.Length; m_Count++)
            {
                m_FileName = Path.GetFileName(m_CaptureFiles[m_Count].ToUpper());
                if (m_FileName.Contains("U1") || m_FileName.Contains("U2"))
                {
                    if (m_LabelList.Contains(m_FileName.ToString()))
                    {
                        m_Labels += m_LabelList[m_FileName].ToString() + ",";
                    }
                }

            }
            if (m_Labels.Length > 0)
                m_Labels.Substring(0, m_Labels.Length - 1);
            return m_Labels;
        }
        #endregion

        #region Get New Label List
        private List<String> GetNewLabelList(String m_Modes, String m_Labels)
        {
            List<String> m_NewLabels    = new List<String>();
            FunctionDAO m_FunctionDao   = new FunctionDAO();
            List<Hashtable> m_Result    = (List<Hashtable>)m_FunctionDao.LabelSearch(m_Modes, m_Labels);
            foreach (Hashtable ht in m_Result)
            {
                if (ht["Label"].ToString() != String.Empty && ht["IntronAvailability"].ToString() == "0")
                {
                    if (!m_NewLabels.Contains(ht["Label"].ToString()))
                        m_NewLabels.Add(ht["Label"].ToString());
                }
            }
            return m_NewLabels;
        }
        #endregion

        #region Get New Functions and New Signal Report
        public DataTable GetReport()
        {
            this.NewSignals     = 0;
            this.NewFunctions   = 0;
            DataTable dtFuntionsSingnals = null;
            if (this.Items != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("#########################################");
                sb.AppendLine("##### New Signals and New Functions #####");
                sb.AppendLine("#########################################");
                dtFuntionsSingnals = new DataTable("NewFunctionsNewSignals");                
                dtFuntionsSingnals.Columns.Add("TN File", Type.GetType("System.String"));
                dtFuntionsSingnals.Columns.Add("Signal File", Type.GetType("System.String"));
                dtFuntionsSingnals.Columns.Add("New Signal", Type.GetType("System.String"));
                dtFuntionsSingnals.Columns.Add("New Function", Type.GetType("System.String"));
                dtFuntionsSingnals.AcceptChanges();
                DataRow dr = null;
                foreach (NewFunctionsNewSignals item in this.Items)
                {
                    String[] m_ZipFileName = Path.GetFileName(item.InputFile).Replace(".ZIP", "").ToString().Split(' ');
                    sb.AppendLine("");
                    sb.AppendLine("----------------------------------------");
                    sb.AppendLine(String.Format("TN File Name : {0}", Path.GetFileName(item.InputFile)));
                    sb.AppendLine(String.Format("Date and Time: {0}", DateTime.Now.ToString()));

                    if (item.SignalList.Count == 0)
                    {                        
                        sb.AppendLine(String.Format("There are no New Signals in this TN File"));
                    }
                    else
                    {
                        this.NewSignals += item.SignalList.Count;
                        sb.AppendLine(String.Format("There are {0} New Signals in this TN File",item.SignalList.Count.ToString()));
                        foreach (KeyValuePair<String, Boolean> m_NewSignal in item.SignalList)
                        {
                            dr      = dtFuntionsSingnals.NewRow();
                            dr[0]   = m_ZipFileName[0];
                            dr[1]   = m_NewSignal.Key.ToString();
                            if (m_NewSignal.Value == true)
                                dr[2] = "Yes";
                            else
                                dr[2] = "-";
                            dtFuntionsSingnals.Rows.Add(dr);
                            dr = null;
                            sb.AppendLine(m_NewSignal.Key.ToString());
                        }
                    }
                    String m_Label = String.Empty;
                    if (item.FunctionList.Count == 0)
                    {
                        sb.AppendLine(String.Format("There are no New Functions in this TN File"));                        
                    }
                    else
                    {
                        this.NewFunctions += item.FunctionList.Count;
                        sb.AppendLine(String.Format("There are {0} New Functions in this TN File", item.FunctionList.Count.ToString()));
                        foreach (KeyValuePair<String, Boolean> m_Newfunction in item.FunctionList)
                        {
                            if (m_Newfunction.Value == true)
                            {
                                if (item.LabelList.ContainsKey(m_Newfunction.Key))
                                {
                                    m_Label = item.LabelList[m_Newfunction.Key].ToString();
                                    sb.AppendLine(String.Format("Signal {0} , Label: {1}", m_Newfunction.Key.ToString(), m_Label));
                                    if (dtFuntionsSingnals.Rows.Count > 0)
                                    {
                                        DataRow[] rows = dtFuntionsSingnals.Select("[TN File]='"+ m_ZipFileName[0] +"' and [Signal File]='" + m_Newfunction.Key.ToString() + "'");
                                        if (rows.Length > 0)
                                            foreach (DataRow d in rows)
                                            {
                                                d[3] = m_Label;
                                            }
                                        else
                                        {
                                            dr = dtFuntionsSingnals.NewRow();
                                            dr[0] = m_ZipFileName[0];
                                            dr[1] = m_Newfunction.Key.ToString();
                                            dr[2] = "-";
                                            dr[3] = m_Label;
                                            dtFuntionsSingnals.Rows.Add(dr);
                                            dr = null;
                                        }
                                    }
                                    else
                                    {
                                        dr = dtFuntionsSingnals.NewRow();
                                        dr[0] = m_ZipFileName[0];
                                        dr[1] = m_Newfunction.Key.ToString();
                                        dr[2] = "-";
                                        dr[3] = m_Label;
                                        dtFuntionsSingnals.Rows.Add(dr);
                                        dr = null;
                                    }
                                }
                            }
                        }
                    }
                    dtFuntionsSingnals.AcceptChanges();
                    sb.AppendLine("----------------------------------------");                                     
                }
                using (TextWriter tw = TextWriter.Synchronized(File.AppendText(CommonForms.Configuration.GetWorkingDirectory() + "NewSignalsNewFunctions.txt")))
                {
                    tw.WriteLine(sb.ToString());
                    tw.Close();
                }
            }
            IsClicked = true;
            return dtFuntionsSingnals;
        }
        #endregion

        #region New Function New Signal Object
        public NewFunctionsNewSignals GetFunctionSignalObject(String m_InputFile)
        {
            NewFunctionsNewSignals m_NewFunctionsNewSignals = null;
            if (this.Items != null)
            {
                foreach (NewFunctionsNewSignals item in this.Items)
                {
                    return item;
                }
            }
            return m_NewFunctionsNewSignals;
        }
        #endregion
    }
    public class NewFunctionsNewSignals
    {
        #region Variables
        private Dictionary<String, Boolean> m_SignalList = null;
        public Dictionary<String, Boolean> SignalList
        {
            get { return m_SignalList; }
            set { m_SignalList = value; }
        }
        private Dictionary<String, Boolean> m_FunctionList = null;
        public Dictionary<String, Boolean> FunctionList
        {
            get { return m_FunctionList; }
            set { m_FunctionList = value; }
        }
        private Dictionary<String, Boolean> m_SingnalInfo = null;
        public Dictionary<String, Boolean> SignalInfo
        {
            get { return m_SingnalInfo; }
            set { m_SingnalInfo = value; }
        }	
        private Hashtable m_LabelList = null;
        public Hashtable LabelList
        {
            get { return m_LabelList; }
            set { m_LabelList = value; }
        }
        private String m_InputFile = String.Empty;
        public String InputFile
        {
            get { return m_InputFile; }
            set { m_InputFile = value; }
        }
        private String[] m_CaptureFiles;
        public String[] CaptureFiles
        {
            get { return m_CaptureFiles; }
            set { m_CaptureFiles = value; }
        }
        private String m_Modes = String.Empty;
        public String Modes
        {
            get { return m_Modes; }
            set { m_Modes = value; }
        }        
        #endregion

        #region Constructor
        public NewFunctionsNewSignals()
        {
            SignalList      = new Dictionary<String, Boolean>();
            FunctionList    = new Dictionary<String, Boolean>();
            m_SingnalInfo   = new Dictionary<String, Boolean>();
            Modes           = String.Empty;
            CaptureFiles    = new String[] { };
            InputFile       = String.Empty;            
            LabelList       = new Hashtable();
        }        
        #endregion        

        #region Get New Signal Information       
        internal void SetNewSignalFlag(String m_FileName,Boolean flag)
        {
            if (!m_SingnalInfo.ContainsKey(m_FileName))
            {
                m_SingnalInfo.Add(m_FileName, flag);
            }
        }
        #endregion        
    }
}
