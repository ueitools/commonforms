﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;
using System.IO;
using BusinessObject;
//Purpose: This class perform the following tasks
//1. Finding conflict, description for Intron and Label
//2. Validate Label present / Not for the given ID
//3. Providing additional details to Search Report
//4. This call is being called from Compare class whenever required
namespace CommonForms
{
    public class IntronLabelMapper
    {
        protected DataSet dsMap = null;
        protected DataTable functionTable = null;
        protected string _dbConnectionType = string.Empty;

        public IntronLabelMapper(string dbType)
        {
            this._dbConnectionType = getConnection(dbType);
        }
        private string getConnection(string dbType)
        {
            string connectionType = string.Empty;
            if (dbType == "Q")
            {
                connectionType = DBConnectionString.UEITEMP;
            }
            if (dbType == "U")
            {
                connectionType = DBConnectionString.UEIPUBLIC;
            }
            return connectionType;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="intron"></param>
        /// <param name="label"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<string> IntronLabelRelation(string intron, string label, string dbLabel, string dbIntron, string assignedIntron, string id, bool fromError, bool outPutFmt)
        //public List<string> IntronLabelRelation(string intron, string label, string id, bool fromError, bool outPutFmt)
        {
            string strTable = string.Empty;
            StringBuilder strHeader = new StringBuilder();
            StringBuilder strBody = new StringBuilder();
            string strFooter = string.Empty;
            string strStatus = string.Empty;
            string strLabelStmt = "SELECT DISTINCT Intron,Label FROM [function] WHERE Intron IS NOT NULL AND Label = '" + label + "' AND LEFT(ID,1) = '" + id.Substring(0, 1) + "'";
            string strIntronStmt = "SELECT DISTINCT Intron,Label FROM [function] WHERE Label IS NOT NULL AND Intron = '" + intron + "' AND LEFT(ID,1) = '" + id.Substring(0, 1) + "'";
            const string strUnion = " UNION ";
            string strSQLStmt = string.Empty;
            bool isFound = false;

            string isPresent = string.Empty;

            List<string> returnValue = new List<string>();
            SqlConnection conn = new SqlConnection(_dbConnectionType);
            //SqlConnection connAI = new SqlConnection(DBConnectionString.UEIPUBLIC);
            try
            {

                if (string.IsNullOrEmpty(label))
                {
                    strSQLStmt = strIntronStmt;
                    strStatus = "Captured Label is Empty. Captured Intron Value : " + intron + "\r\n";

                }

                if (string.IsNullOrEmpty(intron))
                {
                    strSQLStmt = strLabelStmt;
                    if (!fromError)

                    {
                        if (!string.IsNullOrEmpty(assignedIntron))
                        {
                            strStatus = "Captured Intron is Empty; Assigned Intron is " + assignedIntron + " . Captured Label value : " + label + "\r\n";
                        }
                        else
                        {
                            strStatus = "Captured Intron is Empty; Assigned Intron is Empty. Captured Label value : " + label + "\r\n";
                        }
                    }
                    else
                    {
                        strStatus = "Label value : " + label + "\r\n";
                    }
                    List<string> results = AvailinID(id, label);
                    strStatus += results[1];
                }
                if (!string.IsNullOrEmpty(label) && !string.IsNullOrEmpty(intron))
                {
                    strSQLStmt = strLabelStmt + strUnion + strIntronStmt;
                    strStatus = " Both have value Intron: " + intron + " Label: " + label;
                }

                SqlCommand sqlCmd = new SqlCommand(strSQLStmt, conn);
                SqlDataAdapter sDA = new SqlDataAdapter(strSQLStmt, conn);
                dsMap = new DataSet();
                sDA.Fill(dsMap);
                DataTable resultTable = dsMap.Tables[0];

                if (outPutFmt)
                {

                    strHeader.Append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n");
                    strHeader.Append(strStatus);
                    strHeader.Append("\r\n");
                    strHeader.Append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n");
                    strHeader.Append(string.Format("{0,-30}", "Label"));
                    strHeader.Append(string.Format("{0,-90}", "Cap. Description"));
                    strHeader.Append("\r\n");
                    strHeader.Append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n");
                    strHeader.Append(string.Format("{0,-30}", label));
                    strHeader.Append(string.Format("{0,-90}", string.Empty));//functionDesc(label, string.Empty)));
                    strHeader.Append("\r\n");
                    strHeader.Append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n");
                    strHeader.Append("\r\n");
                    strHeader.Append(string.Format("{0,-30}", "Label"));
                    strHeader.Append(string.Format("{0,-15}", "Intron"));
                    strHeader.Append(string.Format("{0,-10}", "Present"));
                    strHeader.Append(string.Format("{0,-90}", "Intron Description"));
                    strHeader.Append("\r\n");
                    strHeader.Append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n");


                    foreach (DataRow dr in resultTable.Rows)
                    {
                        strBody.Append(string.Format("{0,-30}", dr["Label"].ToString()));
                        strBody.Append(string.Format("{0,-15}", dr["Intron"].ToString()));
                        isPresent = checkPresent(dr["Label"].ToString(), dr["Intron"].ToString(), id);
                        strBody.Append(string.Format("{0,-10}", isPresent));
                        strBody.Append(string.Format("{0,-90}", functionDesc(string.Empty, dr["Intron"].ToString())));
                        strBody.Append("\r\n");
                        intronCheck(dr["Intron"].ToString(), ref dbLabel, ref dbIntron);
                        if (isPresent == "YES")
                        {
                            isFound = true;
                        }
                        else if (isFound == null)
                        {
                            isFound = false;
                        }

                    }


                    //if (!string.IsNullOrEmpty(label) && !string.IsNullOrEmpty(dbLabel) && !string.IsNullOrEmpty(dbIntron))
                    //if (!isFound)
                        if (!string.IsNullOrEmpty(dbLabel) && !string.IsNullOrEmpty(dbIntron)
                            && !string.IsNullOrEmpty(label))
                        {
                            {
                                if (conn.State == ConnectionState.Closed) conn.Open();
                                SqlCommand sqlCmdLabel = new SqlCommand("UEI2_Sp_GetUnMatchedLabelandIntron", conn);
                                sqlCmdLabel.CommandType = CommandType.StoredProcedure;
                                sqlCmdLabel.Parameters.Add(new SqlParameter("@vchardbLabel", dbLabel.Trim()));
                                sqlCmdLabel.Parameters.Add(new SqlParameter("@vchardbIntron", dbIntron.Trim()));
                                sqlCmdLabel.Parameters.Add(new SqlParameter("@vcharcapLabel ", label.Trim()));
                                sqlCmdLabel.Parameters.Add(new SqlParameter("@charMode ", id.Substring(0, 1)));
                                SqlDataReader item = sqlCmdLabel.ExecuteReader();
                                while (item.Read())
                                {
                                    strBody.Append(string.Format("{0,-30}", item["Label"].ToString()));
                                    strBody.Append(string.Format("{0,-15}", item["Intron"].ToString()));
                                    isPresent = checkPresent(item["Label"].ToString(), item["Intron"].ToString(), id);
                                    strBody.Append(string.Format("{0,-10}", isPresent));
                                    strBody.Append(string.Format("{0,-90}", functionDesc(string.Empty, item["Intron"].ToString())));
                                    strBody.Append("\r\n");
                                }
                            }
                        }


                    if (resultTable.Rows.Count <= 0)
                    {
                        strBody.Append(string.Format("{0,-15}", "No Record Found in Database"));
                        strBody.Append("\r\n");
                    }
                    strFooter = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\r\n";
                    strHeader.Append(strBody.ToString());
                    strHeader.Append(strFooter);

                    strTable = strHeader.ToString();
                }
                returnValue.Add(strTable);

                if (isFound == null)
                {
                    returnValue.Add("FOUND");
                }
                else
                {
                    returnValue.Add("NOTFOUND");
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
            }
            return returnValue;
        }

        /// <summary>
        /// To Check possible intron for the available list of DBIntrons.
        /// </summary>
        /// <param name="possibleIntron"></param>
        /// <param name="dbLabel"></param>
        /// <param name="dbIntron"></param>
        private void intronCheck(string possibleIntron, ref string dbLabel, ref string dbIntron)
        {
            if (!string.IsNullOrEmpty(dbLabel) && !string.IsNullOrEmpty(dbIntron) && !string.IsNullOrEmpty(possibleIntron))
            {
                string[] lblArray = dbLabel.Split(',');
                string[] intrArray = dbIntron.Split(',');
                if (lblArray.Length > 0)
                {
                    for (int i = 0; i < intrArray.Length; i++)
                    {
                        if (intrArray[i].Contains(possibleIntron))
                        {
                            lblArray[i] = string.Empty;
                            intrArray[i] = string.Empty;
                        }
                    }
                    dbLabel = string.Join(",", lblArray);
                    dbIntron = string.Join(",", intrArray);
                }
                else
                {
                    if (dbIntron.Contains(possibleIntron))
                    {
                        dbLabel = string.Empty;
                        dbIntron = string.Empty;
                    }
                }
            }
        }

        
        //New Changes on May 24th - start
        /// <summary>
        /// 
        /// </summary>
        /// <param name="capLabel"></param>
        /// <param name="capIntron"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public string IntronAndLabelConflict(string capLabel, string capIntron, string ID)
        {
            string result = string.Empty;
            SqlConnection conn = new SqlConnection(_dbConnectionType);
            //string strLabelStmt = "SELECT DISTINCT Intron,Label FROM [function] WHERE Intron IS NOT NULL AND Label = '" + capLabel + "' AND LEFT(ID,1) = '" + ID.Substring(0, 1) + "'";
            //string strIntronStmt = " UNION SELECT DISTINCT Intron,Label FROM [function] WHERE Label IS NOT NULL AND Intron = '" + capIntron + "' AND LEFT(ID,1) = '" + ID.Substring(0, 1) + "'";
            string idMode = ID.Substring(0, 1);
            string strSQLStmt = "SELECT DISTINCT Intron,Label FROM [function] WHERE Label IS NOT NULL AND Intron IS NOT NULL AND Label IN(SELECT DISTINCT Label FROM [function] WHERE Label IS NOT NULL AND Intron = '" + capIntron + "' AND LEFT(ID,1) = '" + idMode + "') AND Intron = '" + capIntron + "' AND ID = '" + ID + "' UNION SELECT DISTINCT Intron,Label FROM [function] WHERE Label IS NOT NULL AND Intron IS NOT NULL AND Intron IN(SELECT DISTINCT Intron FROM [function] WHERE Intron IS NOT NULL AND Label = '" + capLabel + "' AND LEFT(ID,1) = '" + idMode + "')AND ID = '" + ID + "'";

            try
            {
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(strSQLStmt, conn);
                SqlDataReader dr = sqlCmd.ExecuteReader();
                if (dr.HasRows)
                {
                    result = "FOUND";
                }


            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();

            }

            return result;
        }
        public string EmptyIntronConflict()
        {
            string result = string.Empty;
            return result;
        }
        //New Changes on May 24th - end
        /// <summary>
        /// 
        /// </summary>
        /// <param name="label"></param>
        /// <param name="intron"></param>
        /// <returns></returns>
        private string functionDesc(string label, string intron)
        {
            string functiondescription = "<<No Function Description Defined>>";
            SqlConnection conn = new SqlConnection(DBConnectionString.UEIPUBLIC);
            try
            {


                #region  DB
                if (functionTable == null)
                {

                    conn.Open();
                    string sqlStmt = @"SELECT DISTINCT LTRIM(RTRIM(KL.Label)) AS [Label],LTRIM(RTRIM(IT.Intron)) AS 				   
	[Intron],LTRIM(RTRIM(ID.Description)) AS [FunctionBriefDescription] FROM IntronDictionary ID 
	INNER JOIN KeyLabels KL
	ON ID.FK_KeyLabel_RID = KL.KeyLabel_RID 
	JOIN Introns IT 
	ON ID.FK_Intron_RID = IT.Intron_RID
	WHERE ID.Description IS NOT NULL";
                    SqlDataAdapter sDA = new SqlDataAdapter(sqlStmt, conn);

                    functionTable = new DataTable();
                    sDA.Fill(functionTable);

                }
                #endregion


                if (!string.IsNullOrEmpty(label))
                {
                    DataRow[] rowsResult = functionTable.Select("Label = '" + label + "'");
                    foreach (DataRow item in rowsResult)
                    {
                        if (!string.IsNullOrEmpty(item["FunctionBriefDescription"].ToString()))
                        {
                            functiondescription = formatDesc(item["FunctionBriefDescription"].ToString(), 29, "LBL");
                            break;
                        }
                    }

                }
                if (!string.IsNullOrEmpty(intron))
                {
                    DataRow[] rowsResult = functionTable.Select("Intron = '" + intron + "'");
                    foreach (DataRow item in rowsResult)
                    {
                        if (!string.IsNullOrEmpty(item["FunctionBriefDescription"].ToString()))
                        {
                            functiondescription = formatDesc(item["FunctionBriefDescription"].ToString(), 54, "IN");
                            break;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
            }

            return functiondescription;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="desc"></param>
        /// <param name="chrLen"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private string formatDesc(string desc, int chrLen, string key)
        {
            string disp = string.Empty;

            int res = desc.Length / 60;
            if (res >= 1)
            {
                for (int i = 1; i <= res; i++)
                {
                    if (i == 1) disp += desc.Substring(0, 50);
                    if (i > 1)
                    {
                        string fill = new string(' ', chrLen);
                        disp += fill + desc.Substring(0, 50);
                    }
                    desc = desc.Remove(0, 50);
                    disp += "\r\n";
                }
            }
            else
            {
                disp = desc;
            }
            if (res > 0 && desc.Length > 0)
            {
                string fill = new string(' ', chrLen + 1);
                disp += fill + desc;
                if (key == "IN") disp += "\r\n";
            }
            return disp;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="label"></param>
        /// <param name="intron"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private string checkPresent(string label, string intron, string id)
        {
            string presentStatus = "NO";
            bool status = ValidateLabel(label, intron, id);
            if (status) presentStatus = "YES";
            return presentStatus;
        }
        public bool CheckLabelIntron(string label, string intron, string id)
        {
            bool status = false;
            string strValidationStmt = "SELECT DISTINCT Intron,Label FROM [function] WHERE Label IS NOT NULL AND Intron IS NOT NULL AND Label = '" + label + "' AND Intron = '" + intron + "' AND ID = '" + id + "'";
            SqlConnection conn = new SqlConnection(_dbConnectionType);
            try
            {

                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(strValidationStmt, conn);
                sqlCmd.CommandType = CommandType.Text;
                SqlDataReader item = sqlCmd.ExecuteReader();

                if (item.HasRows)
                {
                    status = true;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
            }
            return status;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="label"></param>
        /// <returns></returns>
        public List<string> AvailinID(string id, string label)
        {
            string isFound = string.Empty;
            List<string> results = new List<string>();
            string resultData = string.Empty;
            StringBuilder resultDataX = new StringBuilder();
            string sqlStmt = "SELECT DISTINCT Data, Label, Intron FROM [function] WHERE ID = '" + id + "' AND Label = '" + label + "'";
            SqlConnection conn = new SqlConnection(_dbConnectionType);
            try
            {

                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(sqlStmt, conn);
                sqlCmd.CommandType = CommandType.Text;
                SqlDataReader item = sqlCmd.ExecuteReader();

                if (item.HasRows)

                { isFound = "FOUND"; }
                else
                {
                    isFound = "NOT FOUND";
                    resultDataX.Append(string.Format("{0}{1}{2}{3}{4}", "Label : ", label, " Not Found in the Id '", id, "'.\r\n\r\n"));

                }
                while (item.Read())
                {
                    resultDataX.Append(string.Format("{0}{1}{2}{3}{4}", "Label : ", label, " Found in the Id '", id, "'.\r\n\r\n"));
                    resultDataX.Append(string.Format("{0}{1}{2}", "Data  : ", item["Data"].ToString(), ".\r\n\r\n"));
                    resultDataX.Append(string.Format("{0}{1}{2}", "Label  : ", item["Label"].ToString(), ".\r\n\r\n"));
                    resultDataX.Append(string.Format("{0}{1}{2}", "Intron  : ", item["Intron"].ToString(), ".\r\n\r\n"));
                }



                results.Add(isFound);
                results.Add(resultDataX.ToString());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();

            }
            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="label"></param>
        /// <param name="intron"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool ValidateLabel(string label, string intron, string id)
        {
            bool valid = false;
            string strValidationStmt = "SELECT DISTINCT Intron,Label FROM [function] WHERE Label IS NOT NULL AND Intron IS NOT NULL AND Intron = '" + intron + "' AND ID = '" + id + "'";
                //"SELECT DISTINCT Intron,Label FROM [function] WHERE Label IS NOT NULL AND Intron IS NOT NULL AND Label = '" + label + "' AND Intron = '" + intron + "' AND ID = '" + id + "'";
                //"SELECT DISTINCT Intron,Label FROM [function] WHERE Label IS NOT NULL AND Intron IS NOT NULL AND Label = '" + label + "' AND Intron = '" + intron + "' AND ID = '" + id + "'UNION SELECT DISTINCT Intron,Label FROM [function] WHERE Label IS NOT NULL AND Intron IS NOT NULL AND Intron = '" + intron + "' AND ID = '" + id + "'";
            //string strValidationStmt = "SELECT DISTINCT Intron,Label FROM [function] WHERE Label IS NOT NULL AND Intron IS NOT NULL AND Label = '" + label + "' AND Intron = '" + intron + "' AND ID = '" + id + "'UNION SELECT DISTINCT Intron,Label FROM [function] WHERE Label IS NOT NULL AND Intron IS NOT NULL AND Intron = '" + intron +"' AND ID = '" + id  +"'";
            SqlConnection conn = new SqlConnection(_dbConnectionType);
            try
            {
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(strValidationStmt, conn);
                sqlCmd.CommandType = CommandType.Text;
                SqlDataReader result = sqlCmd.ExecuteReader();
                if (result.HasRows)
                {
                    valid = true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
            }

            return valid;
        }


        //public int FindConflict(ref List<ResultObj> resCollection)
        //{
        //    List<ResultObj> conResults = resCollection.FindAll(
        //       delegate(ResultObj ro)
        //       {
        //           return ro.IsConflict;
        //       }
        //       );

        //    reArrangeResult(ref conResults);

        //    if (conResults.Count < 5)
        //    {
        //        List<ResultObj> priResults = resCollection.FindAll(
        //       delegate(ResultObj ro)
        //       {
        //           return ro.IntronPriority != "(p0)" && ro.IntronPriority != "" & ro.IntronPriority != null;
        //       }
        //       );
        //        priorityDetail(ref priResults);
        //    }
        //    return conResults.Count;
        //}

        //private void reArrangeResult(ref List<ResultObj> conResCollection)
        //{
        //    int conflictCnt = 1;
        //    int conflictCntValue = 5;// 0;
        //    //int.TryParse(ConflictMatch.MaxConflict, out conflictCntValue);
        //    //if (conResCollection.Count > 5)
        //    //{
        //    //    return;
        //    //}
        //    //foreach (ResultObj item in conResCollection)
        //    //{
        //    //    if (!item.IsFromError)
        //    //    {
        //    //        List<string> results = AvailinID(item.ID, item.Label);
        //    //        item.PassDataOutput += results[1];
        //    //    }
        //    //}

        //    foreach (ResultObj item in conResCollection)
        //    {

        //        if (conflictCnt > conflictCntValue)//5)
        //        {
        //            return;
        //        }
        //        List<string> result = this.IntronLabelRelation(item.Intron, item.Label, item.DBLabel, item.DBIntron, item.AssignedIntron, item.ID, item.IsFromError, item.IsFormat);
        //        if (item.IsFormat)
        //        {
        //            if (result.Count > 0 && !string.IsNullOrEmpty(result[0]))
        //            {
        //                if (item.IsFromError)
        //                {
        //                    item.FailedDataOutPut += result[0];
        //                }
        //                else
        //                {
        //                    item.PassDataOutput += result[0];
        //                }
        //            }
        //        }
        //        conflictCnt += 1;
        //    }
        //}

        //private void priorityDetail(ref List<ResultObj> priResCollection)
        //{
        //    foreach (ResultObj item in priResCollection)
        //    {
        //        string result = priorityDetailFromDB(item.ID, item.Intron);
        //        item.PassDataOutput += result;
        //    }
        //}

        public string PriorityDetailFromDB(string id, string intron)
        {
            string returnValue = string.Empty;
            string sqlStmt = "SELECT DISTINCT Intron, Label,Data, IntronPriority FROM  [function] WHERE (Label IS NOT NULL) AND ID='" + id + "' AND Intron = '" + intron + "'";
            StringBuilder resultBlr = new StringBuilder();
            SqlConnection conn = new SqlConnection(_dbConnectionType);
            try
            {
                conn.Open();
                SqlCommand sqlCmd = new SqlCommand(sqlStmt, conn);
                sqlCmd.CommandType = CommandType.Text;
                SqlDataReader item = sqlCmd.ExecuteReader();
                resultBlr.Append(string.Format("{0,-20}", "Label"));
                resultBlr.Append(string.Format("{0,-15}", "Intron"));
                resultBlr.Append(string.Format("{0,-20}", "Data"));
                resultBlr.Append(string.Format("{0,-10}", "Priority"));
                resultBlr.AppendLine();
                while (item.Read())
                {
                    resultBlr.Append(string.Format("{0,-20}", item["Label"].ToString()));
                    resultBlr.Append(string.Format("{0,-15}", item["Intron"].ToString()));
                    resultBlr.Append(string.Format("{0,-20}", item["Data"].ToString()));
                    resultBlr.Append(string.Format("{0,-10}", item["IntronPriority"].ToString()));
                    resultBlr.AppendLine();
                }
                if (item.HasRows)
                { returnValue = resultBlr.ToString(); }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
            }
            return returnValue;
        }
    }
}
